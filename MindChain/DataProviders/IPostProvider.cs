﻿using MindChain.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public interface IPostProvider
    {
        Task<IEnumerable<Post>> GetByAccount(Account account);
        Task<IEnumerable<Post>> GetLocalizable();
        Task<Post> Get(int id);
        Task<Post> GetFirstFromUser(Account account);
        Task<Post> GetPreviousPost(Post post);
        Task<Post> GetNextPost(Post post);
        Task Add(Post post);
        Task Update(Post post);
        Task Delete(Post post);
    }
}
