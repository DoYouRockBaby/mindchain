﻿namespace Xamarin.Forms
{
    public interface IMapController
    {
        void SendMapClicked(Position position);
        void SendMapLongPress(Position position);
        void SendUserLocationChanged(Position position);
        void SendRegionChanged(MapSpan position);
    }
}