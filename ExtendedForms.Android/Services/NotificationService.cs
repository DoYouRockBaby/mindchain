﻿using Android;
using Android.App;
using Android.Content;
using ExtendedForms.Android.Medias;
using ExtendedForms.Android.Notifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Services;
using Xamarin.Medias;
using Xamarin.Notifications;
using Xamarin.Utility;
using static Android.Support.V4.App.NotificationCompat;

namespace ExtendedForms.Android.Services
{
    class NotificationService : INotificationService
    {
        protected static int _currentNotificationIdentifier = 0;
        private static Dictionary<string, Command> _actionIdentifiers = new Dictionary<string, Command>();

        public async Task<int> PushNotificationAsync(Xamarin.Notifications.Notification notification)
        {
            //Build the notification
            var builder = new Builder(Forms.Forms.Context);
            builder.SetContentTitle(notification.Title);
            builder.SetWhen((long)(notification.DateTime - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            SetSmallIcon(notification.Icon, builder);

            foreach (var component in notification.Components)
            {
                var renderer = NotificationComponentRenderer.GetRenderer(component);

                if(renderer != null)
                {
                    renderer.SetComponent(component);
                    await renderer.SetUpBuilderAsync(builder);
                }
            }

            //Click action
            var notificationService = SimpleContainer.Instance.Create<NotificationService>(); ;
            var intent = new Intent(Forms.Forms.Context, Forms.Forms.Activity.GetType());

            intent.PutExtra("NotificationSystem_CallbackAction", notification.ActionIdentifier);

            if (notification.CommandParameter == null)
            {
                intent.PutExtra("NotificationSystem_SerializedObject", "");
                intent.PutExtra("NotificationSystem_SerializedObjectType", "");
            }
            else
            {
                intent.PutExtra("NotificationSystem_SerializedObject", Serializer.Serialize(notification.CommandParameter));
                intent.PutExtra("NotificationSystem_SerializedObjectType", notification.CommandParameter.GetType().ToString());
            }

            TaskStackBuilder stackBuilder = TaskStackBuilder.Create(Forms.Forms.Context);
            stackBuilder.AddParentStack(Forms.Forms.Activity);
            stackBuilder.AddNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.CancelCurrent);
            builder.SetContentIntent(pendingIntent);

            //Get the notification manager:
            var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;

            //Publish the notification:
            var androidNotification = builder.Build();
            notificationManager.Notify(_currentNotificationIdentifier++, androidNotification);

            return _currentNotificationIdentifier;
        }

        public void CancelNotification(int id)
        {
            var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.Cancel(id);
        }

        public void CancelAllNotification()
        {
            var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.CancelAll();
        }

        public void ExecuteActivityNotificationActions(Activity activity)
        {
            //Call Notification Callback
            if (activity.Intent != null)
            {
                var callbackAction = activity.Intent.GetStringExtra("NotificationSystem_CallbackAction");

                if (callbackAction != null && callbackAction != "")
                {
                    var command = GetRegistedNotificationAction(callbackAction);

                    var serializedObject = activity.Intent.GetStringExtra("NotificationSystem_SerializedObject");
                    var serializedObjectType = activity.Intent.GetStringExtra("NotificationSystem_SerializedObjectType");

                    if (serializedObjectType == "")
                    {
                        command.Execute(null);
                    }
                    else
                    {
                        var type = Type.GetType(serializedObjectType);
                        var obj = Serializer.Deserialize(serializedObject, type);

                        command.Execute(obj);
                    }
                }
            }
        }

        public Command GetRegistedNotificationAction(string identifier)
        {
            _actionIdentifiers.TryGetValue(identifier, out Command value);
            return value;

        }

        public void RegisterNotificationAction(string identifier, NotificationAction action)
        {
            if(_actionIdentifiers.ContainsKey(identifier))
            {
                return;
            }

            _actionIdentifiers.Add(identifier, action.Command);
            
        }

        private void SetSmallIcon(MediaSource source, Builder builder)
        {
            IMediaSourceHandler handler;

            if(source == null)
            {
                builder.SetSmallIcon(Resource.Drawable.IcDialogInfo);
                return;
            }

            if ((handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(source)) != null && handler is FileMediaSourceHandler)
            {
                var resid = ResourceManager.IdFromTitle((FileMediaSource)source, ResourceManager.DrawableClass);
                builder.SetSmallIcon(resid);
            }
            else
            {
                builder.SetSmallIcon(Resource.Drawable.IcDialogInfo);
                Log.Warning("Could not load image named, only uri resources are supported for small images: {0}", (FileMediaSource)source);
            }
        }
    }
}