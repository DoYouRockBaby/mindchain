﻿using Xamarin.Forms.Platform.UWP;
using System.ComponentModel;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using ExtendedForms.UWP.Views;
using Windows.UI.Xaml;

[assembly: ExportRenderer(typeof(ExtendedListView), typeof(ExtendedForms.UWP.Renderers.ExtendedListViewRenderer))]
namespace ExtendedForms.UWP.Renderers
{
    public class ExtendedListViewRenderer : ViewRenderer<ExtendedListView, FrameworkElement>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ExtendedListView> e)
        {
            if (e.NewElement != null)
            {
                if (Control == null)
                    SetupControl();

                SetupElement(e.NewElement);
            }

            if (e.OldElement != null)
                DestroyElement(e.OldElement);

            base.OnElementChanged(e);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Element.BackgroundColor):
                    SetBackgroundColor();
                    break;
                case nameof(ExtendedListView.ItemsSource):
                case nameof(ExtendedListView.ItemTemplate):
                    HandleOnItemSourceChanged();
                    break;
                default:
                    break;
            }

            base.OnElementPropertyChanged(sender, e);
        }

        void SetupControl()
        {
            SetNativeControl(new NativeRecyclerListView(this));
        }

        void SetupElement(ExtendedListView element)
        {
            SetBackgroundColor();
            HandleOnItemSourceChanged();
        }

        void DestroyElement(ExtendedListView element)
        {
        }

        void HandleOnItemSourceChanged()
        {
            if (Element?.ItemsSource == null)
                return;

            ((NativeRecyclerListView)Control).EmbeddedList.ItemsSource = Element.ItemsSource;
        }

        void SetBackgroundColor()
        {
            if (Control != null && Element != null)
                ((NativeRecyclerListView)Control).EmbeddedList.Background = ToBrush(Element.BackgroundColor);
        }

        Brush ToBrush(Xamarin.Forms.Color color)
        {
            var wpColor = Windows.UI.Color.FromArgb(
            (byte)(color.A * 255),
            (byte)(color.R * 255),
            (byte)(color.G * 255),
            (byte)(color.B * 255));

            return new SolidColorBrush(wpColor);
        }
    }
}