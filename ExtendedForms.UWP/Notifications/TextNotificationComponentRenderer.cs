﻿using ExtendedForms.UWP.Notifications;
using Microsoft.Toolkit.Uwp.Notifications;
using System.Threading.Tasks;
using Xamarin.Notifications;

[assembly: ExportNotificationComponent(typeof(TextNotificationComponent), typeof(TextNotificationComponentRenderer))]
namespace ExtendedForms.UWP.Notifications
{
    public class TextNotificationComponentRenderer : NotificationComponentRenderer<TextNotificationComponent>
    {
        public override Task SetUpNotificationAsync(ToastContent content, ToastBindingGeneric bindingGeneric)
        {
            if(Component.IsSubtext)
            {
                bindingGeneric.Children.Add(new AdaptiveText() { Text = Component.Text, HintMaxLines = 1, HintStyle = AdaptiveTextStyle.Subtitle });
            }
            else
            {
                bindingGeneric.Children.Add(new AdaptiveText() { Text = Component.Text });
            }

            return Task.CompletedTask;
        }
    }
}