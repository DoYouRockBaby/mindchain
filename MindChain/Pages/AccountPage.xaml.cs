﻿using MindChain.Models;
using MindChain.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountPage : ContentPage
    {
        private AccountViewModel viewModel;

        public AccountPage(Account account)
        {
            viewModel = SimpleContainer.Instance.Create<AccountViewModel>();
            viewModel.Account = account;
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            InitializeComponent();
        }

        public Account Account
        {
            get
            {
                return viewModel.Account;
            }
        }

        private async Task FriendSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var listView = sender as ListView;
            listView.SelectedItem = null;

            var account = e.SelectedItem as Account;
            await viewModel.ShowAccount(account);
        }
    }
}