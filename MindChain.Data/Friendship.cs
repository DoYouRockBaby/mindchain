﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MindChain.Data
{
    public class Friendship
    {
        public string Id { get; set; }
        public Account Account1 { get; set; }
        public Account Account2 { get; set; }
        public DateTime Date { get; set; }
    }
}
