﻿using System;
using CoreGraphics;
using PointF = CoreGraphics.CGPoint;
using RectangleF = CoreGraphics.CGRect;
using SizeF = CoreGraphics.CGSize;
using Xamarin.Forms;
#if __MOBILE__
using UIKit;
#else
using AppKit;
using UIColor = AppKit.NSColor;
#endif

namespace ExtendedForms.iOS.Extensions
{
    public static class ColorExtension
    {
        internal static readonly UIColor SeventyPercentGrey = new UIColor(0.7f, 0.7f, 0.7f, 1);
    }
}