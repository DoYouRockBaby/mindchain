﻿using System;

namespace MindChain.Data
{
    public class FileUrl
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
