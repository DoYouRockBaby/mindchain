﻿using MindChain.DataProviders;
using MindChain.Helpers;
using MindChain.Models;
using MindChain.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class PostsViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IAccountProvider accountProvider;
        private IPostProvider postProvider;
        private IFileProvider fileProvider;

        public PostsViewModel(IAccountProvider accountProvider, IPostProvider postProvider, IFileProvider fileProvider)
        {
            this.accountProvider = accountProvider;
            this.postProvider = postProvider;
            this.fileProvider = fileProvider;

            NewPost = new NewPostViewModel(accountProvider, postProvider, fileProvider);
            ListItems = new ObservableRangeCollection<AccountPost>();

            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            new Command(async () => CurrentAccount = await accountProvider.CurrentUser()).Execute(null);
        }

        private Page parentPage;
        override public Page ParentPage
        {
            get
            {
                return parentPage;
            }
            set
            {
                if (value != parentPage)
                {
                    parentPage = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ParentPage)));
                    NewPost.ParentPage = ParentPage;
                }
            }
        }

        public Command LoadItemsCommand { get; set; }

        public NewPostViewModel newPost = null;
        public NewPostViewModel NewPost
        {
            get
            {
                return newPost;
            }
            set
            {
                if (value != newPost)
                {
                    newPost = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NewPost)));
                }
            }
        }

        public Account currentAccount = null;
        public Account CurrentAccount
        {
            get
            {
                return currentAccount;
            }
            set
            {
                if (value != currentAccount)
                {
                    currentAccount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentAccount)));
                    LoadItemsCommand.Execute(null);
                }
            }
        }
        
        private ObservableRangeCollection<AccountPost> listItems = new ObservableRangeCollection<AccountPost>();
        public ObservableRangeCollection<AccountPost> ListItems
        {
            get
            {
                return listItems;
            }
            set
            {
                if (value != listItems)
                {
                    listItems = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ListItems)));
                }
            }
        }

        bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                if(value != isBusy)
                {
                    isBusy = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsBusy)));
                }
            }
        }

        public async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                ListItems.Clear();
                var accounts = await accountProvider.GetFriends(CurrentAccount);

                List<AccountPost> items = new List<AccountPost>();
                foreach(var account in accounts)
                {
                    var posts = await postProvider.GetByAccount(account);

                    items.Add(new AccountPost(postProvider)
                    {
                        Account = account,
                        Posts = posts,
                        ParentPage = ParentPage
                    });
                }

                ListItems.ReplaceRange(items);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
