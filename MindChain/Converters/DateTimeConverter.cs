﻿using System;
using System.Globalization;
using System.Reflection;
using Xamarin.Forms;

namespace MindChain.Converters
{
    /// <summary>
    /// This converter loads an embedded resource and creates an ImageSource
    /// from it using the built-in Xamarin.Forms method.
    /// </summary>
    public class DateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var datetime = (DateTime)value;

            return datetime.ToString("dd/MM/yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException($"{nameof(EmbeddedImageConverter)} cannot be used on two-way bindings.");
        }
    }
}
