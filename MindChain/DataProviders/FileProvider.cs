﻿using MindChain.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public class FileProvider : AbstractProvider, IFileProvider
    {
        public FileProvider(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<String> PublishFile(FileDatas Filedatas)
        {
            var serialized = JsonConvert.SerializeObject(Filedatas);

            HttpClient client = GetClient();
            var response = await client.PostAsync(Url + "file/",
                new StringContent(serialized, Encoding.UTF8, "application/json"));

            var str = await response.Content.ReadAsStringAsync();

            return str;
        }
    }
}
