﻿using System.ComponentModel;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Support.V7.Widget;
using AndroidResources = Android.Content.Res.Resources;
using Android.Graphics;

[assembly: ExportRenderer(typeof(ExtendedListView), typeof(ExtendedForms.Android.Renderers.ExtendedListViewRenderer))]
namespace ExtendedForms.Android.Renderers
{
    class ExtendedListViewRenderer : ViewRenderer<ExtendedListView, RecyclerView>
    {
        ExtendedListViewAdapter _adapter;
        RecyclerViewMargin _itemDecoration;
        bool _disposed;
        //bool _isAttached;

        public ExtendedListViewRenderer(Context context) : base(context)
		{
            AutoPackage = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;

            if (disposing)
            {
                //dispose
            }

            base.Dispose(disposing);
        }

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            //_isAttached = true;
            //_adapter.IsAttachedToWindow = _isAttached;
            //UpdateIsRefreshing(isInitialValue: true);
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();

            //_isAttached = false;
            //_adapter.IsAttachedToWindow = _isAttached;
        }

        protected override RecyclerView CreateNativeControl()
        {
            return new RecyclerView(Context);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ExtendedListView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                if (_adapter != null)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
            }

            if (e.NewElement != null)
            {
                RecyclerView nativeListView = Control;
                if (nativeListView == null)
                {
                    _itemDecoration = new RecyclerViewMargin();

                    nativeListView = CreateNativeControl();
                    nativeListView.SetAdapter(new ExtendedListViewAdapter(Context, nativeListView, e.NewElement));
                    nativeListView.AddItemDecoration(_itemDecoration);

                    SetNativeControl(nativeListView);
                }

                UpdateDataset();
                UpdateLayouting();
                UpdateItemMargin();
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch(e.PropertyName)
            {
                case nameof(ExtendedListView.ItemsSource):
                    UpdateDataset();
                    break;
                case nameof(ExtendedListView.ItemTemplate):
                    UpdateDataset();
                    break;
                case nameof(ExtendedListView.Orientation):
                    UpdateLayouting();
                    break;
                case nameof(ExtendedListView.ItemMargin):
                    UpdateItemMargin();
                    break;
            }
        }

        protected void UpdateDataset()
        {
            //Control
        }

        protected void UpdateLayouting()
        {
            LinearLayoutManager layoutManager = null;

            if (Element.Orientation == ExtendedListView.ListViewOrientation.Vertical)
            {
                layoutManager = new LinearLayoutManager(Context, LinearLayoutManager.Vertical, false);
            }
            else
            {
                layoutManager = new LinearLayoutManager(Context, LinearLayoutManager.Horizontal, false);
            }

            Control.SetLayoutManager(layoutManager);
        }

        protected void UpdateItemMargin()
        {
            _itemDecoration.Margin = Element.ItemMargin;
        }

        public class RecyclerViewMargin : RecyclerView.ItemDecoration
        {
            public Thickness Margin { get; set; } = default(Thickness);

            public override void GetItemOffsets(Rect outRect, global::Android.Views.View view, RecyclerView parent, RecyclerView.State state)
            {
                int position = parent.GetChildLayoutPosition(view);

                outRect.Right = (int)(Margin.Right * AndroidResources.System.DisplayMetrics.Density);
                outRect.Left = (int)(Margin.Left * AndroidResources.System.DisplayMetrics.Density);
                outRect.Bottom = (int)(Margin.Bottom * AndroidResources.System.DisplayMetrics.Density);
                outRect.Top = (int)(Margin.Top * AndroidResources.System.DisplayMetrics.Density);
            }
        }
    }
}