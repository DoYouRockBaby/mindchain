﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Xamarin.Utility
{
    public static class Serializer
    {
        public static string Serialize(this object self)
        {
            XmlSerializer serializer = new XmlSerializer(self.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, self);

                return writer.ToString();
            }
        }

        public static object Deserialize(this string self, Type type)
        {
            if(self == null || self == "" || type == null)
            {
                return null;
            }

            var serializer = new XmlSerializer(type);

            using (TextReader reader = new StringReader(self))
            {
                try
                {
                    return serializer.Deserialize(reader);
                }
                catch(Exception)
                {
                    return null;
                }
            }
        }
    }
}
