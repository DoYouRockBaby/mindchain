﻿using ExtendedForms.iOS.Notifications;
using Foundation;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Notifications;
using Xamarin.Utility;
using static ExtendedForms.iOS.Notifications.ActionNotificationComponentRenderer;

namespace ExtendedForms.iOS.Services
{
    class NotificationService : INotificationService
    {
        protected static int _currentNotificationIdentifier = 0;
        private static Dictionary<string, Command> _actionIdentifiers = new Dictionary<string, Command>();

        public async Task<int> PushNotificationAsync(Xamarin.Notifications.Notification notification)
        {
            var requestID = _currentNotificationIdentifier++;
            var nativeNotification = new UNMutableNotificationContent
            {
                Title = notification.Title,
            };

            var actions = new List<UNNotificationAction>();

            //Traverse components
            foreach (var component in notification.Components)
            {
                var renderer = NotificationComponentRenderer.GetRenderer(component);

                if(renderer != null)
                {
                    renderer.SetComponent(component);
                    await renderer.SetUpNotificationAsync(nativeNotification, actions);
                }
            }

            //Handle actions
            if(actions.Count > 0)
            {
                var intentIDs = new string[] { };
                var category = UNNotificationCategory.FromIdentifier("category_" + requestID, actions.ToArray(), intentIDs, UNNotificationCategoryOptions.None);
                var categories = new NSMutableSet(await UNUserNotificationCenter.Current.GetNotificationCategoriesAsync())
                {
                    category
                };

                UNUserNotificationCenter.Current.SetNotificationCategories(new NSSet<UNNotificationCategory>(categories.ToArray<UNNotificationCategory>()));

                nativeNotification.CategoryIdentifier = "category_" + requestID;
            }

            //Add tap action
            ActionParameters arguments = null;
            if (notification.CommandParameter != null)
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = notification.ActionIdentifier,
                    SerializedParam = Serializer.Serialize(notification.CommandParameter),
                    SerializedParamType = notification.CommandParameter.GetType().ToString()
                };

                if (nativeNotification.UserInfo == null)
                {
                    nativeNotification.UserInfo = new NSDictionary(new NSString("ExtendedForms.iOS.Notifications.DefautlAction"), arguments.Serialize());
                }
                else
                {
                    var previousUserInfo = new NSMutableDictionary(nativeNotification.UserInfo)
                    {
                        { new NSString("ExtendedForms.iOS.Notifications.DefautlAction"), NSObject.FromObject(arguments.Serialize()) }
                    };

                    nativeNotification.UserInfo = new NSDictionary(previousUserInfo);
                }
            }

            //Setup
            var request = UNNotificationRequest.FromIdentifier(requestID.ToString(), nativeNotification, null);

            //Push the notification
            UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) => {
                if (err != null)
                {
                    // Do something with error...
                }
            });

            return requestID;
        }

        public void CancelNotification(int id)
        {
            /*var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.Cancel(id);*/
        }

        public void CancelAllNotification()
        {
            /*var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.CancelAll();*/
        }

        public Command GetRegistedNotificationAction(string identifier)
        {
            _actionIdentifiers.TryGetValue(identifier, out Command value);
            return value;

        }

        public void RegisterNotificationAction(string identifier, NotificationAction action)
        {
            if(_actionIdentifiers.ContainsKey(identifier))
            {
                return;
            }

            _actionIdentifiers.Add(identifier, action.Command);
            
        }
    }
}