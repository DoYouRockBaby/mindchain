﻿using Xamarin.Forms.Xaml;

namespace Sample.Pages.Notification.Notification
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageNotification : Xamarin.Notifications.Notification
    {
        public ImageNotification()
        {
            InitializeComponent();
        }
    }
}