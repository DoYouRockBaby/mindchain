﻿using System;
using System.Collections.ObjectModel;

namespace MindChain.Data
{
    public class Post
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public string AccountId { get; set; }
        public Account Account { get; set; }
        public ObservableCollection<FileUrl> Files { get; set; } = new ObservableCollection<FileUrl>();
        public Localization Location { get; set; }
    }
}
