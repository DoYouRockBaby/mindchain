﻿using MindChain.Models;
using MindChain.ViewModels;
using Plugin.FilePicker.Abstractions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static MindChain.ViewModels.PostsViewModel;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostsPage : ContentPage
    {
        private PostsViewModel viewModel;

        public PostsPage()
        {
            viewModel = SimpleContainer.Instance.Create<PostsViewModel>();
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.ListItems.Count == 0)
            {
                viewModel.LoadItemsCommand.Execute(null);
            }
        }

        private void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            AccountList.SelectedItem = null;
        }

        private async Task GoToAddContactPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new AddContactPage());
        }

        private async Task GoToEventMapPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MapPostsPage());
        }
    }
}