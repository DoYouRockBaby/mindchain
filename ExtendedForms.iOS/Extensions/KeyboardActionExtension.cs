﻿using UIKit;
using Xamarin.Forms;

namespace ExtendedForms.iOS.Extensions
{
    public static class KeyboardActionExtension
    {
        public static UIReturnKeyType ToIOS(this KeyboardAction self)
        {
            switch (self)
            {
                case KeyboardAction.Next:
                    return UIReturnKeyType.Next;
                case KeyboardAction.Search:
                    return UIReturnKeyType.Search;
                case KeyboardAction.Send:
                    return UIReturnKeyType.Send;
                case KeyboardAction.Go:
                    return UIReturnKeyType.Go;
                case KeyboardAction.Done:
                    return UIReturnKeyType.Done;
                default:
                    return UIReturnKeyType.Send;
            }
        }
    }
}