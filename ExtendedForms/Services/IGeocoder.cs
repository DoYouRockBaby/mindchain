﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xamarin.Forms.Services
{
    public interface IGeocoder
    {
        Task<IEnumerable<string>> GetSearchSuggestionsAsync(string request);
        Task<IEnumerable<string>> GetAddressesForPositionAsync(Position position);
        Task<IEnumerable<Position>> GetPositionsForAddressAsync(string address);
    }
}
