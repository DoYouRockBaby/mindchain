﻿using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using ExtendedForms.Android.Medias;
using System.Threading.Tasks;
using Xamarin.Medias;

namespace ExtendedForms.Android.Extensions
{
    public static class MediaExtension
    {
        public static async Task<Icon> ToIcon(this MediaSource self, Context context)
        {
            IMediaSourceHandler handler;

            if ((handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(self)) != null)
            {
                if (handler is FileMediaSourceHandler)
                {
                    return context.GetIcon((FileMediaSource)self);
                }
                else
                {
                    try
                    {
                        var bitmap = await handler.LoadImageAsync(self, context);
                        return Icon.CreateWithBitmap(bitmap);
                    }
                    catch (TaskCanceledException)
                    {
                    }
                }
            }

            return null;
        }

        public static async Task<Bitmap> ToBitmap(this MediaSource self, Context context)
        {
            IMediaSourceHandler handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(self);

            if(handler != null)
            {
                try
                {
                    return await handler.LoadImageAsync(self, context);
                }
                catch (TaskCanceledException)
                {
                }
            }

            return null;
        }
    }
}