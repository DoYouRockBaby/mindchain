﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class BoolToColorConverter : IValueConverter
    {
        public Color GoodColor { get; set; }
        public Color WrongColor { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boolean = (Boolean)value;
            return boolean ? GoodColor : WrongColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Color)value == GoodColor;
        }
    }
}
