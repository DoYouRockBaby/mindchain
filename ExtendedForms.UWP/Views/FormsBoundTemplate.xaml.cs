﻿using Windows.UI.Xaml.Controls;
using Xamarin.Forms.Xaml;

namespace ExtendedForms.UWP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormsBoundTemplate : UserControl
    {
        public FormsBoundTemplate()
        {
            InitializeComponent();
        }
    }
}