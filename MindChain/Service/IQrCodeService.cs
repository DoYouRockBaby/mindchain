﻿using Xamarin.Forms;

namespace MindChain.Service
{
    public interface IQrCodeService
    {
        ImageSource ImageFromCode(string code);
    }
}
