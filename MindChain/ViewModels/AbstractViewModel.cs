﻿using MindChain.Models;
using MindChain.Pages;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class AbstractViewModel
    {
        public virtual Page ParentPage { get; set; }

        public Command ShowAccountCommand { get; set; }

        public AbstractViewModel()
        {
            ShowAccountCommand = new Command<Account>(async (Account account) => await ShowAccount(account));
        }

        public async Task ShowAccount(Account account)
        {
            if(account == null)
            {
                return;
            }

            if (ParentPage is AccountPage accountPage && accountPage.Account.UserName == account.UserName)
            {
                return;
            }

            if (ParentPage is MyAccountPage myaccountPage && myaccountPage.Account.UserName == account.UserName)
            {
                return;
            }

            await ParentPage.Navigation.PushAsync(new AccountPage(account));
        }
    }
}
