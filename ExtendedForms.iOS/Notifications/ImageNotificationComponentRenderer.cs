﻿using ExtendedForms.iOS.Extensions;
using ExtendedForms.iOS.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserNotifications;

[assembly: ExportNotificationComponent(typeof(Xamarin.Notifications.ImageNotificationComponent), typeof(ImageNotificationComponentRenderer))]
namespace ExtendedForms.iOS.Notifications
{
    public class ImageNotificationComponentRenderer : NotificationComponentRenderer<Xamarin.Notifications.ImageNotificationComponent>
    {
        public override async Task SetUpNotificationAsync(UNMutableNotificationContent notification, List<UNNotificationAction> actions)
        {
            if (Component.Format == Xamarin.Notifications.ImageNotificationComponent.ImageFormat.Big)
            {
                var attachmentId = (notification.Attachments == null) ? 0 : notification.Attachments.Length;
                var attachment = await Component.Source.ToAttachment(attachmentId.ToString());

                if(notification.Attachments != null)
                {
                    var attachments = new UNNotificationAttachment[notification.Attachments.Length + 1];
                    for(int i = 0; i < notification.Attachments.Length; i++)
                    {
                        attachments[i] = notification.Attachments[i];
                    }

                    attachments[notification.Attachments.Length] = attachment;
                    notification.Attachments = attachments;
                }
                else
                {
                    notification.Attachments = new UNNotificationAttachment[] { attachment };
                }
            }
            else
            {
                var attachmentId = (notification.Attachments == null) ? 0 : notification.Attachments.Length;
                var attachment = await Component.Source.ToAttachment(attachmentId.ToString());

                if (notification.Attachments != null)
                {
                    var attachments = new UNNotificationAttachment[notification.Attachments.Length + 1];
                    for (int i = 0; i < notification.Attachments.Length; i++)
                    {
                        attachments[i] = notification.Attachments[i];
                    }

                    attachments[notification.Attachments.Length] = attachment;
                    notification.Attachments = attachments;
                }
                else
                {
                    notification.Attachments = new UNNotificationAttachment[] { attachment };
                }
            }
        }
    }
}