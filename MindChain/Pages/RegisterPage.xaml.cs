﻿using MindChain.DataProviders;
using MindChain.Models;
using MindChain.ViewModels;
using Plugin.Media;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        private RegisterViewModel viewModel;
        private IAccountProvider accountProvider;

        public RegisterPage()
        {
            viewModel = SimpleContainer.Instance.Create<RegisterViewModel>();
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            accountProvider = SimpleContainer.Instance.Create<IAccountProvider>();

            InitializeComponent();

            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += async (s, e) => {
                if (CrossMedia.Current.IsPickPhotoSupported)
                {
                    var photo = await CrossMedia.Current.PickPhotoAsync();
                    if (photo != null)
                    {
                        UserImage.Source = ImageSource.FromStream(() => photo.GetStream());
                    }
                }
            };

            UserImage.GestureRecognizers.Add(tapGestureRecognizer);
        }

        private void LoginClicked(object sender, System.EventArgs e)
        {
            viewModel.DisplayLogin();
        }

        private void HomeClicked(object sender, System.EventArgs e)
        {
            viewModel.DisplayHome();
        }

        private void Register1Clicked(object sender, System.EventArgs e)
        {
            viewModel.DisplayRegister1();
        }

        private void Register2Clicked(object sender, System.EventArgs e)
        {
            viewModel.DisplayRegister2();
        }

        private void SetMan(object sender, System.EventArgs e)
        {
            viewModel.Account.Gender = Gender.Man;
        }

        private void SetWoman(object sender, System.EventArgs e)
        {
            viewModel.Account.Gender = Gender.Woman;
        }

        private async Task LoginRequestClicked(object sender, System.EventArgs e)
        {
            var result = await viewModel.Login();
            if (result)
            {
                App.Current.MainPage = new MainPage();
            }
        }

        private async Task RegisterRequestClicked(object sender, System.EventArgs e)
        {
            var result = await viewModel.Register();

            if(result)
            {
                result = await viewModel.Login();
                if(result)
                {
                    App.Current.MainPage = new MainPage();
                }
            }
        }
    }
}