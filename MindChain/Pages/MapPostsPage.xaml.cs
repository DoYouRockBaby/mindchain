﻿using MindChain.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPostsPage : ContentPage
    {
        private MapPostsViewModel viewModel;

        public MapPostsPage()
        {
            viewModel = SimpleContainer.Instance.Create<MapPostsViewModel>();
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MapView.UserLocationChanged += UserPositionEvent;

            if (viewModel.Posts.Count == 0)
            {
                new Command(async () =>
                {
                    await viewModel.ExecuteLoadPostsCommand();
                }).Execute(null);
            }
        }

        protected override void OnDisappearing()
        {
            MapView.UserLocationChanged -= UserPositionEvent;
        }

        private void UserPositionEvent(object sender, Map.PositionEventArgs e)
        {
            if(MapView.MapRegion != null)
            {
                MapView.MapRegion = new MapSpan(e.Position, MapView.MapRegion.LatitudeDegrees, MapView.MapRegion.LongitudeDegrees);
                MapView.UserLocationChanged -= UserPositionEvent;
            }
        }
    }
}