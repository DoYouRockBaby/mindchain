﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Xamarin.Forms.Internals;
using Xamarin.Medias;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using ExtendedForms.Android.Medias;

[assembly: ExportMediaSourceHandler(typeof(FileMediaSource), typeof(FileMediaSourceHandler))]
namespace ExtendedForms.Android.Medias
{
    public sealed class FileMediaSourceHandler : IMediaSourceHandler
    {
        // This is set to true when run under designer context
        internal static bool DecodeSynchronously
        {
            get;
            set;
        }

        public async Task<Bitmap> LoadImageAsync(MediaSource source, Context context, CancellationToken cancelationToken = default(CancellationToken))
        {
            string file = ((FileMediaSource)source).File;
            Bitmap bitmap;
            if (File.Exists(file))
                bitmap = !DecodeSynchronously ? (await BitmapFactory.DecodeFileAsync(file).ConfigureAwait(false)) : BitmapFactory.DecodeFile(file);
            else
                bitmap = !DecodeSynchronously ? (await context.Resources.GetBitmapAsync(file).ConfigureAwait(false)) : context.Resources.GetBitmap(file);

            if (bitmap == null)
            {
                Log.Warning(nameof(FileMediaSourceHandler), "Could not find image or image file was invalid: {0}", source);
            }

            return bitmap;
        }
    }
}