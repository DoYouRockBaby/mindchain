﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MindChain.Data;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;

namespace MindChain.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<Context>();
                    var userManager = services.GetRequiredService<UserManager<Account>>();

                    if (!context.Accounts.Any())
                    {
                        Random rand = new Random((int)DateTime.Now.ToBinary());

                        var account1 = new Account { Code = rand.Next().ToString(), UserName = "leobg69", FirstName = "Leo", LastName = "Le Sommer", Gender = Gender.Man };
                        var account2 = new Account { Code = rand.Next().ToString(), UserName = "fanny", FirstName = "Fanny", LastName = "Pin Guillard", Gender = Gender.Woman };
                        var account3 = new Account { Code = rand.Next().ToString(), UserName = "loudeouf", FirstName = "Lou", LastName = "Le Sommer", Gender = Gender.Woman };
                        var account4 = new Account { Code = rand.Next().ToString(), UserName = "jcvandamne", FirstName = "Jean-Claude", LastName = "van Damme", Gender = Gender.Man };
                        var account5 = new Account { Code = rand.Next().ToString(), UserName = "kassdebrice", FirstName = "Brice", LastName = "De Nice", Gender = Gender.Man };

                        userManager.CreateAsync(account1, "leo").GetAwaiter().GetResult();
                        userManager.CreateAsync(account2, "fanny").GetAwaiter().GetResult();
                        userManager.CreateAsync(account3, "lou").GetAwaiter().GetResult();
                        userManager.CreateAsync(account4, "jcv").GetAwaiter().GetResult();
                        userManager.CreateAsync(account5, "brice").GetAwaiter().GetResult();

                        context.Friendships.Add(new Friendship { Account1 = account1, Account2 = account2, Date = DateTime.Now });
                        context.Friendships.Add(new Friendship { Account1 = account1, Account2 = account3, Date = DateTime.Now });
                        context.Friendships.Add(new Friendship { Account1 = account1, Account2 = account4, Date = DateTime.Now });
                        context.Friendships.Add(new Friendship { Account1 = account1, Account2 = account5, Date = DateTime.Now });

                        context.SaveChanges();

                        context.Posts.Add(new Post { Account = account1, Location = new Localization { Place = "Place 1", Latitude = 45.774867, Longitude = 4.887785 }, Files = { new FileUrl { Name = "Fanny", Url = "http://192.168.0.30:64072/api/file/fanny.jpg" }, new FileUrl { Name = "Moi Leo", Url = "http://192.168.0.30:64072/api/file/moi.jpg" } }, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });
                        context.Posts.Add(new Post { Account = account2, Location = new Localization { Place = "Place 2", Latitude = 45.769499, Longitude = 4.883885 }, Files = { new FileUrl { Name = "Pokemon Go", Url = "http://192.168.0.30:64072/api/file/grotadmorve.png" }, new FileUrl { Name = "Pandicorne Love", Url = "http://192.168.0.30:64072/api/file/pandicorne.jpg" } }, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });
                        context.Posts.Add(new Post { Account = account3, Location = new Localization { Place = "Place 3", Latitude = 45.773311, Longitude = 4.874584 }, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });
                        context.Posts.Add(new Post { Account = account4, Location = new Localization { Place = "Place 4", Latitude = 45.776645, Longitude = 4.888410 }, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });
                        context.Posts.Add(new Post { Account = account5, Location = new Localization { Place = "Place 5", Latitude = 45.769762, Longitude = 4.878763 }, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });

                        foreach (var account in context.Accounts)
                        {
                            for (uint i = 0; i < 3; i++)
                            {
                                context.Posts.Add(new Post { Account = account, Text = LoremNET.Lorem.Sentence(5, 10), Date = DateTime.Now });
                            }
                        }

                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }
    }
}
