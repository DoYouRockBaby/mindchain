﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using MindChain.Data;

namespace MindChain.Server.Services
{
    public class WebSocketService
    {
        //Key is username
        Dictionary<string, WebSocket> userWebSockets = new Dictionary<string, WebSocket>();
        Context context;

        public class WebSocketRequest
        {
            public string Signal { get; set; }
            public string Type { get; set; }
            public string SerializedParameter { get; set; }
        }

        public WebSocketService(Context context, SignInManager<Account> test)
        {
            this.context = context;

            test.
        }

        public async Task Invoke(HttpContext context)
        {/*
            var token = context.RequestAborted;
            var socket = await context.WebSockets.AcceptWebSocketAsync();

            var guid = Guid.NewGuid().ToString();
            sockets.TryAdd(guid, socket);

            while (true)
            {
                if (token.IsCancellationRequested)
                    break;

                var message = await GetMessageAsync(socket, token);
                System.Console.WriteLine($"Received message - {message} at {DateTime.Now}");

                if (string.IsNullOrEmpty(message))
                {
                    if (socket.State != WebSocketState.Open)
                        break;

                    continue;
                }

                foreach (var s in sockets.Where(p => p.Value.State == WebSocketState.Open))
                    await SendMessageAsync(s.Value, message, token);
            }

            sockets.TryRemove(guid, out WebSocket redundantSocket);

            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Session ended", token);
            socket.Dispose();*/



            if (context.WebSockets.IsWebSocketRequest)
            {
                WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
                userWebSockets.Add(context.User.Identity.Name, webSocket);
            }
            else
            {
                context.Response.StatusCode = 400;
            }
        }

        public async Task Send(string username, string signal)
        {
            if(!userWebSockets.ContainsKey(username))
            {
                return;
            }

            var request = new WebSocketRequest()
            {
                Signal = signal,
                Type = "",
                SerializedParameter = null
            };

            var serialized = JsonConvert.SerializeObject(request);

            var encoded = Encoding.UTF8.GetBytes(serialized);
            var buffer = new ArraySegment<Byte>(encoded, 0, encoded.Length);

            await userWebSockets[username].SendAsync(buffer, WebSocketMessageType.Binary, true, CancellationToken.None);
        }

        public async Task Send<T>(string username, string signal, T parameter)
        {
            if (!userWebSockets.ContainsKey(username))
            {
                return;
            }

            var request = new WebSocketRequest()
            {
                Signal = signal,
                Type = typeof(T).AssemblyQualifiedName,
                SerializedParameter = JsonConvert.SerializeObject(parameter)
            };

            var serialized = JsonConvert.SerializeObject(request);

            var encoded = Encoding.UTF8.GetBytes(serialized);
            var buffer = new ArraySegment<Byte>(encoded, 0, encoded.Length);

            await userWebSockets[username].SendAsync(buffer, WebSocketMessageType.Binary, true, CancellationToken.None);
        }
    }
}
