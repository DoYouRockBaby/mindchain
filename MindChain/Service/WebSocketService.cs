﻿using MindChain.DataProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MindChain.Service
{
    class WebSocketService
    {
        ClientWebSocket client;
        CancellationTokenSource cts;
        Dictionary<string, RequestHandlerDelegate> requestHandlers = new Dictionary<string, RequestHandlerDelegate>();

        public class WebSocketRequest
        {
            public string Signal { get; set; }
            public string Type { get; set; }
            public string SerializedParameter { get; set; }
        }

        public delegate void RequestHandlerDelegate(WebSocketRequest datas);
        public delegate void ConnectDelegate<T>(T datas);

        public async Task InitializeConnection()
        {
            var newPostUrl = "ws://" + AbstractProvider.BaseUrl + "/ws";

            client = new ClientWebSocket();
            cts = new CancellationTokenSource();

            await client.ConnectAsync(new Uri(newPostUrl), cts.Token);

            await Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    WebSocketReceiveResult result;
                    var message = new ArraySegment<byte>(new byte[4096]);
                    do
                    {
                        result = await client.ReceiveAsync(message, cts.Token);
                        var bytes = message.Skip(message.Offset).Take(result.Count).ToArray();
                        string serializedRequest = Encoding.UTF8.GetString(bytes);

                        try
                        {
                            var request = JsonConvert.DeserializeObject<WebSocketRequest>(serializedRequest);

                            if(requestHandlers.ContainsKey(request.Signal))
                            {
                                requestHandlers[request.Signal].Invoke(request);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Invalide parameter format.");
                            Console.WriteLine(ex.StackTrace);
                        }

                    } while (!result.EndOfMessage);
                }
            }, cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void Connect<T>(string signal, ConnectDelegate<T> action)
        {
            requestHandlers.Add(signal, (request) =>
            {
                var param = JsonConvert.DeserializeObject<T>(request.SerializedParameter);
                action.Invoke(param);
            });
        }
    }
}
