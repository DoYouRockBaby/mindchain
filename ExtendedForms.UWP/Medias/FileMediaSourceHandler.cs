﻿using ExtendedForms.UWP.Medias;
using System;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Xamarin.Medias;

[assembly: ExportMediaSourceHandler(typeof(FileMediaSource), typeof(FileMediaSourceHandler))]
namespace ExtendedForms.UWP.Medias
{
    public sealed class FileMediaSourceHandler : IMediaSourceHandler
    {
        public Task<Windows.UI.Xaml.Media.ImageSource> LoadImageAsync(MediaSource imagesource, CancellationToken cancellationToken = new CancellationToken())
        {
            Windows.UI.Xaml.Media.ImageSource image = null;
            if (imagesource is FileMediaSource filesource)
            {
                string file = filesource.File;
                image = new BitmapImage(new Uri("ms-appx:///" + file));
            }

            return Task.FromResult(image);
        }

        public Task<string> GetOrCreateUriAsync(MediaSource source, CancellationToken cancellationToken = new CancellationToken())
        {
            var imageLoader = source as FileMediaSource;
            return Task.FromResult(imageLoader.File);
        }
    }
}