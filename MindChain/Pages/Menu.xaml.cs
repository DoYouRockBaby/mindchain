﻿using Plugin.Settings;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : ContentPage
    {
        public class PageChangedArgs
        {
            public PageChangedArgs(Page page) { Page = page; }
            public Page Page { get; private set; }
        }

        public delegate void PageChangedHandler(object sender, PageChangedArgs e);
        public event PageChangedHandler PageChanged;

        public Menu()
        {
            InitializeComponent();

            LogoutCommand = new Command(() => Loggout());
        }

        public Command LogoutCommand { get; set; }

        private void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            var listView = sender as ListView;
            var item = e.SelectedItem as MasterPageItem;

            if(item.Command != null)
            {
                item.Command.Execute(item.CommandParameter);
            }

            if(item.TargetType != null)
            {
                var page = SimpleContainer.Instance.Create(item.TargetType) as Page;
                listView.SelectedItem = null;

                PageChanged?.Invoke(this, new PageChangedArgs(page));
            }
        }

        private void Loggout()
        {
            CrossSettings.Current.Remove("login-username");
            CrossSettings.Current.Remove("login-password");
        }
    }
}