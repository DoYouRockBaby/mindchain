﻿using Xamarin.Forms.Xaml;

namespace MindChain.Notifications
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestNotification : Xamarin.Notifications.Notification
    {
        public TestNotification()
        {
            InitializeComponent();
        }
    }
}