﻿using MindChain.DataProviders;
using MindChain.Models;
using Plugin.Settings;
using System.ComponentModel;
using System.Threading.Tasks;

namespace MindChain.ViewModels
{
    class RegisterViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public RegisterViewModel(IAccountProvider accountProvider)
        {
            this.accountProvider = accountProvider;
        }

        private IAccountProvider accountProvider;

        Account account = new Account();
        public Account Account
        {
            get
            {
                return account;
            }
            set
            {
                if (account != value)
                {
                    account = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Account)));
                }
            }
        }

        string loginUsername;
        public string LoginUsername
        {
            get
            {
                return loginUsername;
            }
            set
            {
                if (loginUsername != value)
                {
                    loginUsername = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(LoginUsername)));
                }
            }
        }

        string loginPassword;
        public string LoginPassword
        {
            get
            {
                return loginPassword;
            }
            set
            {
                if (loginPassword != value)
                {
                    loginPassword = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(LoginPassword)));
                }
            }
        }

        string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                if (password != value)
                {
                    password = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Password)));
                }
            }
        }

        string repeatPassword;
        public string RepeatPassword
        {
            get
            {
                return repeatPassword;
            }
            set
            {
                if (repeatPassword != value)
                {
                    repeatPassword = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(RepeatPassword)));
                }
            }
        }

        bool logoVisible = true;
        public bool LogoVisible
        {
            get
            {
                return logoVisible;
            }
            set
            {
                if (logoVisible != value)
                {
                    logoVisible = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(LogoVisible)));
                }
            }
        }

        bool homeVisible = true;
        public bool HomeVisible
        {
            get
            {
                return homeVisible;
            }
            set
            {
                if (homeVisible != value)
                {
                    homeVisible = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(HomeVisible)));
                }
            }
        }

        bool loginVisible = false;
        public bool LoginVisible
        {
            get
            {
                return loginVisible;
            }
            set
            {
                if (loginVisible != value)
                {
                    loginVisible = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(LoginVisible)));
                }
            }
        }

        bool register1Visible = false;
        public bool Register1Visible
        {
            get
            {
                return register1Visible;
            }
            set
            {
                if (register1Visible != value)
                {
                    register1Visible = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Register1Visible)));
                }
            }
        }

        bool register2Visible = false;
        public bool Register2Visible
        {
            get
            {
                return register2Visible;
            }
            set
            {
                if (register2Visible != value)
                {
                    register2Visible = value;
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(nameof(Register2Visible)));
                }
            }
        }

        public void DisplayHome()
        {
            LogoVisible = true;
            HomeVisible = true;
            LoginVisible = false;
            Register1Visible = false;
            Register2Visible = false;
        }

        public void DisplayLogin()
        {
            LogoVisible = true;
            HomeVisible = false;
            LoginVisible = true;
            Register1Visible = false;
            Register2Visible = false;
        }

        public void DisplayRegister1()
        {
            LogoVisible = true;
            HomeVisible = false;
            LoginVisible = false;
            Register1Visible = true;
            Register2Visible = false;
        }

        public void DisplayRegister2()
        {
            LogoVisible = false;
            HomeVisible = false;
            LoginVisible = false;
            Register1Visible = false;
            Register2Visible = true;
        }

        public async Task<bool> Login()
        {
            var result = await accountProvider.Login(LoginUsername, LoginPassword);

            if(result)
            {
                CrossSettings.Current.AddOrUpdateValue("login-username", LoginUsername);
                CrossSettings.Current.AddOrUpdateValue("login-password", LoginPassword);
            }

            return result;
        }

        public async Task<bool> Register()
        {
            if(Password != RepeatPassword)
            {
                return false;
            }

            var result = await accountProvider.Register(Account, Password);

            if (result)
            {
                CrossSettings.Current.AddOrUpdateValue("login-username", Account.UserName);
                CrossSettings.Current.AddOrUpdateValue("login-password", Password);
            }

            return result;
        }
    }
}
