﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Notifications;

namespace Sample
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var application = this;
            var testNotificationAction = new NotificationAction
            {
                Command = new Command(async (test) =>
                {
                    var mainPage = application.MainPage as NavigationPage;
                    var currentPage = mainPage.CurrentPage;

                    await Current.MainPage.DisplayAlert("Notification Action", test.ToString(), "OK");
                    //await currentPage.DisplayAlert("Notification Action", test.ToString(), "OK");
                })
            };

            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            notificationService.RegisterNotificationAction("TestAction", testNotificationAction);

            MainPage = new NavigationPage(new Pages.MainPage());
            //MainPage = new NavigationPage(new Pages.ListView.MainListView());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
