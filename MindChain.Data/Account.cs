﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace MindChain.Data
{
    public enum Gender
    {
        Man,
        Woman
    }

    public class Account : IdentityUser
    {
        public virtual string Code { get; set; }
        public virtual string ImageUrl { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual Gender Gender { get; set; }
    }
}
