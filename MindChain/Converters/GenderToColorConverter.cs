﻿using MindChain.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class GenderToColorConverter : IValueConverter
    {
        public Gender Gender { get; set; }
        public Color GoodColor { get; set; }
        public Color WrongColor { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var gender = (Gender)value;
            return gender == Gender ? GoodColor : WrongColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var booleanValue = (Color)value == GoodColor;
            if(booleanValue)
            {
                return Gender;
            }
            else
            {
                if(Gender == Gender.Man)
                {
                    return Gender.Woman;
                }
                else
                {
                    return Gender.Man;
                }
            }
        }
    }
}
