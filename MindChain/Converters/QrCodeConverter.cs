﻿using MindChain.Service;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class QrCodeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var qrCodeService = SimpleContainer.Instance.Create<IQrCodeService>();
            return qrCodeService.ImageFromCode((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
