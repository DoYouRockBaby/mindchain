﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MindChain.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.Net;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace MindChain.Server.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly Context context;
        private readonly UserManager<Account> userManager;

        public AccountController(Context context, UserManager<Account> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        [HttpGet]
        public List<Account> GetAll()
        {
            return context.Accounts.ToList();
        }

        [HttpGet("{username}")]
        public Account Get(string username)
        {
            return context.Accounts.FirstOrDefault(t => t.UserName == username);
        }

        [HttpGet("{username}/friends")]
        public List<Account> GetFriends(string username)
        {
            var friendships = context.Friendships.Include(x => x.Account1).Include(x => x.Account2).Where(t => t.Account1.UserName == username || t.Account2.UserName == username);

            List<Account> result = new List<Account>();
            foreach(Friendship fs in friendships)
            {
                if(fs.Account1.UserName != username)
                {
                    result.Add(fs.Account1);
                }

                if (fs.Account2.UserName != username)
                {
                    result.Add(fs.Account2);
                }
            }


            result.Sort((x, y) =>
            {
                var maxX = context.Posts.Where(p => p.AccountId == x.Id).Max(p => p.Date);
                var maxY = context.Posts.Where(p => p.AccountId == y.Id).Max(p => p.Date);

                if(maxX == maxY)
                {
                    return 0;
                }
                else if(maxX < maxY)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            });

            return result;
        }

        [HttpPost("search")]
        public List<Account> Search([FromBody]string request)
        {
            if (request == null || request.Length < 3)
            {
                return null;
            }

            return context.Accounts.Where(x => x.UserName.Contains(request) || x.FirstName.Contains(request) || x.LastName.Contains(request)).Take(10).ToList();
        }

        [HttpPost("ask/friend"), Authorize]
        public bool AskFriend([FromBody]string username)
        {
            var account = context.Accounts.FirstOrDefault(t => t.UserName == username);

            //will be implemented after messaging

            return account != null;
        }

        [HttpPost("add/friend"), Authorize]
        public bool AddFriendByCode([FromBody]string code)
        {
            var account = context.Accounts.FirstOrDefault(t => t.Code == code);

            if(account != null)
            {
                var friendship = new Friendship
                {
                    Date = DateTime.Now,
                    Account1 = account,
                    Account2 = context.Accounts.FirstOrDefault(t => t.UserName == User.Identity.Name)
                };

                return true;
            }
            else
            {
                return false;
            }

        }

        [HttpPut("{username}"), Authorize]
        public void Put([FromBody]Account account)
        {
            if(User.Identity.Name != account.UserName)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.Forbidden);
            }

            context.Accounts.Update(account);
            context.SaveChanges();
        }
        
        [HttpDelete("{username}"), Authorize]
        public void Delete([FromBody]Account account)
        {
            if (User.Identity.Name != account.UserName)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.Forbidden);
            }

            context.Posts.RemoveRange(context.Posts.Where(t => t.Account.UserName == account.UserName));
            context.Friendships.RemoveRange(context.Friendships.Where(t => t.Account1.UserName == account.UserName || t.Account2.UserName == account.UserName));
            context.Accounts.Remove(context.Accounts.FirstOrDefault(t => t.UserName == account.UserName));
            context.SaveChanges();
        }
    }
}
