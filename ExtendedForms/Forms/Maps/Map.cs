﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Xamarin.Forms
{
    public class Map : View, IMapController
    {
        public class PinEventArgs : EventArgs
        {
            /// <summary>
            /// Gets the pin
            /// </summary>
            public Pin Pin { get; set; }
        }

        public class PositionEventArgs : EventArgs
        {
            /// <summary>
            /// Gets the position
            /// </summary>
            public Position Position { get; set; }
        }

        /// <summary>
        /// Event raised when a pin gets selected
        /// </summary>
        //public event EventHandler<PinEventArgs> PinSelected;
        /// <summary>
        /// Event raised when an area of the map gets clicked
        /// </summary>
        public event EventHandler<PositionEventArgs> MapClicked;
        /// <summary>
        /// Event raised when an area of the map gets long-pressed
        /// </summary>
        public event EventHandler<PositionEventArgs> MapLongPress;
        /// <summary>
        /// Event raised when the location of the user changes
        /// </summary>
        public event EventHandler<PositionEventArgs> UserLocationChanged;

        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(
                nameof(ItemsSource),
                typeof(IEnumerable),
                typeof(Map),
                null,
                BindingMode.OneWay,
                propertyChanged: (bindableObject, oldValue, newValue) =>
                {
                    ((Map)bindableObject).ItemsSourceChanged();
                }
            );

        /// <summary>
        /// Bindable Property of <see cref="MapRegion"/>
        /// </summary>
        public static readonly BindableProperty MapRegionProperty = BindableProperty.Create(
            nameof(MapRegion),
            typeof(MapSpan),
            typeof(Map),
            MapSpan.FromCenterAndRadius(new Position(40.7142700, -74.0059700), Distance.FromKilometers(2)),
            BindingMode.OneWay,
            null,
            OnRegionChanged);

        /// <summary>
        /// Bindable Property of <see cref="MapCenter"/>
        /// </summary>
        public static readonly BindableProperty MapCenterProperty = BindableProperty.Create(
            nameof(MapCenter),
            typeof(Position),
            typeof(Map),
            default(Position));

        /// <summary>
        /// Binadble property of <see cref="MapType"/>
        /// </summary>
        public static readonly BindableProperty MapTypeProperty = BindableProperty.Create(
            nameof(MapType),
            typeof(MapType),
            typeof(Map),
            default(MapType));

        /// <summary>
        /// Binadble property of <see cref="IsShowingUser"/>
        /// </summary>
        public static readonly BindableProperty IsShowingUserProperty = BindableProperty.Create(
            nameof(IsShowingUser),
            typeof(bool),
            typeof(Map),
            default(bool));

        /// <summary>
        /// Binadble property of <see cref="HasClusteringEnabled"/>
        /// </summary>
        public static readonly BindableProperty HasClusteringEnabledProperty = BindableProperty.Create(
            nameof(HasClusteringEnabled),
            typeof(bool),
            typeof(Map),
            default(bool));

        /// <summary>
        /// Binadble property of <see cref="HasScrollEnabled"/>
        /// </summary>
        public static readonly BindableProperty HasScrollEnabledProperty = BindableProperty.Create(
            nameof(HasScrollEnabled),
            typeof(bool),
            typeof(Map),
            true);
        
        /// <summary>
        /// Binadble property of <see cref="HasZoomEnabled"/>
        /// </summary>
        public static readonly BindableProperty HasZoomEnabledProperty = BindableProperty.Create(
            nameof(HasZoomEnabled),
            typeof(bool),
            typeof(Map),
            true);

        readonly ObservableCollection<Pin> pins = new ObservableCollection<Pin>();

        /// <summary>
        /// Gets/Sets the pins displayed on map
        /// </summary>
        public IList<Pin> Pins
        {
            get { return pins; }
        }

        /// <summary>
        /// Template used for pin generation, use it with ItemSource
        /// </summary>
        public DataTemplate PinTemplate
        {
            get;
            set;
        }

        /// <summary>
        /// Item source used for pin generation, use it with PinTemplate
        /// </summary>
        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        /// <summary>
        /// Gets/Sets the visible map region
        /// </summary>
        public MapSpan MapRegion
        {
            get
            {
                return (MapSpan)GetValue(MapRegionProperty);
            }
            set
            {
                SetValue(MapRegionProperty, value);
                SetValue(MapCenterProperty, value.Center);
            }
        }

        /// <summary>
        /// Gets/Sets the current center of the map.
        /// </summary>
        public Position MapCenter
        {
            get
            {
                return (Position)GetValue(MapCenterProperty);
            }
            set
            {
                MapRegion = new MapSpan(value, MapRegion.LatitudeDegrees, MapRegion.LongitudeDegrees);
            }
        }

        /// <summary>
        /// Gets/Sets the current <see cref="MapType"/>
        /// </summary>
        public MapType MapType
        {
            get => (MapType)GetValue(MapTypeProperty);
            set => SetValue(MapTypeProperty, value);
        }

        /// <summary>
        /// Gets/Sets if the user should be displayed on the map
        /// </summary>
        public bool IsShowingUser
        {
            get => (bool)GetValue(IsShowingUserProperty);
            set => SetValue(IsShowingUserProperty, value);
        }

        /// <summary>
        /// Gets/Sets whether clustering is enabled or not
        /// </summary>
        public bool HasClusteringEnabled
        {
            get => (bool)GetValue(HasClusteringEnabledProperty);
            set => SetValue(HasClusteringEnabledProperty, value);
        }

        /// <summary>
        /// Gets/Sets whether scrolling is enabled or not
        /// </summary>
        public bool HasScrollEnabled
        {
            get => (bool)GetValue(HasScrollEnabledProperty);
            set => SetValue(HasScrollEnabledProperty, value);
        }

        /// <summary>
        /// Gets/Sets whether zooming is enabled or not
        /// </summary>
        public bool HasZoomEnabled
        {
            get => (bool)GetValue(HasZoomEnabledProperty);
            set => SetValue(HasZoomEnabledProperty, value);
        }

        public class MoveToRegionArgs
        {
            public MapSpan Region { get; set; }
            public bool Animate { get; set; }
        }

        public Map()
        {
            VerticalOptions = HorizontalOptions = LayoutOptions.FillAndExpand;
            HeightRequest = 40;
        }

        public void MoveToRegion(MapSpan region, bool animate = true)
        {
            if (region == null)
                throw new ArgumentNullException(nameof(region));

            MessagingCenter.Send(this, "MapMoveToRegion", new MoveToRegionArgs
            {
                Region = region,
                Animate = animate
            });
        }

        private static void OnRegionChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var map = bindable as Map;
            map.MoveToRegion((MapSpan)newValue, false);
        }

        void ItemsSourceChanged()
        {
            if (ItemsSource is INotifyCollectionChanged collection)
            {
                collection.CollectionChanged += (s, e) => UpdateSubViews();
            }

            UpdateSubViews();
        }

        void UpdateSubViews()
        {
            if(ItemsSource == null)
            {
                return;
            }

            Pins.Clear();
            foreach (var item in ItemsSource)
            {
                var created = PinTemplate.CreateContent();
                var pin = created as Pin;

                if(pin == null)
                {
                    continue;
                }

                if (pin is BindableObject bindableObject)
                    bindableObject.BindingContext = item;

                Pins.Add(pin);
            }
        }

        public void SendMapClicked(Position position)
        {
            MapClicked?.Invoke(this, new PositionEventArgs { Position = position });
        }

        public void SendMapLongPress(Position position)
        {
            MapLongPress?.Invoke(this, new PositionEventArgs { Position = position });
        }

        public void SendUserLocationChanged(Position position)
        {
            UserLocationChanged?.Invoke(this, new PositionEventArgs { Position = position });
        }

        public void SendRegionChanged(MapSpan position)
        {
            SetValueCore(MapRegionProperty, position, SetValueFlags.None, SetValuePrivateFlags.Silent);
        }
    }
}
