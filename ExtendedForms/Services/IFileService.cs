﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Xamarin.Forms.Services
{
    public interface IFileService
    {
        string CreateTempFile(byte[] bytes, string ext);
    }
}
