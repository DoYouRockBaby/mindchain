﻿using Android;
using Android.Content;
using ExtendedForms.Android.Medias;
using ExtendedForms.Android.Notifications;
using ExtendedForms.Android.Services;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Medias;
using Xamarin.Notifications;
using Android.App;
using static Android.Support.V4.App.NotificationCompat;
using Xamarin.Utility;

[assembly: ExportNotificationComponent(typeof(ActionNotificationComponent), typeof(ActionNotificationComponentRenderer))]
namespace ExtendedForms.Android.Notifications
{
    public class ActionNotificationComponentRenderer : NotificationComponentRenderer<ActionNotificationComponent>
    {
        public override Task SetUpBuilderAsync(Builder builder)
        {
            IMediaSourceHandler handler;
            var iconId = Resource.Drawable.IcMenuSend;

            if (Component.Icon != null)
            {
                if ((handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(Component.Icon)) != null && handler is FileMediaSourceHandler)
                {
                    iconId = ResourceManager.IdFromTitle((FileMediaSource)Component.Icon, ResourceManager.DrawableClass);
                }
                else
                {
                    Log.Warning("Could not load image named, only uri resources are supported for small images: {0}", (FileMediaSource)Component.Icon);
                }
            }

            var notificationService = SimpleContainer.Instance.Create<NotificationService>(); ;
            var intent = new Intent(Forms.Forms.Context, Forms.Forms.Activity.GetType());

            intent.PutExtra("NotificationSystem_CallbackAction", Component.ActionIdentifier);

            if (Component.CommandParameter == null)
            {
                intent.PutExtra("NotificationSystem_SerializedObject", "");
                intent.PutExtra("NotificationSystem_SerializedObjectType", "");
            }
            else
            {
                intent.PutExtra("NotificationSystem_SerializedObject", Serializer.Serialize(Component.CommandParameter));
                intent.PutExtra("NotificationSystem_SerializedObjectType", Component.CommandParameter.GetType().ToString());
            }

            TaskStackBuilder stackBuilder = TaskStackBuilder.Create(Forms.Forms.Context);
            stackBuilder.AddParentStack(Forms.Forms.Activity);
            stackBuilder.AddNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.GetPendingIntent(0, PendingIntentFlags.CancelCurrent);

            builder.AddAction(iconId, Component.Text, pendingIntent);

            return Task.CompletedTask;
        }
    }
}