﻿using MindChain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public interface IAccountProvider
    {
        Task<Account> CurrentUser();
        Task<bool> Login(string username, string password);
        Task<bool> Register(Account account, string password);
        Task<IEnumerable<Account>> GetAll();
        Task<Account> Get(string username);
        Task<IEnumerable<Account>> GetFriends(Account account);
        Task<IEnumerable<Account>> Search(string request);
        Task<bool> AskFriend(string username);
        Task<bool> AddFriendByCode(string code);
        Task Update(Account account);
        Task Delete(Account account);
    }
}