﻿using System.ComponentModel;
using System.Reflection;
using Xamarin.Forms;

namespace MindChain.Models
{
    public enum Gender
    {
        Man,
        Woman
    }

    public class Account : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        static ImageSource ManImage = ImageSource.FromResource("MindChain.Resources.Images.man.png", typeof(Account).GetTypeInfo().Assembly);
        static ImageSource WomanImage = ImageSource.FromResource("MindChain.Resources.Images.woman.png", typeof(Account).GetTypeInfo().Assembly);

        private string id;
        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                if (id != value)
                {
                    id = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Id)));
                }
            }
        }

        private string code;
        public string Code
        {
            get
            {
                return code;
            }
            set
            {
                if (code != value)
                {
                    code = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Code)));
                }
            }
        }

        private string username;
        public string UserName
        {
            get
            {
                return username;
            }
            set
            {
                if(username != value)
                {
                    username = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(UserName)));
                }
            }
        }

        private string imageUrl;
        public string ImageUrl
        {
            get
            {
                return imageUrl;
            }
            set
            {
                if (imageUrl != value)
                {
                    imageUrl = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ImageUrl)));
                }
            }
        }

        private ImageSource image;
        public ImageSource Image
        {
            get
            {
                if(image == null)
                {
                    if (Gender == Gender.Man)
                    {
                        return ManImage;
                    }
                    else
                    {
                        return WomanImage;
                    }
                }
                else
                {
                    return image;
                }
            }
            set
            {
                if (image != value)
                {
                    image = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Image)));
                }
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FirstName)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FullName)));
                }
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LastName)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FullName)));
                }
            }
        }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        private Gender gender;
        public Gender Gender
        {
            get
            {
                return gender;
            }
            set
            {
                if (gender != value)
                {
                    gender = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Gender)));

                    if(image == null)
                    {
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Image)));
                    }
                }
            }
        }
    }
}
