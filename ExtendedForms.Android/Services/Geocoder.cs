﻿using Android.Content;
using Android.Locations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using AGeocoder = Android.Locations.Geocoder;

namespace ExtendedForms.Android.Services
{
    public class Geocoder : IGeocoder
    {
        static AGeocoder AndroidGeocoder;

        public static void Init(Context context)
        {
            AndroidGeocoder = new AGeocoder(context);
        }

        public async Task<IEnumerable<string>> GetSearchSuggestionsAsync(string request)
        {
            IList<Address> addresses = await AndroidGeocoder.GetFromLocationNameAsync(request, 5);
            return addresses.Select(p =>
            {
                IEnumerable<string> lines = Enumerable.Range(0, p.MaxAddressLineIndex + 1).Select(p.GetAddressLine);
                return string.Join("\n", lines);
            });
        }

        public async Task<IEnumerable<Position>> GetPositionsForAddressAsync(string address)
        {
            IList<Address> addresses = await AndroidGeocoder.GetFromLocationNameAsync(address, 5);
            return addresses.Select(p => new Position(p.Latitude, p.Longitude));
        }

        public async Task<IEnumerable<string>> GetAddressesForPositionAsync(Position position)
        {
            IList<Address> addresses = await AndroidGeocoder.GetFromLocationAsync(position.Latitude, position.Longitude, 5);
            return addresses.Select(p =>
            {
                IEnumerable<string> lines = Enumerable.Range(0, p.MaxAddressLineIndex + 1).Select(p.GetAddressLine);
                return string.Join("\n", lines);
            });
        }
    }
}
