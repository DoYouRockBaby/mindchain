﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xamarin.Forms.Datas
{
    class Collumn : Element
    {
        public delegate int SortingDelegate(object a, object b);

        public static readonly BindableProperty HeaderPropertyProperty = BindableProperty.Create(nameof(HeaderProperty), typeof(object), typeof(Collumn), null, BindingMode.OneWay);
        public static readonly BindableProperty HeaderTemplateProperty = BindableProperty.Create(nameof(HeaderTemplate), typeof(DataTemplate), typeof(Collumn), null, BindingMode.OneWay);
        public static readonly BindableProperty TemplateProperty = BindableProperty.Create(nameof(Template), typeof(DataTemplate), typeof(Collumn), null, BindingMode.OneWay);
        public static readonly BindableProperty HeightProperty = BindableProperty.Create(nameof(Height), typeof(Int32), typeof(Collumn), -1, BindingMode.OneWay);
        public static readonly BindableProperty SortingEnabledProperty = BindableProperty.Create(nameof(SortingEnabled), typeof(bool), typeof(Collumn), false, BindingMode.OneWay);
        public static readonly BindableProperty SortingFunctionProperty = BindableProperty.Create(nameof(SortingFunction), typeof(SortingDelegate), typeof(Collumn), null, BindingMode.OneWay);

        /*public event EventHandler<ItemTappedEventArgs> ItemTapped;
        public event EventHandler<ItemTappedEventArgs> ItemDoubleTapped;
        public event EventHandler<ItemTappedEventArgs> ItemLongTapped;*/

        public object HeaderProperty
        {
            get => GetValue(HeaderPropertyProperty);
            set => SetValue(HeaderPropertyProperty, value);
        }

        public DataTemplate HeaderTemplate
        {
            get => (DataTemplate)GetValue(HeaderTemplateProperty);
            set => SetValue(HeaderTemplateProperty, value);
        }

        public DataTemplate Template
        {
            get => (DataTemplate)GetValue(TemplateProperty);
            set => SetValue(TemplateProperty, value);
        }

        public Int32 Height
        {
            get => (Int32)GetValue(HeightProperty);
            set => SetValue(HeightProperty, value);
        }

        public bool SortingEnabled
        {
            get => (bool)GetValue(SortingEnabledProperty);
            set => SetValue(SortingEnabledProperty, value);
        }

        public SortingDelegate SortingFunction
        {
            get => (SortingDelegate)GetValue(SortingFunctionProperty);
            set => SetValue(SortingFunctionProperty, value);
        }
    }
}
