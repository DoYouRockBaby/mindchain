﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace Sample.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public class SamplePage
        {
            public string Name { get; set; }
            public ImageSource Image { get; set; }
            public Page Target { get; set; }
        }

        private ObservableCollection<SamplePage> samplePages = new ObservableCollection<SamplePage>();
        public ObservableCollection<SamplePage> SamplePages
        {
            get
            {
                return samplePages;
            }
            set
            {
                if (value != samplePages)
                {
                    samplePages = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SamplePages)));
                }
            }
        }
    }
}
