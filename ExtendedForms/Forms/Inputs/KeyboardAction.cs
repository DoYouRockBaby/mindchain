﻿namespace Xamarin.Forms
{
    public enum KeyboardAction
    {
        Next,
        Send,
        Search,
        Go,
        Done
    }
}
