﻿using MindChain.Helpers;
using MindChain.Models;
using MindChain.Pages;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using static Xamarin.Forms.AutocompleteEntry;

namespace MindChain.ViewModels
{
    public class PickLocalizationViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private IGeocoder geocoder;

        public PickLocalizationViewModel(IGeocoder geocoder)
        {
            this.geocoder = geocoder;

            UpdateSuggestionsCommand = new Command(async () => await UpdateSuggestions());
            SearchCommand = new Command(async () => await Search());
            SendLocalizationCommand = new Command(async () => await SendLocalization());
            SuggestionProvider = new GeocolSuggestionProvider();
        }

        public Localization localization = null;
        public Localization Localization
        {
            get
            {
                return localization;
            }
            set
            {
                if (value != localization)
                {
                    localization = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Localization)));

                    Localizations.Clear();
                    if(localization != null)
                    {
                        Localizations.Add(localization);
                    }
                }
            }
        }

        public Position mapCenter;
        public Position MapCenter
        {
            get
            {
                return mapCenter;
            }
            set
            {
                if (value != mapCenter)
                {
                    mapCenter = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MapCenter)));
                }
            }
        }

        private ObservableCollection<Localization> localizations = new ObservableCollection<Localization>();
        public ObservableCollection<Localization> Localizations
        {
            get
            {
                return localizations;
            }
        }

        class GeocolSuggestionProvider : ISuggestionProvider
        {
            private IGeocoder geocoder;

            public GeocolSuggestionProvider()
            {
                geocoder = SimpleContainer.Instance.Create<IGeocoder>();
            }

            public async Task<IEnumerable> GetSuggestions(string request)
            {
                return await geocoder.GetSearchSuggestionsAsync(request);
            }

            public string ConvertToString(object obj)
            {
                return obj.ToString();
            }
        }

        public ISuggestionProvider suggestionProvider;
        public ISuggestionProvider SuggestionProvider
        {
            get
            {
                return suggestionProvider;
            }
            set
            {
                if (suggestionProvider != value)
                {
                    suggestionProvider = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SuggestionProvider)));
                }
            }
        }

        public string searchRequest = "";
        public string SearchRequest
        {
            get
            {
                return searchRequest;
            }
            set
            {
                if (value != searchRequest)
                {
                    searchRequest = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SearchRequest)));

                    if (searchRequest != "")
                    {
                        UpdateSuggestionsCommand?.Execute(null);
                    }
                }
            }
        }

        public ObservableRangeCollection<string> searchSuggestions = new ObservableRangeCollection<string>();
        public ObservableRangeCollection<string> SearchSuggestions
        {
            get
            {
                return searchSuggestions;
            }
            set
            {
                if (value != searchSuggestions)
                {
                    searchSuggestions = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SearchSuggestions)));
                }
            }
        }

        public Command UpdateSuggestionsCommand { get; set; }
        public Command SearchCommand { get; set; }
        public Command SendLocalizationCommand { get; set; }

        public async Task UpdateSuggestions()
        {
            SearchSuggestions.Clear();

            if (SearchRequest != "")
            {
                SearchSuggestions.AddRange(await geocoder.GetSearchSuggestionsAsync(SearchRequest));
            }
        }

        public async Task Search()
        {
            if (SearchRequest != "")
            {
                var results = await geocoder.GetPositionsForAddressAsync(SearchRequest);
                foreach(var result in results)
                {
                    await AddPin(new Position(result.Latitude, result.Longitude));
                    break;
                }
            }
        }

        public async Task SendLocalization()
        {
            await ParentPage.Navigation.PopModalAsync();
        }

        public async Task AddPin(Position position)
        {
            var results = await geocoder.GetAddressesForPositionAsync(position);
            foreach (var result in results)
            {
                Localization = new Localization
                {
                    Place = result,
                    Latitude = position.Latitude,
                    Longitude = position.Longitude,
                };

                var mappage = ParentPage as PickLocalizationPage;
                MapCenter = position;

                break;
            }
        }
    }
}
