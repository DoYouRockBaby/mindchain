﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Notifications;

namespace ExtendedForms.UWP.Notifications
{
    public abstract class NotificationComponentRenderer : IRegisterable
    {
        static readonly BindableProperty RendererProperty = BindableProperty.CreateAttached("Renderer", typeof(NotificationComponentRenderer), typeof(NotificationComponent), null);

        public abstract void SetComponent(NotificationComponent component);
        public abstract Task SetUpNotificationAsync(ToastContent content, ToastBindingGeneric bindingGeneric);

        protected virtual void OnNotificationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        internal static NotificationComponentRenderer GetRenderer(BindableObject component)
        {
            var renderer = (NotificationComponentRenderer)component.GetValue(RendererProperty);
            if (renderer == null)
            {
                renderer = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<NotificationComponentRenderer>(component);
            }

            return renderer;
        }

        internal static void SetRenderer(BindableObject component, NotificationComponentRenderer renderer)
        {
            component.SetValue(RendererProperty, renderer);
        }

        class RendererHolder : Object
        {
            readonly WeakReference<NotificationComponentRenderer> _rendererRef;

            public RendererHolder(NotificationComponentRenderer renderer)
            {
                _rendererRef = new WeakReference<NotificationComponentRenderer>(renderer);
            }

            public NotificationComponentRenderer Renderer
            {
                get
                {
                    return _rendererRef.TryGetTarget(out NotificationComponentRenderer renderer) ? renderer : null;
                }
                set { _rendererRef.SetTarget(value); }
            }
        }
    }

    public abstract class NotificationComponentRenderer<T> : NotificationComponentRenderer where T : NotificationComponent
    {
        public T Component { get; set; }

        override public void SetComponent(NotificationComponent component)
        {
            Component = component as T;
        }
    }
}