﻿using ExtendedForms.Android.Renderers;
using Android.Content;
using Android.App;
using Android.OS;
using ExtendedForms.Android.Services;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using ExtendedForms.Android.Notifications;
using ExtendedForms.Android.Medias;
using System.Reflection;
using Xamarin.Forms.Internals;

namespace ExtendedForms.Forms
{
    public class Forms
    {
        public static Context Context { get; set; }
        public static Activity Activity { get; set; }

        public static void Init(Context context, Activity activity, Bundle bundle)
        {
            //Store params
            Context = context;
            Activity = activity;

            //Init renderer
            MapRenderer.InitMapRenderer(bundle);
            Geocoder.Init(context);
            ResourceManager.Init(Assembly.GetCallingAssembly());

            //Register handlers
            Registrar.RegisterAll(new[] { typeof(ExportNotificationComponentAttribute), typeof(ExportMediaSourceHandlerAttribute) });

            //Register services
            SimpleContainer.Instance.Register<IFileService, FileService>();
            SimpleContainer.Instance.Register<IGeocoder, Geocoder>();
            SimpleContainer.Instance.Register<INotificationService, NotificationService>();

            //Register notification channel
            /*if (Build.VERSION.SdkInt >= Build.VERSION_CODES.O)
            {
                // Create the NotificationChannel, but only on API 26+ because
                // the NotificationChannel class is new and not in the support library
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_description);
                int importance = NotificationManagerCompat.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                notificationManager.createNotificationChannel(channel);
            }*/
        }

        public static void OnStart()
        {
            //Call Notification Callback
            var notificationService = SimpleContainer.Instance.Create<NotificationService>();
            notificationService.ExecuteActivityNotificationActions(Activity);
        }
    }
}
