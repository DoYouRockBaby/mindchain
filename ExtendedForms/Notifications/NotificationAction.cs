﻿using System;
using Xamarin.Forms;

namespace Xamarin.Notifications
{
    public class NotificationAction : Element
    {
        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(Command), typeof(ActionNotificationComponent), null);

        public Command Command
        {
            get { return (Command)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
    }
}