﻿using Xamarin.Forms;
using Xamarin.Medias;

namespace Xamarin.Notifications
{
    public class ImageNotificationComponent : NotificationComponent
    {
        public enum ImageFormat
        {
            Small,
            Big
        }

        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(MediaSource), typeof(ImageNotificationComponent), null);
        public static readonly BindableProperty FormatProperty = BindableProperty.Create(nameof(Format), typeof(ImageFormat), typeof(ImageNotificationComponent), ImageFormat.Small);

        [TypeConverter(typeof(MediaSourceConverter))]
        public MediaSource Source
        {
            get { return (MediaSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public ImageFormat Format
        {
            get { return (ImageFormat)GetValue(FormatProperty); }
            set { SetValue(FormatProperty, value); }
        }
    }
}
