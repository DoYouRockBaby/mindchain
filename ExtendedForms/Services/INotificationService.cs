﻿using System.Threading.Tasks;
using Xamarin.Notifications;

namespace Xamarin.Forms.Services
{
    public interface INotificationService
    {
        void RegisterNotificationAction(string identifier, NotificationAction action);
        Task<int> PushNotificationAsync(Notification notification);
        void CancelNotification(int id);
        void CancelAllNotification();
    }
}
