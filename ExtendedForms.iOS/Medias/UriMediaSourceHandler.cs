﻿using ExtendedForms.iOS.Medias;
using Foundation;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Medias;

[assembly: ExportMediaSourceHandler(typeof(UriMediaSource), typeof(UriMediaSourceHandler))]
namespace ExtendedForms.iOS.Medias
{
    public sealed class UriMediaSourceHandler : IMediaSourceHandler
    {
        public async Task<UIImage> LoadImageAsync(MediaSource source, CancellationToken cancelationToken = default(CancellationToken), float scale = 1)
        {
            UIImage image = null;
            var urisource = source as UriMediaSource;
            if (urisource?.Uri != null)
            {
                using (var streamImage = await urisource.GetStreamAsync(cancelationToken).ConfigureAwait(false))
                {
                    if (streamImage != null)
                        image = UIImage.LoadFromData(NSData.FromStream(streamImage), scale);
                }
            }

            if (image == null)
            {
                Log.Warning(nameof(StreamMediaSourceHandler), "Could not load media: {0}", urisource);
            }

            return image;
        }
    }
}
