﻿using MindChain.DataProviders;
using MindChain.Models;
using MindChain.Pages;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class AccountPost : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IPostProvider postProvider;

        public AccountPost(IPostProvider postProvider)
        {
            this.postProvider = postProvider;
        }

        private Account account = null;
        public Account Account
        {
            get
            {
                return account;
            }
            set
            {
                if (value != account)
                {
                    account = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Account)));
                }
            }
        }

        private IEnumerable<Post> posts = null;
        public IEnumerable<Post> Posts
        {
            get
            {
                return posts;
            }
            set
            {
                if (value != posts)
                {
                    posts = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Posts)));
                }
            }
        }
    }
}
