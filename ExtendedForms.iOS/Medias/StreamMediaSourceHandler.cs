﻿using ExtendedForms.iOS.Medias;
using Foundation;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms.Internals;
using Xamarin.Medias;

[assembly: ExportMediaSourceHandler(typeof(StreamMediaSource), typeof(StreamMediaSourceHandler))]
namespace ExtendedForms.iOS.Medias
{
    public sealed class StreamMediaSourceHandler : IMediaSourceHandler
    {
        public async Task<UIImage> LoadImageAsync(MediaSource source, CancellationToken cancelationToken = default(CancellationToken), float scale = 1f)
        {
            UIImage image = null;
            var streamsource = source as StreamMediaSource;
            if (streamsource?.Stream != null)
            {
                using (var streamImage = await streamsource.GetStreamAsync(cancelationToken).ConfigureAwait(false))
                {
                    if (streamImage != null)
                        image = UIImage.LoadFromData(NSData.FromStream(streamImage), scale);
                }
            }

            if (image == null)
            {
                Log.Warning(nameof(StreamMediaSourceHandler), "Could not load image: {0}", source);
            }

            return image;
        }
    }
}
