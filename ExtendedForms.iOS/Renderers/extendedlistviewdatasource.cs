﻿using System;
using Foundation;
using UIKit;
using Xamarin.Forms;
using System.Collections;
using System.Linq;
using Xamarin.Forms.Platform.iOS;
using CoreGraphics;

namespace ExtendedForms.iOS.Renderers
{
    class ExtendedListViewDataSource : UICollectionViewDataSource
    {
        public ExtendedListView ListView => _listView;
        protected readonly ExtendedListView _listView;
        private readonly IList _dataSource;

        public ExtendedListViewDataSource(ExtendedListView listView)
        {
            _listView = listView;
            _dataSource = listView.ItemsSource?.Cast<object>()?.ToList();
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            //Get previous cell
            CollectionViewCell cell = (CollectionViewCell)collectionView.DequeueReusableCell(nameof(CollectionViewCell), indexPath);

            //create view
            if (cell.View == null)
            {
                cell.View = _listView.ItemTemplate?.CreateContent() as View;
                cell.ListView = _listView;
            }

            //Get or create renderer
            if (cell.NativeView == null)
            {
                if (Platform.GetRenderer(cell.View) == null)
                {
                    Platform.SetRenderer(cell.View, Platform.CreateRenderer(cell.View));
                }
                cell.NativeView = Platform.GetRenderer(cell.View).NativeView;

                //And create UIViews
                foreach (UIView subView in cell.ContentView.Subviews)
                {
                    subView.RemoveFromSuperview();
                }
                cell.NativeView.ContentMode = UIViewContentMode.ScaleToFill;
                cell.ContentView.AddSubview(cell.NativeView);
            }

            //Data binding
            var dataContext = _dataSource[indexPath.Row];
            cell.View.BindingContext = dataContext;
            cell.View.Parent = _listView;

            //Calculate size
            var size = SizeRequest(cell.View, _listView.Width, _listView.Height);
            cell.View.Layout(new Rectangle(0, 0, size.Width, size.Height));

            return cell;
        }

        public Size SizeRequest(View view, double widthConstraint, double heightConstraint)
        {
            if (view != null)
            {
                if (ListView.Orientation == ExtendedListView.ListViewOrientation.Vertical)
                {
                    var measure = view.Measure(_listView.Width, _listView.Height);

                    var width = _listView.ColumnWidth > 0 ? _listView.ColumnWidth : widthConstraint / ListView.RowOrColumnNumber;
                    var height = _listView.RowHeight > 0 ? _listView.RowHeight : measure.Request.Height;

                    return new Size(width, height);
                }
                else
                {
                    var measure = view.Measure(_listView.Width, _listView.Height);

                    var width = _listView.ColumnWidth > 0 ? _listView.ColumnWidth : measure.Request.Width;
                    var height = _listView.RowHeight > 0 ? _listView.RowHeight : heightConstraint / ListView.RowOrColumnNumber;

                    return new Size(width, height);
                }
            }
            else
            {
                var width = _listView.ColumnWidth > 0 ? _listView.ColumnWidth : widthConstraint;
                var height = _listView.RowHeight > 0 ? _listView.RowHeight : 40.0;

                return new Size(width, height);
            }
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return _dataSource != null ? _dataSource.Count : 0;
        }

        public class CollectionViewCell : UICollectionViewCell
        {
            public View View { get; set; }
            public UIView NativeView { get; set; }
            public ExtendedListView ListView { get; set; }

            public CollectionViewCell(IntPtr p) : base(p)
            {
            }

            public override CGSize SizeThatFits(CGSize size)
            {
                if(View == null || ListView == null)
                {
                    return new CGSize(100, 100);
                }

                var request = SizeRequest(View, ListView.Width, ListView.Height);
                return new CGSize(request.Width, request.Height);
            }

            public Size SizeRequest(View view, double widthConstraint, double heightConstraint)
            {
                if (view != null)
                {
                    if (ListView.Orientation == ExtendedListView.ListViewOrientation.Vertical)
                    {
                        var measure = view.Measure(ListView.Width, ListView.Height);

                        var width = ListView.ColumnWidth > 0 ? ListView.ColumnWidth : widthConstraint / ListView.RowOrColumnNumber;
                        var height = ListView.RowHeight > 0 ? ListView.RowHeight : measure.Request.Height;

                        return new Size(width, height);
                    }
                    else
                    {
                        var measure = view.Measure(ListView.Width, ListView.Height);

                        var width = ListView.ColumnWidth > 0 ? ListView.ColumnWidth : measure.Request.Width;
                        var height = ListView.RowHeight > 0 ? ListView.RowHeight : heightConstraint / ListView.RowOrColumnNumber;

                        return new Size(width, height);
                    }
                }
                else
                {
                    var width = ListView.ColumnWidth > 0 ? ListView.ColumnWidth : widthConstraint;
                    var height = ListView.RowHeight > 0 ? ListView.RowHeight : 40.0;

                    return new Size(width, height);
                }
            }
        }
    }
}