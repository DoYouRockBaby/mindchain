using System;
using System.ComponentModel;
using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(AutocompleteEntry), typeof(ExtendedForms.UWP.Renderers.AutocompleteEntryRenderer))]
namespace ExtendedForms.UWP.Renderers
{
    public class AutocompleteEntryRenderer : ViewRenderer<AutocompleteEntry, AutoSuggestBox>
    {
        public static AutocompleteEntry LastAutocompleteEntryUsed { get; set;}

        ITemplatedItemsView<Cell> TemplatedItemsView => Element;
        bool _fontApplied;
        Brush _backgroundColorFocusedDefaultBrush;
        Brush _textDefaultBrush;

        protected override void OnElementChanged(ElementChangedEventArgs<AutocompleteEntry> e)
        {
            base.OnElementChanged(e);


            if (e.OldElement != null)
            {
                ((ITemplatedItemsView<Cell>)e.OldElement).TemplatedItems.CollectionChanged -= OnCollectionChanged;
            }

            if (e.NewElement != null)
            {
                ((ITemplatedItemsView<Cell>)e.NewElement).TemplatedItems.CollectionChanged += OnCollectionChanged;

                if (Control == null)
                {
                    var textBox = new AutoSuggestBox();
                    SetNativeControl(textBox);

                    textBox.ItemTemplate = CreateDataTemplate();
                    textBox.TextBoxStyle = (Windows.UI.Xaml.Style)Windows.UI.Xaml.Application.Current.Resources["FormsTextBoxStyle"];
                    textBox.TextChanged += OnNativeTextChanged;
                    textBox.SuggestionChosen += OnNativeSuggestionChosen;
                    textBox.KeyUp += TextBoxOnKeyUp;
                }

                Control.DataContext = new CollectionViewSource { Source = Element.ItemsSource };

                UpdateText();
                UpdatePlaceholder();
                UpdateTextColor();
                UpdateFont();
                UpdateAlignment();
                UpdateInputScope();
                ClearSizeEstimate();
            }
        }

        public Windows.UI.Xaml.DataTemplate CreateDataTemplate()
        {
            var str =
                  @"<DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation"" xmlns:forms=""using:ExtendedForms.UWP.Renderers"" > " + Environment.NewLine
                + @"<forms:AutocompleteCellControl HorizontalContentAlignment = ""Stretch"" Height = ""{Binding Cell.RenderHeight,RelativeSource={RelativeSource Mode=Self},Converter={StaticResource HeightConverter}}"" />" + Environment.NewLine
                + @"</DataTemplate>";

            var generated = XamlReader.Load(str);
            var template = generated as Windows.UI.Xaml.DataTemplate;

            return template;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && Control != null)
            {
                Control.TextChanged -= OnNativeTextChanged;
                Control.SuggestionChosen -= OnNativeSuggestionChosen;
                Control.KeyUp -= TextBoxOnKeyUp;
            }

            base.Dispose(disposing);
        }

        void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Control.DataContext = new CollectionViewSource { Source = Element.ItemsSource };
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == AutocompleteEntry.TextProperty.PropertyName)
            {
                UpdateText();
            }
            else if (e.PropertyName == AutocompleteEntry.PlaceholderProperty.PropertyName)
            {
                UpdatePlaceholder();
            }
            else if (e.PropertyName == AutocompleteEntry.KeyboardProperty.PropertyName)
            {
                UpdateInputScope();
            }
            else if (e.PropertyName == AutocompleteEntry.IsSpellCheckEnabledProperty.PropertyName)
            {
                UpdateInputScope();
            }
            else if (e.PropertyName == AutocompleteEntry.TextColorProperty.PropertyName)
            {
                UpdateTextColor();
            }
            else if (e.PropertyName == AutocompleteEntry.FontAttributesProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == AutocompleteEntry.FontFamilyProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == AutocompleteEntry.FontSizeProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == AutocompleteEntry.HorizontalTextAlignmentProperty.PropertyName)
            {
                UpdateAlignment();
            }
            else if (e.PropertyName == AutocompleteEntry.ItemTemplateProperty.PropertyName)
            {
                ClearSizeEstimate();
            }
            else if (e.PropertyName == nameof(AutocompleteEntry.ItemsSource))
            {
                ClearSizeEstimate();
                Control.ItemsSource = Element.ItemsSource;
            }
        }

        void ClearSizeEstimate()
        {
            Element.ClearValue(AutocompleteCellControl.MeasuredEstimateProperty);
        }

        protected override void UpdateBackgroundColor()
        {
            base.UpdateBackgroundColor();

            if (Control == null)
            {
                return;
            }

            // By default some platforms have alternate default background colors when focused
            BrushHelpers.UpdateColor(Element.BackgroundColor, ref _backgroundColorFocusedDefaultBrush,
                () => Control.Background, brush => Control.Background = brush);
        }

        async void OnNativeTextChanged(object sender, Windows.UI.Xaml.Controls.AutoSuggestBoxTextChangedEventArgs args)
        {
            LastAutocompleteEntryUsed = Element;

            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput && Control.Text != "" && Control.Text.Length >= Element.CharCountBeforeAutocomplete)
            {
                Element.ItemsSource = null;

                if (Element.SuggestionProvider != null)
                {
                    var newSuggestions = await Element.SuggestionProvider.GetSuggestions(Control.Text);
                    Element.ItemsSource = newSuggestions;
                }
            }

            Element.SetValueCore(AutocompleteEntry.TextProperty, Control.Text);
        }

        private void OnNativeSuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            if (args.SelectedItem != null)
            {
                if (Element.SuggestionProvider != null)
                {
                    Control.Text = Element.SuggestionProvider.ConvertToString(args.SelectedItem);
                }
                else
                {
                    Control.Text = args.SelectedItem.ToString();
                }

                Element.SelectedItem = args.SelectedItem;
            }
        }

        void TextBoxOnKeyUp(object sender, KeyRoutedEventArgs args)
        {
            if (args?.Key != VirtualKey.Enter)
                return;

            #if WINDOWS_UWP
            // Hide the soft keyboard; this matches the behavior of Forms on Android/iOS
            Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();
            #else
			// WinRT doesn't have TryHide(), so the best we can do is force the control to unfocus
			UnfocusControl(Control);
            #endif

            ((IEntryController)Element).SendCompleted();
        }

        void UpdateAlignment()
        {
            //Control.HorizontalAlignment = Element.HorizontalTextAlignment.ToNativeHorizontalAlignment();
        }

        void UpdateFont()
        {
            if (Control == null)
                return;

            AutocompleteEntry entry = Element;

            if (entry == null)
                return;

            bool entryIsDefault = entry.FontFamily == null && entry.FontSize == Device.GetNamedSize(NamedSize.Default, typeof(AutocompleteEntry), true) && entry.FontAttributes == FontAttributes.None;

            if (entryIsDefault && !_fontApplied)
                return;

            if (entryIsDefault)
            {
                // ReSharper disable AccessToStaticMemberViaDerivedType
                // Resharper wants to simplify 'FormsTextBox' to 'Control', but then it'll conflict with the property 'Control'
                Control.ClearValue(FormsTextBox.FontStyleProperty);
                Control.ClearValue(FormsTextBox.FontSizeProperty);
                Control.ClearValue(FormsTextBox.FontFamilyProperty);
                Control.ClearValue(FormsTextBox.FontWeightProperty);
                Control.ClearValue(FormsTextBox.FontStretchProperty);
                // ReSharper restore AccessToStaticMemberViaDerivedType
            }
            else
            {
                Control.ApplyFont(entry);
            }

            _fontApplied = true;
        }

        void UpdateInputScope()
        {
            /*AutocompleteEntry entry = Element;
            var custom = entry.Keyboard as CustomKeyboard;
            if (custom != null)
            {
                Control.IsTextPredictionEnabled = (custom.Flags & KeyboardFlags.Suggestions) != 0;
                Control.IsSpellCheckEnabled = (custom.Flags & KeyboardFlags.Spellcheck) != 0;
            }
            else
            {
                Control.ClearValue(TextBox.IsTextPredictionEnabledProperty);
                if (entry.IsSet(AutocompleteEntry.IsSpellCheckEnabledProperty))
                    Control.IsSpellCheckEnabled = entry.IsSpellCheckEnabled;
                else
                    Control.ClearValue(TextBox.IsSpellCheckEnabledProperty);
            }

            Control.InputScope = entry.Keyboard.ToInputScope();*/
        }

        void UpdatePlaceholder()
        {
            Control.PlaceholderText = Element.Placeholder ?? "";
        }

        void UpdateText()
        {
            Control.Text = Element.Text ?? "";
        }

        void UpdateTextColor()
        {
            Color textColor = Element.TextColor;

            BrushHelpers.UpdateColor(textColor, ref _textDefaultBrush,
                () => Control.Foreground, brush => Control.Foreground = brush);
        }
    }
}