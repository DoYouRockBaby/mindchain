﻿using System.Net.Http;

namespace MindChain.DataProviders
{
    public class AbstractProvider
    {
        public const string BaseUrl = "192.168.0.30:64072";
        public const string Url = "http://" + BaseUrl + "/api/";
        protected HttpClient httpClient;

        public AbstractProvider(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        protected HttpClient GetClient()
        {
            return httpClient;
        }
    }
}
