using System;
using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using Android.Text;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.Lang;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Text.Method;
using ExtendedForms.Android.Extensions;

[assembly: ExportRenderer(typeof(AutocompleteEntry), typeof(ExtendedForms.Android.Renderers.AutocompleteEntryRenderer))]
namespace ExtendedForms.Android.Renderers
{
    public class AutocompleteEntryRenderer : ViewRenderer<AutocompleteEntry, AutoCompleteTextView>, ITextWatcher, TextView.IOnEditorActionListener
    {
        AutocompleteAdapter _adapter;
        ColorStateList _hintTextColorDefault;
        ColorStateList _textColorDefault;
        bool _disposed;
        //bool _isAttached;

        ITemplatedItemsView<Cell> TemplatedItemsView => Element;

        public AutocompleteEntryRenderer(Context context) : base(context)
        {
        }

        [Obsolete("This constructor is obsolete as of version 2.5. Please use EntryRenderer(Context) instead.")]
        public AutocompleteEntryRenderer()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;

            if (disposing)
            {
                if (_adapter != null)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
            }

            base.Dispose(disposing);
        }

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            //_isAttached = true;
            //_adapter.IsAttachedToWindow = _isAttached;
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();

            //_isAttached = false;
            //_adapter.IsAttachedToWindow = _isAttached;
        }


        bool TextView.IOnEditorActionListener.OnEditorAction(TextView v, ImeAction actionId, KeyEvent e)
        {
            // Fire Completed and dismiss keyboard for hardware / physical keyboards
            if (actionId == ImeAction.Done || (actionId == ImeAction.ImeNull && e.KeyCode == Keycode.Enter && e.Action == KeyEventActions.Up))
            {
                Control.ClearFocus();
                Context.HideKeyboard(v);
                ((IEntryController)Element).SendCompleted();
            }

            return true;
        }

        void ITextWatcher.AfterTextChanged(IEditable s)
        {
        }

        void ITextWatcher.BeforeTextChanged(ICharSequence s, int start, int count, int after)
        {
        }

        void ITextWatcher.OnTextChanged(ICharSequence s, int start, int before, int count)
        {
            if (string.IsNullOrEmpty(Element.Text) && s.Length() == 0)
                return;

            ((IElementController)Element).SetValueFromRenderer(AutocompleteEntry.TextProperty, s.ToString());
        }

        protected override AutoCompleteTextView CreateNativeControl()
        {
            return new AutoCompleteTextView(Context);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<AutocompleteEntry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                if (_adapter != null)
                {
                    _adapter.Dispose();
                    _adapter = null;
                }
            }

            if (e.NewElement != null)
            {
                AutoCompleteTextView nativeListView = Control;
                if (nativeListView == null)
                {
                    var ctx = Context;
                    nativeListView = CreateNativeControl();
                    nativeListView.AddTextChangedListener(this);
                    nativeListView.SetOnEditorActionListener(this);
                    nativeListView.Adapter = _adapter = new AutocompleteAdapter(Context, nativeListView, e.NewElement);

                    SetNativeControl(nativeListView);
                }

                Control.Hint = Element.Placeholder;
                Control.Text = Element.Text;
                UpdateInputType();

                UpdateColor();
                UpdateAlignment();
                UpdateFont();
                UpdatePlaceholderColor();
                UpdateInputType();
                UpdateKeyboardAction();
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AutocompleteEntry.PlaceholderProperty.PropertyName)
                Control.Hint = Element.Placeholder;
            else if (e.PropertyName == AutocompleteEntry.TextProperty.PropertyName)
            {
                if (Control.Text != Element.Text)
                {
                    Control.Text = Element.Text;
                    if (Control.IsFocused)
                    {
                        Control.SetSelection(Control.Text.Length);
                        //Control.ShowKeyboard();
                    }
                }
            }
            else if (e.PropertyName == AutocompleteEntry.TextColorProperty.PropertyName)
                UpdateColor();
            else if (e.PropertyName == AutocompleteEntry.HorizontalTextAlignmentProperty.PropertyName)
                UpdateAlignment();
            else if (e.PropertyName == AutocompleteEntry.FontAttributesProperty.PropertyName)
                UpdateFont();
            else if (e.PropertyName == AutocompleteEntry.FontFamilyProperty.PropertyName)
                UpdateFont();
            else if (e.PropertyName == AutocompleteEntry.FontSizeProperty.PropertyName)
                UpdateFont();
            else if (e.PropertyName == AutocompleteEntry.PlaceholderColorProperty.PropertyName)
                UpdatePlaceholderColor();
            else if (e.PropertyName == AutocompleteEntry.KeyboardProperty.PropertyName)
                UpdateInputType();
            else if (e.PropertyName == AutocompleteEntry.IsSpellCheckEnabledProperty.PropertyName)
                UpdateInputType();
            else if (e.PropertyName == AutocompleteEntry.KeyboardActionProperty.PropertyName)
                UpdateKeyboardAction();

            base.OnElementPropertyChanged(sender, e);
        }

        void UpdateAlignment()
        {
            //Control.Gravity = Element.HorizontalTextAlignment.ToHorizontalGravityFlags();
        }

        void UpdateColor()
        {
            if (Element.TextColor.IsDefault)
            {
                if (_textColorDefault == null)
                {
                    // This control has always had the default colors; nothing to update
                    return;
                }

                // This control is being set back to the default colors
                Control.SetTextColor(_textColorDefault);
            }
            else
            {
                if (_textColorDefault == null)
                {
                    // Keep track of the default colors so we can return to them later
                    // and so we can preserve the default disabled color
                    _textColorDefault = Control.TextColors;
                }

                Control.SetTextColor(Element.TextColor.ToAndroidPreserveDisabled(_textColorDefault));
            }
        }

        void UpdateFont()
        {
            //Control.Typeface = Element.ToTypeface();
            Control.SetTextSize(ComplexUnitType.Sp, (float)Element.FontSize);
        }

        void UpdateInputType()
        {
            var keyboard = Element.Keyboard;

            Control.InputType = keyboard.ToInputType();

            if (keyboard == Keyboard.Numeric)
            {
                Control.KeyListener = GetDigitsKeyListener(Control.InputType);
            }
        }

        void UpdateKeyboardAction()
        {
            if (Control != null)
            {
                Control.ImeOptions = Element.KeyboardAction.ToAndroid();
            }
        }

        protected virtual NumberKeyListener GetDigitsKeyListener(InputTypes inputTypes)
        {
            return LocalizedDigitsKeyListener.Create(inputTypes);
        }

        void UpdatePlaceholderColor()
        {
            Color placeholderColor = Element.PlaceholderColor;

            if (placeholderColor.IsDefault)
            {
                if (_hintTextColorDefault == null)
                {
                    // This control has always had the default colors; nothing to update
                    return;
                }

                // This control is being set back to the default colors
                Control.SetHintTextColor(_hintTextColorDefault);
            }
            else
            {
                if (_hintTextColorDefault == null)
                {
                    // Keep track of the default colors so we can return to them later
                    // and so we can preserve the default disabled color
                    _hintTextColorDefault = Control.HintTextColors;
                }

                Control.SetHintTextColor(placeholderColor.ToAndroidPreserveDisabled(_hintTextColorDefault));
            }
        }
    }
}