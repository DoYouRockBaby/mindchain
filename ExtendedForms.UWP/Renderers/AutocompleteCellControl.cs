﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.UWP;

namespace ExtendedForms.UWP.Renderers
{
    public class AutocompleteCellControl : ContentControl
    {
        public static readonly DependencyProperty CellProperty = DependencyProperty.Register("Cell", typeof(object), typeof(AutocompleteCellControl),
            new PropertyMetadata(null, (o, e) => ((AutocompleteCellControl)o).SetSource((Cell)e.OldValue, (Cell)e.NewValue)));

        internal static readonly BindableProperty MeasuredEstimateProperty = BindableProperty.Create("MeasuredEstimate", typeof(double), typeof(AutocompleteEntry), -1d);
        readonly Lazy<AutocompleteEntry> _listView;
        Windows.UI.Xaml.DataTemplate _currentTemplate;
        object _newValue;

        public AutocompleteCellControl()
        {
            _listView = new Lazy<AutocompleteEntry>(GetListView);

            DataContextChanged += OnDataContextChanged;

            Unloaded += (sender, args) =>
            {
                Cell?.SendDisappearing();
            };
        }

        public Cell Cell
        {
            get { return (Cell)GetValue(CellProperty); }
            set { SetValue(CellProperty, value); }
        }

        protected FrameworkElement CellContent
        {
            get { return (FrameworkElement)Content; }
        }

        protected override Windows.Foundation.Size MeasureOverride(Windows.Foundation.Size availableSize)
        {
            AutocompleteEntry lv = _listView.Value;

            // set the Cell now that we have a reference to the ListView, since it will have been skipped
            // on DataContextChanged.
            if (_newValue != null)
            {
                SetCell(_newValue);
                _newValue = null;
            }

            if (Content == null)
            {
                if (lv != null)
                {
                    var estimate = (double)lv.GetValue(MeasuredEstimateProperty);
                    if (estimate > -1)
                        return new Windows.Foundation.Size(availableSize.Width, estimate);
                }

                // This needs to return a size with a non-zero height; 
                // otherwise, it kills virtualization.
                return new Windows.Foundation.Size(0, Cell.DefaultCellHeight);
            }

            // Children still need measure called on them
            Windows.Foundation.Size result = base.MeasureOverride(availableSize);

            if (lv != null)
            {
                lv.SetValue(MeasuredEstimateProperty, result.Height);
            }

            return result;
        }

        AutocompleteEntry GetListView()
        {
            return AutocompleteEntryRenderer.LastAutocompleteEntryUsed;
        }

        Windows.UI.Xaml.DataTemplate GetTemplate(Cell cell)
        {
            var renderer = Registrar.Registered.GetHandlerForObject<ICellRenderer>(cell);
            return renderer.GetTemplate(cell);
        }

        void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            // We don't want to set the Cell until the ListView is realized, just in case the 
            // Cell has an ItemTemplate. Instead, we'll store the new data item, and it will be
            // set on MeasureOverrideDelegate. However, if the parent is a TableView, we'll already 
            // have a complete Cell object to work with, so we can move ahead.
            if (args.NewValue is Cell)
            {
                SetCell(args.NewValue);
            }
            else if (args.NewValue != null)
            {
                SetCell(args.NewValue);
                _newValue = args.NewValue;
            }
        }

        void SetCell(object newContext)
        {
            var cell = newContext as Cell;

            if (cell != null)
            {
                Cell = cell;
                return;
            }

            if (ReferenceEquals(Cell?.BindingContext, newContext))
                return;

            // If there is a ListView, load the Cell content from the ItemTemplate.
            // Otherwise, the given Cell is already a templated Cell from a TableView.
            AutocompleteEntry lv = _listView.Value;
            if (lv != null)
            {
                Xamarin.Forms.DataTemplate template = lv.ItemTemplate;
                object bindingContext = newContext;

                if (template is Xamarin.Forms.DataTemplateSelector)
                {
                    template = ((Xamarin.Forms.DataTemplateSelector)template).SelectTemplate(bindingContext, lv);
                }

                if (template != null)
                {
                    cell = RealizeItemTemplate(lv.TemplatedItems, template, newContext);
                }
                else
                {
                    cell = lv.CreateDefaultCell(bindingContext);
                }

                // A TableView cell should already have its parent,
                // but we need to set the parent for a ListView cell.
                cell.Parent = lv;

                // Set inherited BindingContext after setting the Parent so it won't be wiped out
                BindableObject.SetInheritedBindingContext(cell, bindingContext);
            }

            Cell = cell;
        }

        void SetSource(Cell oldCell, Cell newCell)
        {
            if (oldCell != null)
            {
                oldCell.SendDisappearing();
            }

            if (newCell != null)
            {
                newCell.SendAppearing();
                UpdateContent(newCell);
            }
        }

        void UpdateContent(Cell newCell)
        {
            Windows.UI.Xaml.DataTemplate dt = GetTemplate(newCell);
            if (dt != _currentTemplate || Content == null)
            {
                _currentTemplate = dt;
                Content = dt.LoadContent();
            }

            ((FrameworkElement)Content).DataContext = newCell;
        }

        static Cell RealizeItemTemplate(ITemplatedItemsList<Cell> templatedItems,
            ElementTemplate template, object context)
        {
            var index = templatedItems.GetGlobalIndexOfItem(context);
            if (index > -1)
            {
                return templatedItems[index];
            }

            return template.CreateContent() as Cell;
        }
    }
}
