﻿using Android.Support.V4.App;
using ExtendedForms.Android.Extensions;
using ExtendedForms.Android.Notifications;
using System.Threading.Tasks;

[assembly: ExportNotificationComponent(typeof(Xamarin.Notifications.ImageNotificationComponent), typeof(ImageNotificationComponentRenderer))]
namespace ExtendedForms.Android.Notifications
{
    public class ImageNotificationComponentRenderer : NotificationComponentRenderer<Xamarin.Notifications.ImageNotificationComponent>
    {
        public override async Task SetUpBuilderAsync(NotificationCompat.Builder builder)
        {
            if(Component.Format == Xamarin.Notifications.ImageNotificationComponent.ImageFormat.Big)
            {
                var bitmap = await Component.Source.ToBitmap(Forms.Forms.Context);
                builder.SetStyle(new NotificationCompat.BigPictureStyle().BigPicture(bitmap).BigLargeIcon(null));
            }
            else
            {
                var bitmap = await Component.Source.ToBitmap(Forms.Forms.Context);
                builder.SetLargeIcon(bitmap);
            }
        }
    }
}