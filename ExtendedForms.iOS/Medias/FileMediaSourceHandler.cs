﻿using ExtendedForms.iOS.Medias;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms.Internals;
using Xamarin.Medias;

[assembly: ExportMediaSourceHandler(typeof(FileMediaSource), typeof(FileMediaSourceHandler))]
namespace ExtendedForms.iOS.Medias
{
    public sealed class FileMediaSourceHandler : IMediaSourceHandler
    {
        public Task<UIImage> LoadImageAsync(MediaSource source, CancellationToken cancelationToken = default(CancellationToken), float scale = 1f)
        {
            UIImage image = null;
            var filesource = source as FileMediaSource;
            var file = filesource?.File;
            if (!string.IsNullOrEmpty(file))
                image = File.Exists(file) ? new UIImage(file) : UIImage.FromBundle(file);

            if (image == null)
            {
                Log.Warning(nameof(FileMediaSourceHandler), "Could not find image: {0}", source);
            }

            return Task.FromResult(image);
        }
    }
}