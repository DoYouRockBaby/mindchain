﻿using ExtendedForms.UWP.Renderers;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace ExtendedForms.UWP.Views
{
    public sealed partial class NativeRecyclerListView : UserControl, IDisposable
    {
        public ExtendedListViewRenderer RendererReference { get; set; }

        public class ItemsPanelParameters
        {
            public Orientation ScrollOrientation { get; set; } = Orientation.Vertical;
            public ScrollMode IsVerticalEnabled { get => ScrollOrientation == Orientation.Vertical ? ScrollMode.Enabled : ScrollMode.Disabled; }
            public ScrollMode IsHorizontalEnabled { get => ScrollOrientation == Orientation.Horizontal ? ScrollMode.Enabled : ScrollMode.Disabled; }
            public ScrollBarVisibility IsVerticalVisibility { get => ScrollOrientation == Orientation.Vertical ? ScrollBarVisibility.Visible : ScrollBarVisibility.Hidden; }
            public ScrollBarVisibility IsHorizontalVisibility { get => ScrollOrientation == Orientation.Horizontal ? ScrollBarVisibility.Visible : ScrollBarVisibility.Hidden; }
        }

        public ItemsPanelParameters ItemsPanelBindingContext { get; set; } = new ItemsPanelParameters();

        public NativeRecyclerListView(ExtendedListViewRenderer renderer)
        {
            RendererReference = renderer;

            DataContext = ItemsPanelBindingContext;
            InitializeComponent();

            SetupElement(renderer.Element as ExtendedListView);

            renderer.Element.PropertyChanged += Element_PropertyChanged;
            /*EmbeddedList.CanReorderItems = true;
            EmbeddedList.CanDragItems = true;
            EmbeddedList.AllowDrop = true;*/

            UpdateOrientation();
        }

        private void Element_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(ExtendedListView.Orientation):
                    UpdateOrientation();
                    break;
            }
        }

        private void UpdateOrientation()
        {
            ItemsPanelBindingContext.ScrollOrientation = RendererReference.Element.Orientation == ExtendedListView.ListViewOrientation.Vertical ? Orientation.Vertical : Orientation.Horizontal;
        }

        void SetupElement(ExtendedListView element)
        {
            if (element == null)
                return;

        }

        void DestroyElement(ExtendedListView element)
        {
            if (element == null)
                return;
        }

        void OnTemplateDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            var fbt = sender as FormsBoundTemplate;
            if (fbt == null) return;

            var dataContext = fbt.DataContext;
            if (dataContext == null) return;

            // Create
            var view = RendererReference.Element.ItemTemplate?.CreateContent() as View;
            view.BindingContext = dataContext;
            view.Parent = RendererReference.Element;

            if (fbt != null && view != null)
            {
                //Get renderer
                var renderer = Platform.CreateRenderer(view);

                //Set size
                var size = SizeRequest(view, RendererReference.Element.Width, RendererReference.Element.Height);
                view.Layout(new Rectangle(0, 0, size.Width, size.Height));

                // Append to Container
                fbt.Container.Children.Clear();
                fbt.Container.Children.Add(renderer.ContainerElement);
            }
        }

        public Size SizeRequest(Xamarin.Forms.View view, double widthConstraint, double heightConstraint)
        {
            if (view != null)
            {
                if (RendererReference.Element.Orientation == ExtendedListView.ListViewOrientation.Vertical)
                {
                    var measure = view.Measure(RendererReference.Element.Width, RendererReference.Element.Height);

                    var width = RendererReference.Element.ColumnWidth > 0 ? RendererReference.Element.ColumnWidth : widthConstraint / RendererReference.Element.RowOrColumnNumber;
                    var height = RendererReference.Element.RowHeight > 0 ? RendererReference.Element.RowHeight : measure.Request.Height;

                    return new Size(width, height);
                }
                else
                {
                    var measure = view.Measure(RendererReference.Element.Width, RendererReference.Element.Height);

                    var width = RendererReference.Element.ColumnWidth > 0 ? RendererReference.Element.ColumnWidth : measure.Request.Width;
                    var height = RendererReference.Element.RowHeight > 0 ? RendererReference.Element.RowHeight : heightConstraint / RendererReference.Element.RowOrColumnNumber;

                    return new Size(width, height);
                }
            }
            else
            {
                var width = RendererReference.Element.ColumnWidth > 0 ? RendererReference.Element.ColumnWidth / RendererReference.Element.RowOrColumnNumber : widthConstraint;
                var height = RendererReference.Element.RowHeight > 0 ? RendererReference.Element.RowHeight : 40.0;

                return new Size(width, height);
            }
        }

        public void Dispose()
        {
            DestroyElement(RendererReference?.Element as ExtendedListView);
        }
    }
}
