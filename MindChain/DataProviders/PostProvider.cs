﻿using MindChain.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public class PostProvider : AbstractProvider, IPostProvider
    {
        public PostProvider(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<IEnumerable<Post>> GetByAccount(Account account)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/account/" + account.UserName);
            return JsonConvert.DeserializeObject<IEnumerable<Post>>(result);
        }

        public async Task<IEnumerable<Post>> GetLocalizable()
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/account/localizable/");
            return JsonConvert.DeserializeObject<IEnumerable<Post>>(result);
        }

        public async Task<Post> Get(int id)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/" + id);
            return JsonConvert.DeserializeObject<Post>(result);
        }

        public async Task<Post> GetFirstFromUser(Account account)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/account/" + account.UserName + "/first/");
            return JsonConvert.DeserializeObject<Post>(result);
        }

        public async Task<Post> GetPreviousPost(Post post)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/" + post.Id + "/previous/");
            return JsonConvert.DeserializeObject<Post>(result);
        }

        public async Task<Post> GetNextPost(Post post)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "post/" + post.Id + "/next/");
            return JsonConvert.DeserializeObject<Post>(result);
        }

        public async Task Add(Post post)
        {
            HttpClient client = GetClient();
            await client.PostAsync(Url + "post",
                new StringContent(
                    JsonConvert.SerializeObject(post),
                    Encoding.UTF8, "application/json"));
        }

        public async Task Update(Post post)
        {
            HttpClient client = GetClient();
            await client.PutAsync(Url + "post/" + post.Id + "/",
                new StringContent(
                    JsonConvert.SerializeObject(post),
                    Encoding.UTF8, "application/json"));
        }

        public async Task Delete(Post post)
        {
            HttpClient client = GetClient();
            await client.DeleteAsync(Url + "post/" + post.Id + "/");
        }
    }
}
