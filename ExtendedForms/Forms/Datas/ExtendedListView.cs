﻿using System.Collections;

namespace Xamarin.Forms
{
    public class ExtendedListView : ListView
    {
        public enum ListViewOrientation
        {
            Vertical,
            Horizontal
        }

        public static readonly BindableProperty OrientationProperty = BindableProperty.Create(nameof(Orientation), typeof(ListViewOrientation), typeof(ExtendedListView), ListViewOrientation.Vertical, BindingMode.OneWay);
        public static readonly BindableProperty ColumnWidthProperty = BindableProperty.Create(nameof(ColumnWidth), typeof(int), typeof(ExtendedListView), default(int));
        public static readonly BindableProperty HasUnevenColumnsProperty = BindableProperty.Create(nameof(HasUnevenColumns), typeof(bool), typeof(ExtendedListView), true);
        public static readonly BindableProperty RowOrColumnNumberProperty = BindableProperty.Create(nameof(RowOrColumnNumber), typeof(int), typeof(ExtendedListView), 1);
        public static readonly BindableProperty ItemMarginProperty = BindableProperty.Create(nameof(ItemMargin), typeof(Thickness), typeof(ExtendedListView), default(Thickness), BindingMode.OneWay);

        public ListViewOrientation Orientation
        {
            get => (ListViewOrientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        public int ColumnWidth
        {
            get { return (int)GetValue(ColumnWidthProperty); }
            set { SetValue(ColumnWidthProperty, value); }
        }

        public bool HasUnevenColumns
        {
            get { return (bool)GetValue(HasUnevenColumnsProperty); }
            set { SetValue(HasUnevenColumnsProperty, value); }
        }

        public int RowOrColumnNumber
        {
            get { return (int)GetValue(RowOrColumnNumberProperty); }
            set { SetValue(RowOrColumnNumberProperty, value); }
        }

        public Thickness ItemMargin
        {
            get { return (Thickness)GetValue(ItemMarginProperty); }
            set { SetValue(ItemMarginProperty, value); }
        }
    }
}
