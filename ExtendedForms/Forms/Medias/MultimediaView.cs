﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms.Services;

namespace Xamarin.Forms
{
    public class MultimediaView : StackLayout
    {
        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(SourceProperty), typeof(ImageSource), typeof(MultimediaView), null, BindingMode.OneWay, null, HasChanged);
        public static readonly BindableProperty FileTypeProperty = BindableProperty.Create(nameof(FileTypeProperty), typeof(string), typeof(MultimediaView), null, BindingMode.OneWay, null, HasChanged);

        public static Dictionary<string, Func<MultimediaView, View>> Handlers = new Dictionary<string, Func<MultimediaView, View>>();

        static MultimediaView()
        {
            /*Handlers.Add(".avi", GenerateVideoView);
            Handlers.Add(".mp4", GenerateVideoView);
            Handlers.Add(".mkv", GenerateVideoView);*/
            Handlers.Add(".jpg", GenerateImageView);
            Handlers.Add(".jpeg", GenerateImageView);
            Handlers.Add(".gif", GenerateImageView);
            Handlers.Add(".bmp", GenerateImageView);
            Handlers.Add(".tiff", GenerateImageView);
            Handlers.Add(".png", GenerateImageView);
        }

        public ImageSource Source
        {
            get
            {
                return (ImageSource)GetValue(SourceProperty);
            }
            set
            {
                SetValue(SourceProperty, value);
            }
        }

        public string FileType
        {
            get
            {
                return (string)GetValue(FileTypeProperty);
            }
            set
            {
                SetValue(FileTypeProperty, value);
            }
        }

        public static void HasChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = (MultimediaView)bindable;
            view.Children.Clear();

            if(view.FileType == null || view.FileType == "" || view.Source == null)
            {
                return;
            }

            var handler = Handlers[view.FileType.ToLower()];
            if(handler != null)
            {
                view.Children.Add(handler.Invoke(view));
            }
        }

        /*private static View GenerateVideoView(MultimediaView multimediaView)
        {

            var source = "";

            if (multimediaView.Source is UriImageSource uriImageSource)
            {
                source = uriImageSource.Uri.OriginalString;
            }

            if (multimediaView.Source is FileImageSource fileImageSource)
            {
                source = fileImageSource.File;
            }

            if (multimediaView.Source is StreamImageSource streamImageSource)
            {
                var fileService = SimpleContainer.Instance.Create<IFileService>();

                CancellationToken cancellationToken = System.Threading.CancellationToken.None;
                Task<Stream> task = streamImageSource.Stream(cancellationToken);
                Stream stream = task.Result;

                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }

                    var filename = fileService.CreateTempFile(ms.ToArray(), multimediaView.FileType);
                    source = filename;
                }
            }

            return new Video
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Source = source,
                AspectMode = Plugin.MediaManager.Abstractions.Enums.VideoAspectMode.AspectFill,
                Play = true
            };
        }*/

        private static View GenerateImageView(MultimediaView multimediaView)
        {
            return new Image
            {
                Source = multimediaView.Source,
                Aspect = Aspect.AspectFit
            };
        }
    }
}
