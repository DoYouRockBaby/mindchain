﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MindChain.Models
{
    public class Group : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
                }
            }
        }

        ObservableCollection<Account> Accounts = new ObservableCollection<Account>();
    }
}
