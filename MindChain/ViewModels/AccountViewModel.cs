﻿using MindChain.DataProviders;
using MindChain.Helpers;
using MindChain.Models;
using MindChain.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    class AccountViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IAccountProvider accountProvider;
        private IPostProvider postProvider;

        public AccountViewModel(IAccountProvider accountProvider, IPostProvider postProvider)
        {
            this.accountProvider = accountProvider;
            this.postProvider = postProvider;

            LoadFriendsCommand = new Command(async () => await ExecuteLoadFriendsCommand());
            GoToFirstPostCommand = new Command(async () => CurrentPost = await postProvider.GetFirstFromUser(Account));
            GoToPreviousPostCommand = new Command(async () => await GoToPreviousPost());
            GoToNextPostCommand = new Command(async () => await GoToNextPost());
        }

        public Command LoadFriendsCommand { get; set; }
        public Command GoToFirstPostCommand { get; set; }
        public Command GoToPreviousPostCommand { get; set; }
        public Command GoToNextPostCommand { get; set; }

        private Account account = null;
        public Account Account
        {
            get
            {
                return account;
            }
            set
            {
                if (value != account)
                {
                    account = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Account)));
                    LoadFriendsCommand.Execute(null);
                    GoToFirstPostCommand.Execute(null);
                }
            }
        }

        private Post currentPost = null;
        public Post CurrentPost
        {
            get
            {
                return currentPost;
            }
            set
            {
                if (value != currentPost)
                {
                    currentPost = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentPost)));
                }
            }
        }

        private bool hasNextPost = true;
        public bool HasNextPost
        {
            get
            {
                return hasNextPost;
            }
            set
            {
                if (value != hasNextPost)
                {
                    hasNextPost = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasNextPost)));
                }
            }
        }

        private bool hasPreviousPost = true;
        public bool HasPreviousPost
        {
            get
            {
                return hasPreviousPost;
            }
            set
            {
                if (value != hasPreviousPost)
                {
                    hasPreviousPost = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasPreviousPost)));
                }
            }
        }

        bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                if (value != isBusy)
                {
                    isBusy = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsBusy)));
                }
            }
        }

        public async Task ExecuteLoadFriendsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Friends.Clear();
                var accounts = await accountProvider.GetFriends(Account);
                Friends.ReplaceRange(accounts);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private ObservableRangeCollection<Account> friends = new ObservableRangeCollection<Account>();
        public ObservableRangeCollection<Account> Friends
        {
            get
            {
                return friends;
            }
            set
            {
                if (value != friends)
                {
                    friends = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Friends)));
                }
            }
        }

        public async Task GoToPreviousPost()
        {
            if (CurrentPost != null)
            {
                var post = await postProvider.GetPreviousPost(CurrentPost);
                if (post != null)
                {
                    CurrentPost = post;
                    /*accountPost.HasNextPost = await postProvider.GetNextPost(post) != null;
                    accountPost.HasPreviousPost = await postProvider.GetPreviousPost(post) != null;*/
                }
            }
        }

        public async Task GoToNextPost()
        {
            if (CurrentPost != null)
            {
                var post = await postProvider.GetNextPost(CurrentPost);
                if (post != null)
                {
                    CurrentPost = post;
                    /*accountPost.HasNextPost = await postProvider.GetNextPost(post) != null;
                    accountPost.HasPreviousPost = await postProvider.GetPreviousPost(post) != null;*/
                }
            }
        }
    }
}
