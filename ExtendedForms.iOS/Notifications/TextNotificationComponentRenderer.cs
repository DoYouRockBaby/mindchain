﻿using ExtendedForms.iOS.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;
using UserNotifications;
using Xamarin.Notifications;

[assembly: ExportNotificationComponent(typeof(TextNotificationComponent), typeof(TextNotificationComponentRenderer))]
namespace ExtendedForms.iOS.Notifications
{
    public class TextNotificationComponentRenderer : NotificationComponentRenderer<TextNotificationComponent>
    {
        public override Task SetUpNotificationAsync(UNMutableNotificationContent notification, List<UNNotificationAction> actions)
        {
            if(Component.IsSubtext)
            {
                notification.Subtitle = Component.Text;
            }
            else
            {
                notification.Body = Component.Text;
            }

            return Task.CompletedTask;
        }
    }
}