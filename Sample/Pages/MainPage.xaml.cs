﻿using Sample.Pages.Autocomplete;
using Sample.Pages.ListView;
using Sample.Pages.Notification;
using Sample.ViewModels;
using Xamarin.Forms;
using static Sample.ViewModels.MainPageViewModel;

namespace Sample.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            var vm = new MainPageViewModel
            {
                SamplePages = new System.Collections.ObjectModel.ObservableCollection<SamplePage>
                {
                    new SamplePage
                    {
                        Name = "Notification",
                        Target = new MainNotification(),
                    },
                    new SamplePage
                    {
                        Name = "Autocomplete",
                        Target = new MainAutocomplete(),
                    },
                    new SamplePage
                    {
                        Name = "List View",
                        Target = new MainListView(),
                    }
                }
            };

            BindingContext = vm;

            InitializeComponent();
        }

        private void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as SamplePage;
            Navigation.PushAsync(item.Target);
        }
    }
}
