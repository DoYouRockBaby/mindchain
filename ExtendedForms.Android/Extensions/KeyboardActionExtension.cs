﻿using Android.Views.InputMethods;
using Xamarin.Forms;

namespace ExtendedForms.Android.Extensions
{
    public static class KeyboardActionExtension
    {
        public static ImeAction ToAndroid(this KeyboardAction self)
        {
            switch(self)
            {
                case KeyboardAction.Next:
                    return ImeAction.Next;
                case KeyboardAction.Search:
                    return ImeAction.Search;
                case KeyboardAction.Send:
                    return ImeAction.Send;
                case KeyboardAction.Go:
                    return ImeAction.Go;
                case KeyboardAction.Done:
                    return ImeAction.Done;
                default:
                    return ImeAction.Send;
            }
        }
    }
}