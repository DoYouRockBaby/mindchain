﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ExtendedForms.iOS.Extensions
{
    /*public static class FontExtension
    {
        static readonly Dictionary<ToUIFontKey, UIFont> ToUiFont = new Dictionary<ToUIFontKey, UIFont>();

        internal static UIFont ToUIFont(this IFontElement element)
        {
            return ToUIFont(element.FontFamily, (float)element.FontSize, element.FontAttributes);
        }

        static UIFont ToUIFont(string family, float size, FontAttributes attributes)
        {
            var key = new ToUIFontKey(family, size, attributes);

            lock (ToUiFont)
            {
                UIFont value;
                if (ToUiFont.TryGetValue(key, out value))
                    return value;
            }

            var generatedValue = _ToUIFont(family, size, attributes);

            lock (ToUiFont)
            {
                UIFont value;
                if (!ToUiFont.TryGetValue(key, out value))
                    ToUiFont.Add(key, value = generatedValue);
                return value;
            }
        }

        struct ToUIFontKey
        {
            internal ToUIFontKey(string family, float size, FontAttributes attributes)
            {
                _family = family;
                _size = size;
                _attributes = attributes;
            }
#pragma warning disable 0414 // these are not called explicitly, but they are used to establish uniqueness. allow it!
            string _family;
            float _size;
            FontAttributes _attributes;
#pragma warning restore 0414
        }

        static UIFont _ToUIFont(string family, float size, FontAttributes attributes)
        {
            var bold = (attributes & FontAttributes.Bold) != 0;
            var italic = (attributes & FontAttributes.Italic) != 0;

            if (family != null)
            {
                try
                {
                    UIFont result;
#if __MOBILE__
                    if (UIFont.FamilyNames.Contains(family))
                    {
                        var descriptor = new UIFontDescriptor().CreateWithFamily(family);

                        if (bold || italic)
                        {
                            var traits = (UIFontDescriptorSymbolicTraits)0;
                            if (bold)
                                traits = traits | UIFontDescriptorSymbolicTraits.Bold;
                            if (italic)
                                traits = traits | UIFontDescriptorSymbolicTraits.Italic;

                            descriptor = descriptor.CreateWithTraits(traits);
                            result = UIFont.FromDescriptor(descriptor, size);
                            if (result != null)
                                return result;
                        }
                    }

                    result = UIFont.FromName(family, size);
#else

					var descriptor = new NSFontDescriptor().FontDescriptorWithFamily(family);

					if (bold || italic)
					{
						var traits = (NSFontSymbolicTraits)0;
						if (bold)
							traits = traits | NSFontSymbolicTraits.BoldTrait;
						if (italic)
							traits = traits | NSFontSymbolicTraits.ItalicTrait;

						descriptor = descriptor.FontDescriptorWithSymbolicTraits(traits);
						result = NSFont.FromDescription(descriptor, size);
						if (result != null)
							return result;
					}

					result = NSFont.FromFontName(family, size);
#endif
                    if (result != null)
                        return result;
                }
                catch
                {
                    Debug.WriteLine("Could not load font named: {0}", family);
                }
            }

            if (bold && italic)
            {
                var defaultFont = UIFont.SystemFontOfSize(size);

#if __MOBILE__
                var descriptor = defaultFont.FontDescriptor.CreateWithTraits(UIFontDescriptorSymbolicTraits.Bold | UIFontDescriptorSymbolicTraits.Italic);
                return UIFont.FromDescriptor(descriptor, 0);
            }
            if (italic)
                return UIFont.ItalicSystemFontOfSize(size);
#else
				var descriptor = defaultFont.FontDescriptor.FontDescriptorWithSymbolicTraits(
					NSFontSymbolicTraits.BoldTrait |
					NSFontSymbolicTraits.ItalicTrait);

				return NSFont.FromDescription(descriptor, 0);
			}
			if (italic)
			{
				Debug.WriteLine("Italic font requested, passing regular one");
				return NSFont.UserFontOfSize(size);
			}
#endif
            if (bold)
                return UIFont.BoldSystemFontOfSize(size);

            return UIFont.SystemFontOfSize(size);
        }
    }*/
}