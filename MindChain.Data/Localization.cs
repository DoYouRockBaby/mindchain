﻿using System;

namespace MindChain.Data
{
    public class Localization
    {
        public string Id { get; set; }
        public string Place { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
