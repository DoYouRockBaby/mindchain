﻿using System;
using Xamarin.Forms;

namespace MindChain.Pages
{
    public class MasterPageItem
    {
        public string Title { get; set; }
        public ImageSource IconSource { get; set; }
        public Type TargetType { get; set; }
        public Command Command { get; set; }
        public object CommandParameter { get; set; }
    }
}
