﻿using ExtendedForms.UWP.Medias;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Medias;

[assembly: ExportMediaSourceHandler(typeof(StreamMediaSource), typeof(StreamMediaSourceHandler))]
namespace ExtendedForms.UWP.Medias
{
    public sealed class StreamMediaSourceHandler : IMediaSourceHandler
    {
        public async Task<Windows.UI.Xaml.Media.ImageSource> LoadImageAsync(MediaSource source, CancellationToken cancellationToken = new CancellationToken())
        {
            BitmapImage bitmapimage = null;

            if (source is StreamMediaSource streamsource && streamsource.Stream != null)
            {
                using (Stream stream = await streamsource.GetStreamAsync(cancellationToken))
                {
                    if (stream == null)
                        return null;
                    bitmapimage = new BitmapImage();
                    await bitmapimage.SetSourceAsync(stream.AsRandomAccessStream());
                }
            }

            return bitmapimage;
        }

        public async Task<string> GetOrCreateUriAsync(MediaSource source, CancellationToken cancellationToken = new CancellationToken())
        {
            var streamsource = source as StreamMediaSource;

            byte[] bytes;
            using (Stream stream = await streamsource.GetStreamAsync(cancellationToken).ConfigureAwait(false))
            {
                bytes = new byte[stream.Length];
                await stream.ReadAsync(bytes, 0, (int)stream.Length);
            }

            var fileService = SimpleContainer.Instance.Create<IFileService>();
            return fileService.CreateTempFile(bytes, ".tmp");
        }
    }
}
