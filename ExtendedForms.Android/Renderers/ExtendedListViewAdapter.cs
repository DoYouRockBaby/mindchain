﻿using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Support.V7.Widget;
using AView = Android.Views.View;
using AndroidResources = Android.Content.Res.Resources;
using System.Collections;
using static Android.Views.View;

namespace ExtendedForms.Android.Renderers
{
    internal class ExtendedListViewAdapter : RecyclerView.Adapter
    {
        readonly Context _context;
        public ExtendedListView ListView => _listView;
        protected readonly ExtendedListView _listView;
        private readonly IList _dataSource;
        readonly RecyclerView _realListView;
        protected int _listCount = -1;

        public override int ItemCount
        {
            get
            {
                return _dataSource.Count;
            }
        }

        public ExtendedListViewAdapter(Context context, RecyclerView realListView, ExtendedListView listView)
        {
            _context = context;
            _realListView = realListView;
            _listView = listView;
            _dataSource = listView.ItemsSource?.Cast<object>()?.ToList();
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            //Get binding context
            var item = (ViewHolder)holder;
            var dataContext = _dataSource[position];

            //Update binding context
            var renderer = item.Renderer;
            renderer.Element.BindingContext = dataContext;

            //Update Size
            var size = SizeRequest(item.View, _listView.Width, _listView.Height);
            item.View.Layout(new Rectangle(0, 0, size.Width * AndroidResources.System.DisplayMetrics.Density, size.Height * AndroidResources.System.DisplayMetrics.Density));
            item.AndroidView.LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent)
            {
                Width = (int)(size.Width * AndroidResources.System.DisplayMetrics.Density),
                Height = (int)(size.Height * AndroidResources.System.DisplayMetrics.Density)
            };
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            //Create Cell
            var dataTemplate = _listView.ItemTemplate;
            Xamarin.Forms.View view = dataTemplate?.CreateContent() as Xamarin.Forms.View;
            view.Parent = _listView;

            //Get or create renderer
            if (Platform.GetRenderer(view) == null)
            {
                Platform.SetRenderer(view, Platform.CreateRendererWithContext(view, _context));
            }
            var renderer = Platform.GetRenderer(view);
            var viewGroup = renderer.View;

            //Size elements
            var size = SizeRequest(view, _listView.Width, _listView.Height);
            view.Layout(new Rectangle(0, 0, size.Width * AndroidResources.System.DisplayMetrics.Density, size.Height * AndroidResources.System.DisplayMetrics.Density));

            viewGroup.LayoutParameters = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent)
            {
                Width = (int)(size.Width * AndroidResources.System.DisplayMetrics.Density),
                Height = (int)(size.Height * AndroidResources.System.DisplayMetrics.Density)
            };

            //Return view holder
            return new ViewHolder(view, viewGroup, renderer);
        }

        public Size SizeRequest(Xamarin.Forms.View view, double widthConstraint, double heightConstraint)
        {
            if (view != null)
            {
                if(ListView.Orientation == ExtendedListView.ListViewOrientation.Vertical)
                {
                    var width = _listView.ColumnWidth > 0 ? _listView.ColumnWidth : widthConstraint / ListView.RowOrColumnNumber;

                    double height = 0.0;
                    if (_listView.ColumnWidth > 0)
                    {
                        height = _listView.RowHeight;
                    }
                    else if (_listView.HasUnevenColumns)
                    {
                        var measure = view.Measure(_listView.Width, _listView.Height, MeasureFlags.IncludeMargins);
                        height = measure.Request.Height;
                    }
                    else
                    {
                        height = heightConstraint;
                    }

                    return new Xamarin.Forms.Size(width - _listView.ItemMargin.HorizontalThickness, height - _listView.ItemMargin.VerticalThickness);
                }
                else
                {
                    double width = 0.0;
                    if(_listView.ColumnWidth > 0)
                    {
                        width = _listView.ColumnWidth;
                    }
                    else if(_listView.HasUnevenColumns)
                    {
                        var measure = view.Measure(_listView.Width, _listView.Height, MeasureFlags.IncludeMargins);
                        width = measure.Request.Width;
                    }
                    else
                    {
                        width = widthConstraint;
                    }

                    var height = _listView.RowHeight > 0 ? _listView.RowHeight : heightConstraint / ListView.RowOrColumnNumber;

                    return new Xamarin.Forms.Size(width - _listView.ItemMargin.HorizontalThickness, height - _listView.ItemMargin.VerticalThickness);
                }
            }
            else
            {
                var width = _listView.ColumnWidth > 0 ? _listView.ColumnWidth : widthConstraint;
                var height = _listView.RowHeight > 0 ? _listView.RowHeight : 40.0;

                return new Xamarin.Forms.Size(width - _listView.ItemMargin.HorizontalThickness, height - _listView.ItemMargin.VerticalThickness);
            }
        }

        public class ViewHolder : RecyclerView.ViewHolder, IOnClickListener
        {
            public IVisualElementRenderer Renderer { get; private set; }
            public Xamarin.Forms.View View { get; private set; }
            public AView AndroidView { get; private set; }

            public ViewHolder(Xamarin.Forms.View view, AView androidView, IVisualElementRenderer renderer) : base(androidView)
            {
                View = view;
                AndroidView = androidView;
                Renderer = renderer;
                AndroidView.SetOnClickListener(this);
            }

            public void SetBindableObject(object o)
            {

            }

            public void OnClick(AView v)
            {
                //if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }
}