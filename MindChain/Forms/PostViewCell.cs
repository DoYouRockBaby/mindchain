﻿using MindChain.DataProviders;
using MindChain.Models;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.Forms
{
    public class PostViewCell : ViewCell, INotifyPropertyChanged
    {
        private IPostProvider postProvider;

        public static readonly BindableProperty PostProperty = BindableProperty.Create(nameof(PostProperty), typeof(Post), typeof(PostViewCell), null);

        public PostViewCell()
        {
            postProvider = SimpleContainer.Instance.Create<IPostProvider>();

            if (BindingContext != null)
            {
                if((Post)BindingContext != null)
                {
                    Post = (Post)BindingContext;
                }
                else if ((Account)BindingContext != null)
                {
                    Account = (Account)BindingContext;
                }
            }
        }

        public Post Post
        {
            get
            {
                return (Post)GetValue(PostProperty);
            }
            set
            {
                SetValue(PostProperty, value);
                BindingContext = value;
            }
        }

        public Account Account
        {
            set
            {
                if(value != null)
                {
                    new Command(async (object vc) => { ((PostViewCell)vc).Post = await postProvider.GetFirstFromUser(value); }).Execute(this);
                }
                else
                {
                    Post = null;
                }
            }
        }

        protected async Task GoToPreviousPost()
        {
            Post = await postProvider.GetPreviousPost(Post);
        }

        protected async Task GoToNextPost()
        {
            Post = await postProvider.GetNextPost(Post);
        }
    }
}
