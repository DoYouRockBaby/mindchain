﻿using MindChain.DataProviders;
using MindChain.Models;
using MindChain.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyAccountPage : ContentPage
    {
        private AccountViewModel viewModel;

        public MyAccountPage()
        {
            var accountProvider = SimpleContainer.Instance.Create<AccountProvider>();

            viewModel = SimpleContainer.Instance.Create<AccountViewModel>();
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            InitializeComponent();

            new Command(async () => viewModel.Account = await accountProvider.CurrentUser()).Execute(null);
        }

        public Account Account
        {
            get
            {
                return viewModel.Account;
            }
        }

        private async Task FriendSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var listView = sender as ListView;
            listView.SelectedItem = null;

            var account = e.SelectedItem as Account;
            await viewModel.ShowAccount(account);
        }
    }
}