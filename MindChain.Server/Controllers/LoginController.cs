﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MindChain.Data;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace MindChain.Server.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private Context context;
        private readonly UserManager<Account> userManager;
        private readonly SignInManager<Account> signInManager;

        public LoginController(Context context, UserManager<Account> userManager, SignInManager<Account> signInManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpPost("register")]
        public async Task<bool> Register([FromBody]RegisterRequest request)
        {
            var result = await userManager.CreateAsync(request.Account, request.Password);
            return result.Succeeded;
        }

        [HttpGet("current")]
        public Account CurrentUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return null;
            }
            else
            {
                var result = context.Accounts.FirstOrDefault(t => t.UserName == User.Identity.Name);
                return result;
            }
        }

        [HttpPost]
        public async Task<bool> Login([FromBody]LoginRequest request)
        {
            var storedAccount = context.Accounts.FirstOrDefault(t => t.UserName == request.UserName);

            if (storedAccount == null)
            {
                return false;
            }

            var result = await signInManager.PasswordSignInAsync(storedAccount, request.Password, false, true);
            return result.Succeeded;
        }
    }
}