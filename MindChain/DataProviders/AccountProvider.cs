﻿using MindChain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public class AccountProvider : AbstractProvider, IAccountProvider
    {
        public static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
        {
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public AccountProvider(HttpClient httpClient) : base(httpClient)
        {
        }

        public async Task<Account> CurrentUser()
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "login/current/");
            return JsonConvert.DeserializeObject<Account>(result);
        }

        public async Task<bool> Login(string username, string password)
        {
            HttpClient client = GetClient();

            var request = new LoginRequest
            {
                UserName = username,
                Password = password
            };

            var response = await client.PostAsync(Url + "login/",
                new StringContent(
                    JsonConvert.SerializeObject(request),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Boolean>(await response.Content.ReadAsStringAsync());
        }

        public async Task<bool> Register(Account account, string password)
        {
            HttpClient client = GetClient();

            var request = new RegisterRequest
            {
                Account = account,
                Password = password
            };

            var response = await client.PostAsync(Url + "login/register/",
                new StringContent(
                    JsonConvert.SerializeObject(request),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Boolean>(
                await response.Content.ReadAsStringAsync());
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "account/");
            return JsonConvert.DeserializeObject<IEnumerable<Account>>(result);
        }

        public async Task<Account> Get(string username)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "account/" + username + "/");
            return JsonConvert.DeserializeObject<Account>(result);
        }

        public async Task<IEnumerable<Account>> GetFriends(Account account)
        {
            HttpClient client = GetClient();
            string result = await client.GetStringAsync(Url + "account/" + account.UserName + "/friends/");
            return JsonConvert.DeserializeObject<List<Account>>(result);
        }

        public async Task<IEnumerable<Account>> Search(string request)
        {
            if (request.Length < 3)
            {
                return new List<Account>();
            }

            HttpClient client = GetClient();
            var response = await client.PostAsync(Url + "account/search/",
                new StringContent(JsonConvert.SerializeObject(request),
                Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<List<Account>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<bool> AskFriend(string username)
        {
            HttpClient client = GetClient();
            var response = await client.PostAsync(Url + "account/ask/friend/",
                new StringContent(JsonConvert.SerializeObject(username),
                Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<bool>(await response.Content.ReadAsStringAsync());
        }

        public async Task<bool> AddFriendByCode(string code)
        {
            HttpClient client = GetClient();
            var response = await client.PostAsync(Url + "account/add/friend/",
                new StringContent(JsonConvert.SerializeObject(code),
                Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<bool>(await response.Content.ReadAsStringAsync());
        }

        public async Task Update(Account account)
        {
            HttpClient client = GetClient();
            await client.PutAsync(Url + "account/" + account.UserName + "/",
                new StringContent(
                    JsonConvert.SerializeObject(account),
                    Encoding.UTF8, "application/json"));
        }

        public async Task Delete(Account account)
        {
            HttpClient client = GetClient();
            await client.DeleteAsync(Url + "account/" + account.UserName + "/");
        }
    }
}
