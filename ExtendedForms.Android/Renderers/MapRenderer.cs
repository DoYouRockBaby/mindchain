﻿using Android.Gms.Maps;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using Android.Gms.Maps.Model;
using Java.Lang;
using Android.OS;
using System.Collections.Generic;
using ExtendedForms.Android.Extensions;
using Com.Google.Maps.Android.Clustering;
using static Xamarin.Forms.Map;

[assembly: ExportRenderer(typeof(Map), typeof(ExtendedForms.Android.Renderers.MapRenderer))]
namespace ExtendedForms.Android.Renderers
{
    [Preserve(AllMembers = true)]
    public class MapRenderer : ViewRenderer<Map, MapView>, GoogleMap.IOnCameraMoveListener, IOnMapReadyCallback
    {
        private MapView MapControl
        {
            get { return Control as MapView; }
        }

        public Map MapView
        {
            get { return Element as Map; }
        }

        IMapController Controller => Element;

        protected GoogleMap NativeMap;
        private bool _init = true;
        private static Bundle bundle;
        readonly Dictionary<Pin, CustomMarker> _markers = new Dictionary<Pin, CustomMarker>();
        ClusterManager _clusterManager;

        public MapRenderer(Context context) : base(context)
        {
        }

        /// <summary>
        /// Dummy function to avoid linker.
        /// </summary>
        [Preserve]
        public static void InitMapRenderer(Bundle bundle)
        {
            MapRenderer.bundle = bundle;
        }

        /// <inheritdoc/>
        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null && MapControl != null)
            {
                if (NativeMap != null)
                {
                    NativeMap.MyLocationEnabled = false;
                    NativeMap.SetOnCameraMoveListener(null);

                    NativeMap.InfoWindowClick -= MapOnMarkerClick;
                    NativeMap.MapClick -= MapTapped;
                    NativeMap.MapLongClick -= MapHolding;
                    NativeMap.MyLocationChange -= UserLocationChanged;

                    if(_clusterManager != null)
                    {
                        _clusterManager?.Dispose();
                        _clusterManager = null;
                    }

                    NativeMap.Dispose();
                    NativeMap = null;
                }
            }

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    var mapView = new MapView(Context);
                    mapView.OnCreate(bundle);
                    mapView.OnResume();

                    SetNativeControl(mapView);
                }

                Control.GetMapAsync(this);

            }
        }

        /// <inheritdoc/>
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case nameof(Map.MapRegion):
                    //UpdateMapRegion();
                    break;
                case nameof(Map.MapType):
                    UpdateMapType();
                    break;
                case nameof(Map.IsShowingUser):
                    UpdateIsShowingUser(true);
                    break;
                case nameof(Map.HasClusteringEnabled):
                    UpdateHasClusteringEnabled();
                    break;
                case nameof(Map.HasScrollEnabled):
                    UpdateHasScrollEnabled();
                    break;
                case nameof(Map.HasZoomEnabled):
                    UpdateHasZoomEnabled();
                    break;
            }
        }

        /// <inheritdoc/>
        public override SizeRequest GetDesiredSize(int widthConstraint, int heightConstraint)
        {
            return new SizeRequest(new Size(Context.ToPixels(40), Context.ToPixels(40)));
        }

        /// <inheritdoc/>
        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            if (_init)
            {
                if (NativeMap != null)
                {
                    MoveToRegion(Element.MapRegion, false);
                    OnCollectionChanged(Element.Pins, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    _init = false;
                }
            }
            else if (changed)
            {
                if (NativeMap != null)
                {
                    UpdateVisibleRegion(NativeMap.CameraPosition.Target);
                }
                MoveToRegion(Element.MapRegion, false);
            }
        }

        /// <summary>
        /// Called when map is ready to use
        /// </summary>
        /// <param name="map">Native map object</param>
        protected virtual void OnMapReady(GoogleMap map)
        {
            if (map == null)
            {
                return;
            }

            map.SetOnCameraMoveListener(this);
            map.InfoWindowClick += MapOnMarkerClick;
            map.MapClick += MapTapped;
            map.MapLongClick += MapHolding;
            map.MyLocationChange += UserLocationChanged;

            MessagingCenter.Subscribe<Map, MoveToRegionArgs>(this, "MapMoveToRegion", (sender, args) => MoveToRegion(args.Region, args.Animate));

            UpdateMapRegion();
            UpdateMapType();
            UpdateIsShowingUser(true);
            UpdateHasClusteringEnabled();
            UpdateHasScrollEnabled();
            UpdateHasZoomEnabled();

            ((ObservableCollection<Pin>)MapView.Pins).CollectionChanged += OnCollectionChanged;

            foreach(var pin in MapView.Pins)
            {
                AddPin(pin);
            }
        }

        /// <summary>
        /// When the map is clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapTapped(object sender, GoogleMap.MapClickEventArgs e)
        {
            Controller.SendMapClicked(e.Point.ToPosition());
        }

        /// <summary>
        /// When the map is long clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapHolding(object sender, GoogleMap.MapLongClickEventArgs e)
        {
            Controller.SendMapLongPress(e.Point.ToPosition());
        }

        /// <summary>
        /// When the user location changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void UserLocationChanged(object sender, GoogleMap.MyLocationChangeEventArgs e)
        {
            if (e.Location == null || MapView == null) return;

            var newPosition = new Position(e.Location.Latitude, e.Location.Longitude);
            Controller.SendUserLocationChanged(newPosition);
        }

        /// <summary>
        /// When a marker on the map is clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        void MapOnMarkerClick(object sender, GoogleMap.InfoWindowClickEventArgs eventArgs)
        {
            // clicked marker
            var marker = eventArgs.Marker;

            // lookup pin
            Pin targetPin = null;
            for (var i = 0; i < MapView.Pins.Count; i++)
            {
                Pin pin = MapView.Pins[i];
                if ((string)pin.Id != marker.Id)
                {
                    continue;
                }

                targetPin = pin;
                break;
            }

            // only consider event handled if a handler is present. 
            // Else allow default behavior of displaying an info window.
            targetPin?.Command?.Execute(targetPin);
        }

        /// <summary>
        /// Move the map view
        /// </summary>
        /// <param name="region">Target region</param>
        /// <param name="animate">Does animate the region movement</param>
        public void MoveToRegion(MapSpan region, bool animate)
        {
            animate = false;
            if (MapControl == null || region == null) return;

            region = region.ClampLatitude(85, -85);
            var ne = new LatLng(region.Center.Latitude + region.LatitudeDegrees / 2,
                region.Center.Longitude + region.LongitudeDegrees / 2);
            var sw = new LatLng(region.Center.Latitude - region.LatitudeDegrees / 2,
                region.Center.Longitude - region.LongitudeDegrees / 2);
            CameraUpdate update = CameraUpdateFactory.NewLatLngBounds(new LatLngBounds(sw, ne), 0);

            try
            {
                if (animate)
                {
                    NativeMap.AnimateCamera(update);
                }
                else
                {
                    NativeMap.MoveCamera(update);
                }
            }
            catch (IllegalStateException exc)
            {
                System.Diagnostics.Debug.WriteLine("MoveToRegion exception: " + exc);
                Log.Warning("Xamarin.Forms MapRenderer", $"MoveToRegion exception: {exc}");
            }
        }

        /// <summary>
        /// When the collection of pins changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
		void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Pin pin in e.NewItems)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Pin pin in e.OldItems)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach (Pin pin in e.OldItems)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    foreach (Pin pin in e.NewItems)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    foreach (Pin pin in MapView.Pins)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    foreach (Pin pin in MapView.Pins)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    //do nothing
                    break;
            }
        }

        /// <summary>
        /// Adds a marker to the map
        /// </summary>
        /// <param name="pin">The Forms Pin</param>
        async void AddPin(Pin pin)
        {
            if (_markers.Keys.Contains(pin)) return;

            var marker = new CustomMarker(pin, Context);
            var markerWithIcon = new MarkerOptions();
            await marker.InitializeMarkerOptionsAsync(markerWithIcon);

            _markers.Add(pin, marker);

            if (MapView.HasClusteringEnabled)
            {
                _clusterManager.AddItem(marker);
                _clusterManager.Cluster();
            }
            else
            {
                marker.Marker = NativeMap.AddMarker(markerWithIcon);
            }
        }

        /// <summary>
        /// Remove a pin from the map and the internal dictionary
        /// </summary>
        /// <param name="pin">The pin to remove</param>
        /// <param name="removeMarker">true to remove the marker from the map</param>
        void RemovePin(Pin pin, bool removeMarker = true)
        {
            if (!_markers.TryGetValue(pin, out var item)) return;

            _clusterManager?.RemoveItem(item);
            item.Marker?.Remove();

            if (removeMarker)
            {
                _markers.Remove(pin);
            }
        }

        /// <summary>
        /// Generate android marker options
        /// </summary>
        /// <param name="pin">The referenced pin</param>
        protected virtual MarkerOptions CreateMarker(Pin pin)
        {
            var opts = new MarkerOptions();
            opts.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            opts.SetTitle(pin.Title);
            opts.SetSnippet(pin.Subtitle);

            return opts;
        }

        /// <summary>
        /// Updates the map type
        /// </summary>
        void UpdateMapType()
        {
            GoogleMap map = NativeMap;
            if (map == null)
            {
                return;
            }

            switch (MapView.MapType)
            {
                case MapType.Street:
                    map.MapType = GoogleMap.MapTypeNormal;
                    break;
                case MapType.Satellite:
                    map.MapType = GoogleMap.MapTypeSatellite;
                    break;
                case MapType.Hybrid:
                    map.MapType = GoogleMap.MapTypeHybrid;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Updates the map region when changed
        /// </summary>
        private void UpdateMapRegion()
        {
            if (MapView == null || MapView.MapRegion == null) return;

            MoveToRegion(MapView.MapRegion, false);
        }

        /// <summary>
        /// Hide or show user when needed
        /// </summary>
        /// <param name="moveToLocation">Does move to location when user found</param>
        void UpdateIsShowingUser(bool moveToLocation = true)
        {
            if (NativeMap == null)
            {
                return;
            }

            NativeMap.MyLocationEnabled = NativeMap.UiSettings.MyLocationButtonEnabled = MapView.IsShowingUser;
        }

        /// <summary>
        /// Enable or disable clustering when channeededged
        /// </summary>
        void UpdateHasClusteringEnabled()
        {
            if (MapView.HasClusteringEnabled)
            {
                if (_clusterManager == null)
                {
                    _clusterManager = new ClusterManager(Context, NativeMap);
                    _clusterManager.Renderer = new MarkerRenderer(Context, NativeMap, _clusterManager, this);
                }

                foreach (var marker in _markers.ToList())
                {
                    RemovePin(marker.Key);
                    AddPin(marker.Key);
                }

                _clusterManager.Cluster();
            }
            else
            {
                if(_clusterManager != null)
                {
                    foreach (var marker in _markers.ToList())
                    {
                        RemovePin(marker.Key);
                        AddPin(marker.Key);
                    }

                    _clusterManager.Cluster();

                    _clusterManager.Dispose();
                    _clusterManager = null;
                }
            }
        }

        /// <summary>
        /// Enable or disable scroll when needed
        /// </summary>
        void UpdateHasScrollEnabled()
        {
            if (NativeMap == null)
            {
                return;
            }

            NativeMap.UiSettings.ScrollGesturesEnabled = MapView.HasScrollEnabled;
        }

        /// <summary>
        /// Enable or disable zoom when channeededged
        /// </summary>
        void UpdateHasZoomEnabled()
        {
            if (NativeMap == null)
            {
                return;
            }

            NativeMap.UiSettings.ZoomControlsEnabled = MapView.HasZoomEnabled;
            NativeMap.UiSettings.ZoomGesturesEnabled = MapView.HasZoomEnabled;
        }

        /// <inheritdoc/>
        void UpdateVisibleRegion(LatLng pos)
        {
            GoogleMap map = NativeMap;
            if (map == null)
            {
                return;
            }

            if (MapView.HasClusteringEnabled)
            {
                _clusterManager.OnCameraIdle();
            }

            Projection projection = map.Projection;
            int width = Control.Width;
            int height = Control.Height;
            LatLng ul = projection.FromScreenLocation(new global::Android.Graphics.Point(0, 0));
            LatLng ur = projection.FromScreenLocation(new global::Android.Graphics.Point(width, 0));
            LatLng ll = projection.FromScreenLocation(new global::Android.Graphics.Point(0, height));
            LatLng lr = projection.FromScreenLocation(new global::Android.Graphics.Point(width, height));
            double dlat = System.Math.Max(System.Math.Abs(ul.Latitude - lr.Latitude), System.Math.Abs(ur.Latitude - ll.Latitude));
            double dlong = System.Math.Max(System.Math.Abs(ul.Longitude - lr.Longitude), System.Math.Abs(ur.Longitude - ll.Longitude));

            Element.SendRegionChanged(new MapSpan(new Position(pos.Latitude, pos.Longitude), dlat, dlong));
        }

        /// <inheritdoc/>
        void IOnMapReadyCallback.OnMapReady(GoogleMap map)
        {
            NativeMap = map;
            OnMapReady(map);
        }

        /// <inheritdoc/>
        void GoogleMap.IOnCameraMoveListener.OnCameraMove()
        {
            UpdateVisibleRegion(NativeMap.CameraPosition.Target);
        }
    }
}