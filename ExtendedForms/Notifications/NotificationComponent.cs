﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Xamarin.Notifications
{
    public class NotificationComponent : Element, IResourcesProvider
    {
        ResourceDictionary _resources;

        public ResourceDictionary Resources
        {
            get
            {
                if (_resources != null)
                    return _resources;
                _resources = new ResourceDictionary();
                ((IResourceDictionary)_resources).ValuesChanged += OnResourcesChanged;
                return _resources;
            }
            set
            {
                if (_resources == value)
                    return;
                OnPropertyChanging();
                if (_resources != null)
                    ((IResourceDictionary)_resources).ValuesChanged -= OnResourcesChanged;
                _resources = value;
                OnResourcesChanged(value);
                if (_resources != null)
                    ((IResourceDictionary)_resources).ValuesChanged += OnResourcesChanged;
                OnPropertyChanged();
            }
        }
    }
}
