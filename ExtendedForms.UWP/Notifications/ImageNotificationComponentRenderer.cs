﻿using ExtendedForms.UWP.Medias;
using ExtendedForms.UWP.Notifications;
using Microsoft.Toolkit.Uwp.Notifications;
using System.Threading.Tasks;

[assembly: ExportNotificationComponent(typeof(Xamarin.Notifications.ImageNotificationComponent), typeof(ImageNotificationComponentRenderer))]
namespace ExtendedForms.UWP.Notifications
{
    public class ImageNotificationComponentRenderer : NotificationComponentRenderer<Xamarin.Notifications.ImageNotificationComponent>
    {
        public override async Task SetUpNotificationAsync(ToastContent content, ToastBindingGeneric bindingGeneric)
        {
            if (Component.Format == Xamarin.Notifications.ImageNotificationComponent.ImageFormat.Big)
            {
                var handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(Component.Source);
                bindingGeneric.Children.Add(new AdaptiveImage()
                {
                    Source = await handler.GetOrCreateUriAsync(Component.Source)
                });
            }
            else
            {
                var handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(Component.Source);
                bindingGeneric.HeroImage = new ToastGenericHeroImage()
                {
                    Source = await handler.GetOrCreateUriAsync(Component.Source)
                };
            }
        }
    }
}