﻿using System;
using Xamarin.Forms;

namespace ExtendedForms.Android.Notifications
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public sealed class ExportNotificationComponentAttribute : HandlerAttribute
    {
        public ExportNotificationComponentAttribute(Type handler, Type target) : base(handler, target)
		{
        }
    }
}