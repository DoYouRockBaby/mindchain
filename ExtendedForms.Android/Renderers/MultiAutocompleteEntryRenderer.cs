/*using System;
using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using Android.Text;
using Android.Text.Method;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Java.Lang;
using ExtendedForms.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(MultiAutocompleteEntry), typeof(ExtendedForms.Android.Renderers.MultiAutocompleteEntryRenderer))]
namespace ExtendedForms.Android.Renderers
{
    public class MultiAutocompleteEntryRenderer : AutocompleteEntryRenderer
    {
        public MultiAutocompleteEntryRenderer(Context context) : base(context)
        {
        }

        [Obsolete("This constructor is obsolete as of version 2.5. Please use EntryRenderer(Context) instead.")]
        public MultiAutocompleteEntryRenderer()
        {
        }

        protected override AutoCompleteTextView CreateNativeControl()
        {
            var control = new MultiAutoCompleteTextView(Context)
            {
                Adapter = new AutocompleteAdapter(Context, Control, Element)
            };
            control.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

            return control;
        }
    }
}*/