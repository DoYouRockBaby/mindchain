﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MindChain.Data;

namespace MindChain.Server
{
    public class Context : IdentityDbContext<Account>
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Friendship> Friendships { get; set; }
        public DbSet<Post> Posts { get; set; }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>().ToTable("Account");
            modelBuilder.Entity<Account>().HasKey(x => x.Id);
            modelBuilder.Entity<Account>().Property(x => x.Code).IsRequired();
            modelBuilder.Entity<Account>().Property(x => x.UserName).IsRequired();
            modelBuilder.Entity<Account>().Property(x => x.PasswordHash).IsRequired();
            modelBuilder.Entity<Account>().Property(x => x.FirstName).IsRequired();
            modelBuilder.Entity<Account>().Property(x => x.LastName).IsRequired();
            modelBuilder.Entity<Account>().Property(x => x.Gender).IsRequired();

            modelBuilder.Entity<Friendship>().ToTable("Friendship");
            modelBuilder.Entity<Friendship>().HasKey(x => x.Id);
            modelBuilder.Entity<Friendship>().Property(x => x.Date).IsRequired();

            modelBuilder.Entity<Post>().ToTable("Post");
            modelBuilder.Entity<Post>().HasKey(x => x.Id);
            modelBuilder.Entity<Post>().Property(x => x.Text).IsRequired();
            modelBuilder.Entity<Post>().Property(x => x.Date).IsRequired();
            modelBuilder.Entity<Post>().HasOne(x => x.Account)
                .WithMany()
                .HasForeignKey(x => x.AccountId);

            modelBuilder.Entity<FileUrl>().ToTable("FileUrl");
            modelBuilder.Entity<FileUrl>().HasKey(x => x.Id);

            modelBuilder.Entity<Localization>().ToTable("Localization");
            modelBuilder.Entity<Localization>().HasKey(x => x.Id);
        }
    }
}
