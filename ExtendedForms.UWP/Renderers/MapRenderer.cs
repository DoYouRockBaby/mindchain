﻿using ExtendedForms.Forms;
using ExtendedForms.UWP.Controls;
using ExtendedForms.UWP.Extensions;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.UWP;
using static Xamarin.Forms.Map;

[assembly: ExportRenderer(typeof(Map), typeof(ExtendedForms.UWP.Renderers.MapRenderer))]
namespace ExtendedForms.UWP.Renderers
{
    [Preserve(AllMembers = true)]
    public class MapRenderer : ViewRenderer<Map, MapControl>
    {
        Ellipse _userPositionCircle;
        DispatcherTimer _timer;

        internal static string AuthenticationToken = "";

        private MapControl MapControl
        {
            get { return Control as MapControl; }
        }

        private Map MapView
        {
            get { return Element as Map; }
        }

        IMapController Controller => Element;

        /// <summary>
        /// Dummy function to avoid linker.
        /// </summary>
        [Preserve]
        public static void InitMapRenderer(string authenticationToken)
        {
            AuthenticationToken = authenticationToken;
            var temp = DateTime.Now;
        }

        /// <inheritdoc/>
        protected override async void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null && MapControl != null)
            {
                MapControl.MapTapped -= MapTapped;
                MapControl.MapHolding -= MapHolding;
            }

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    SetNativeControl(new MapControl());
                    Control.MapServiceToken = AuthenticationToken;
                    MapControl.CenterChanged += MapCenterChanged;
                    MapControl.ZoomLevelChanged += MapZoomLevelChanged;
                    MapControl.MapTapped += MapTapped;
                    MapControl.MapHolding += MapHolding;
                }

                MessagingCenter.Subscribe<Map, MoveToRegionArgs>(this, "MapMoveToRegion", async (sender, args) => await MoveToRegion(args.Region, args.Animate));

                await UpdateMapRegion();
                UpdateMapType();
                await UpdateIsShowingUser(true);
                UpdateHasClusteringEnabled();
                UpdateHasScrollEnabled();
                UpdateHasZoomEnabled();

                ((ObservableCollection<Pin>)MapView.Pins).CollectionChanged += OnCollectionChanged;

                if (MapView.Pins.Any())
                    LoadPins();
            }
        }

        private void MapZoomLevelChanged(MapControl sender, object args)
        {
            RegionChanged();
        }

        private void MapCenterChanged(MapControl sender, object args)
        {
            RegionChanged();
        }

        private void RegionChanged()
        {
            var visibleRegion = Control.GetVisibleRegion(MapVisibleRegionKind.Full);

            if(visibleRegion == null)
            {
                return;
            }

            var corners = visibleRegion.Positions;

            var minlat = double.MaxValue;
            var maxlat = double.MinValue;
            var minlng = double.MaxValue;
            var maxlng = double.MinValue;

            foreach (var p in corners)
            {
                minlat = Math.Min(p.Latitude, minlat);
                maxlat = Math.Max(p.Latitude, maxlat);
                minlng = Math.Min(p.Longitude, minlng);
                maxlng = Math.Max(p.Longitude, maxlng);
            }

            var center = new Position((minlat + maxlat) / 2, (minlng + maxlng) / 2);
            var latitudeDelta = Math.Abs(maxlat - minlat);
            var longitudeDelta = Math.Abs(maxlng - minlng);

            Element.SendRegionChanged(new MapSpan(center, latitudeDelta, longitudeDelta));
        }

        /// <inheritdoc/>
        protected override async void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case nameof(Map.MapRegion):
                    await UpdateMapRegion();
                    break;
                case nameof(Map.MapType):
                    UpdateMapType();
                    break;
                case nameof(Map.IsShowingUser):
                    await UpdateIsShowingUser(true);
                    break;
                case nameof(Map.HasClusteringEnabled):
                    UpdateHasClusteringEnabled();
                    break;
                case nameof(Map.HasScrollEnabled):
                    UpdateHasScrollEnabled();
                    break;
                case nameof(Map.HasZoomEnabled):
                    UpdateHasZoomEnabled();
                    break;
            }
        }

        /// <summary>
        /// When the map is clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapTapped(MapControl sender, MapInputEventArgs args)
        {
            Controller.SendMapClicked(args.Location.ToPosition());
        }

        /// <summary>
        /// When the map is long clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapHolding(MapControl sender, MapInputEventArgs args)
        {
            Controller.SendMapLongPress(args.Location.ToPosition());
        }

        public static double PutLatLngInGoodRange(double value, double min, double max)
        {
            if(min <= value && value <= max)
            {
                return value;
            }

            var range = max - min;

            while(value < min)
            {
                value = value + range;
            }

            if (value > max)
            {
                value = value - range;
            }

            return value;
        }

        public async Task MoveToRegion(MapSpan region, bool animate)
        {
            if (MapControl == null) return;
            if (region == null) return;

            var animation = animate ? MapAnimationKind.Bow : MapAnimationKind.None;

            await MapControl.TrySetViewBoundsAsync(region.ToUWPRegion(), null, animation);
        }

        /// <summary>
        /// When the collection of pins changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
		void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Pin pin in e.NewItems)
                        LoadPin(pin);
                    break;
                case NotifyCollectionChangedAction.Move:
                    // no matter
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Pin pin in e.OldItems)
                        RemovePin(pin);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach (Pin pin in e.OldItems)
                        RemovePin(pin);
                    foreach (Pin pin in e.NewItems)
                        LoadPin(pin);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    ClearPins();
                    break;
            }
        }

        /// <summary>
        /// Adds all pins
        /// </summary>
		void LoadPins()
        {
            foreach (var pin in Element.Pins)
                LoadPin(pin);
        }

        /// <summary>
        /// Remove all pins
        /// </summary>
        void ClearPins()
        {
            Control.Children.Clear();
#pragma warning disable 4014 // don't wanna block UI thread
            UpdateIsShowingUser();
#pragma warning restore
        }

        /// <summary>
        /// Remove a pin
        /// </summary>
        /// <param name="pin">The pin to remove</param>
        void RemovePin(Pin pinToRemove)
        {
            var pushPin = Control.Children.FirstOrDefault(c =>
            {
                var pin = (c as PushPin);
                return (pin != null && pin.DataContext.Equals(pinToRemove));
            });

            if (pushPin != null)
                Control.Children.Remove(pushPin);
        }

        /// <summary>
        /// Adds a pin
        /// </summary>
        /// <param name="pin">The pin to add</param>
        void LoadPin(Pin pin)
        {
            Control.Children.Add(new PushPin(pin));
        }

        /// <summary>
        /// Updates the map type
        /// </summary>
        void UpdateMapType()
        {
            switch (Element.MapType)
            {
                case MapType.Street:
                    Control.Style = MapStyle.Road;
                    break;
                case MapType.Satellite:
                    Control.Style = MapStyle.Aerial;
                    break;
                case MapType.Hybrid:
                    Control.Style = MapStyle.AerialWithRoads;
                    break;
            }
        }

        /// <summary>
        /// Updates the map region when changed
        /// </summary>
        private async Task UpdateMapRegion()
        {
            if (MapView == null || MapView.MapRegion == null) return;

            await MoveToRegion(MapView.MapRegion, false);
        }

        /// <summary>
        /// Hide or show user when needed
        /// </summary>
        /// <param name="moveToLocation">Does move to location when user found</param>
        async Task UpdateIsShowingUser(bool moveToLocation = true)
        {
            if (Control == null || Element == null) return;

            if(await Geolocator.RequestAccessAsync() == GeolocationAccessStatus.Denied)
            {
                return;
            }

            if (Element.IsShowingUser)
            {
                var myGeolocator = new Geolocator();
                if (myGeolocator.LocationStatus != PositionStatus.NotAvailable &&
                    myGeolocator.LocationStatus != PositionStatus.Disabled)
                {
                    var userPosition = await myGeolocator.GetGeopositionAsync();
                    if (userPosition?.Coordinate != null)
                        LoadUserPosition(userPosition.Coordinate, moveToLocation);
                }

                if (Control == null || Element == null) return;

                if (_timer == null)
                {
                    _timer = new DispatcherTimer();
                    _timer.Tick += async (s, o) => await UpdateIsShowingUser(moveToLocation: false);
                    _timer.Interval = TimeSpan.FromSeconds(15);
                }

                if (!_timer.IsEnabled)
                    _timer.Start();
            }
            else if (_userPositionCircle != null && Control.Children.Contains(_userPositionCircle))
            {
                _timer.Stop();
                Control.Children.Remove(_userPositionCircle);
            }
        }

        void LoadUserPosition(Geocoordinate userCoordinate, bool center)
        {
            if (Control == null || Element == null) return;

            var userPosition = new BasicGeoposition
            {
                Latitude = userCoordinate.Point.Position.Latitude,
                Longitude = userCoordinate.Point.Position.Longitude
            };

            var point = new Geopoint(userPosition);

            if (_userPositionCircle == null)
            {
                _userPositionCircle = new Ellipse
                {
                    Stroke = new SolidColorBrush(Colors.White),
                    Fill = new SolidColorBrush(Colors.Blue),
                    StrokeThickness = 2,
                    Height = 20,
                    Width = 20,
                    Opacity = 50
                };
            }

            if (Control.Children.Contains(_userPositionCircle))
                Control.Children.Remove(_userPositionCircle);

            MapControl.SetLocation(_userPositionCircle, point);
            MapControl.SetNormalizedAnchorPoint(_userPositionCircle, new Windows.Foundation.Point(0.5, 0.5));

            Control.Children.Add(_userPositionCircle);

            if (center)
            {
                Control.Center = point;
                Control.ZoomLevel = 13;
            }

            Controller.SendUserLocationChanged(userPosition.ToPosition());
        }

        /// <summary>
        /// Enable or disable clustering when channeededged
        /// </summary>
        void UpdateHasClusteringEnabled()
        {
            //
        }

        /// <summary>
        /// Enable or disable scroll when needed
        /// </summary>
        void UpdateHasScrollEnabled()
        {
            Control.PanInteractionMode = Element.HasScrollEnabled ? MapPanInteractionMode.Auto : MapPanInteractionMode.Disabled;
        }

        /// <summary>
        /// Enable or disable zoom when channeededged
        /// </summary>
        void UpdateHasZoomEnabled()
        {
            Control.ZoomInteractionMode = Element.HasZoomEnabled
                ? MapInteractionMode.GestureAndControl
                : MapInteractionMode.ControlOnly;
        }
    }
}