﻿using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;
using Xamarin.Medias;

namespace ExtendedForms.iOS.Medias
{
    public interface IMediaSourceHandler : IRegisterable
    {
        Task<UIImage> LoadImageAsync(MediaSource imagesource, CancellationToken cancelationToken = default(CancellationToken), float scale = 1);
    }
}