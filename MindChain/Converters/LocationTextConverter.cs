﻿using MindChain.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class LocationTextConverter : IValueConverter
    {
        public String DefaultValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Localization localization)
            {
                return localization.Place;
            }
            else
            {
                return DefaultValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
