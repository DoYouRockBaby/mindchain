﻿using MindChain.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    class LocalizationToPositionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var localization = value as Localization;
            if(value == null)
            {
                return null;
            }

            return new Position(localization.Latitude, localization.Longitude);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
