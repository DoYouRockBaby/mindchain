﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class IntFormatConverter : IValueConverter
    {
        public String Zero { get; set; }
        public String One { get; set; }
        public String More { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var intValue = (int)value;

            if(intValue == 0)
            {
                return String.Format(Zero, intValue);
            }
            else if(intValue == 1)
            {
                return String.Format(One, intValue);
            }
            else
            {
                return String.Format(More, intValue);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
