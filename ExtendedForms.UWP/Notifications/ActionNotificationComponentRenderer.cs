﻿using System.Threading.Tasks;
using Xamarin.Notifications;
using ExtendedForms.UWP.Notifications;
using Microsoft.Toolkit.Uwp.Notifications;
using ExtendedForms.UWP.Medias;
using Xamarin.Utility;

[assembly: ExportNotificationComponent(typeof(ActionNotificationComponent), typeof(ActionNotificationComponentRenderer))]
namespace ExtendedForms.UWP.Notifications
{
    public class ActionNotificationComponentRenderer : NotificationComponentRenderer<ActionNotificationComponent>
    {
        public class ActionParameters
        {
            public string ActionIdentifier;
            public string SerializedParam;
            public string SerializedParamType;
        }

        public override async Task SetUpNotificationAsync(ToastContent content, ToastBindingGeneric bindingGeneric)
        {
            ActionParameters arguments = null;

            if (Component.CommandParameter == null)
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = Component.ActionIdentifier,
                    SerializedParam = "",
                    SerializedParamType = ""
                };
            }
            else
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = Component.ActionIdentifier,
                    SerializedParam = Serializer.Serialize(Component.CommandParameter),
                    SerializedParamType = Component.CommandParameter.GetType().ToString()
                };
            }

            var button = new ToastButton(Component.Text, Serializer.Serialize(arguments))
            {
                ActivationType = ToastActivationType.Foreground
            };

            if(Component.Icon != null)
            {
                var handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(Component.Icon);
                button.ImageUri = await handler.GetOrCreateUriAsync(Component.Icon);
            }

            if(content.Actions == null)
            {
                content.Actions = new ToastActionsCustom();
            }

            var actions = content.Actions as ToastActionsCustom;
            actions.Buttons.Add(button);
        }
    }
}