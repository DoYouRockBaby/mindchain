﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using MindChain.Android;
using MindChain.Service;
using System;
using Xamarin.Forms;

namespace MindChain.Droid
{
    [Activity(Label = "MindChain", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            AndroidEnvironment.UnhandledExceptionRaiser += (sender, args) =>
            {
                throw new Exception("Unhandled exception");
            };

            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                throw new Exception("Unhandled exception");
            };

            Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity = this;

            SimpleContainer.Instance.Register<IQrCodeService, QrCodeService>();

            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            ExtendedForms.Forms.Forms.Init(ApplicationContext, this, bundle);

            LoadApplication(new App());
        }

        protected override void OnStart()
        {
            base.OnStart();
            ExtendedForms.Forms.Forms.OnStart();
        }
    }
}