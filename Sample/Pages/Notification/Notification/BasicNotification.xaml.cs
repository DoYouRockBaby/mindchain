﻿using Xamarin.Forms.Xaml;

namespace Sample.Pages.Notification.Notification
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasicNotification : Xamarin.Notifications.Notification
    {
        public BasicNotification()
        {
            InitializeComponent();
        }
    }
}