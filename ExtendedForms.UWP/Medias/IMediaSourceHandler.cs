﻿using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Medias;

namespace ExtendedForms.UWP.Medias
{
    public interface IMediaSourceHandler : IRegisterable
    {
        Task<Windows.UI.Xaml.Media.ImageSource> LoadImageAsync(MediaSource imagesource, CancellationToken cancellationToken = default(CancellationToken));
        Task<string> GetOrCreateUriAsync(MediaSource imagesource, CancellationToken cancellationToken = default(CancellationToken));
    }
}