﻿using MindChain.DataProviders;
using System.ComponentModel;
using Xamarin.Forms;
using System;
using System.Threading.Tasks;
using MindChain.Models;
using System.Diagnostics;
using MindChain.Helpers;
using ZXing.Mobile;
using MindChain.Pages;
using ZXing.Net.Mobile.Forms;
using System.Collections.Generic;

namespace MindChain.ViewModels
{
    public class AddContactViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IAccountProvider accountProvider;

        public AddContactViewModel(IAccountProvider accountProvider)
        {
            this.accountProvider = accountProvider;

            UpdateSuggestedFriendsCommand = new Command(async () => await UpdateSuggestedFriends());
            ScanQrCodeCommand = new Command(async () => await ScanQrCode());
        }

        public Command UpdateSuggestedFriendsCommand { get; set; }
        public Command ScanQrCodeCommand { get; set; }

        private ObservableRangeCollection<Account> suggestedFriends = new ObservableRangeCollection<Account>();
        public ObservableRangeCollection<Account> SuggestedFriends
        {
            get
            {
                return suggestedFriends;
            }
            set
            {
                if (value != suggestedFriends)
                {
                    suggestedFriends = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SuggestedFriends)));
                }
            }
        }

        string searchRequest = "";
        public string SearchRequest
        {
            get
            {
                return searchRequest;
            }
            set
            {
                if (value != searchRequest)
                {
                    searchRequest = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SearchRequest)));
                }
            }
        }

        bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                if (value != isBusy)
                {
                    isBusy = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsBusy)));
                }
            }
        }

        private async Task UpdateSuggestedFriends()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                SuggestedFriends.Clear();
                var accounts = await accountProvider.Search(SearchRequest);
                SuggestedFriends.ReplaceRange(accounts);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task AskFriend(string username)
        {
            var result = await accountProvider.AskFriend(username);
            if (result)
            {
                await ParentPage.DisplayAlert("Your friendship demand has been sended", "You now need your friend to accept your invitation and interract with him.", "Ok");
            }
            else
            {
                await ParentPage.DisplayAlert("Your friend couldn't be added", "You could have entered a wrong username.", "Ok");
            }
        }

        private async Task ScanQrCode()
        {
            var scanPage = new ZXingScannerPage();

            var options = new MobileBarcodeScanningOptions
            {
                PossibleFormats = new List<ZXing.BarcodeFormat>
                {
                    ZXing.BarcodeFormat.QR_CODE
                }
            };

            var overlay = new ZXingDefaultOverlay
            {
                ShowFlashButton = false,
            };
            overlay.Children.Clear();
            overlay.BindingContext = overlay;

            scanPage = new ZXingScannerPage(options, overlay);

            scanPage.OnScanResult += (code) =>
            {
                // Stop scanning
                scanPage.IsScanning = false;

                // Pop the page and show the result
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = await accountProvider.AddFriendByCode(code.Text);
                    if (result)
                    {
                        //Go to friend page
                    }
                    else
                    {
                        await ParentPage.DisplayAlert("Your friend couldn't be added", "You can try to scan the code again.", "Ok");
                    }
                });
            };

            await ParentPage.Navigation.PushAsync(scanPage);
        }
    }
}
