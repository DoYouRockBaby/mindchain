﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ExtendedForms.Forms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AutocompleteEntry : ContentView
    {
        /// <summary>
        /// Bindable Property of <see cref="SuggestionsTemplate"/>
        /// </summary>
        public static readonly BindableProperty SuggestionsTemplateProperty = BindableProperty.Create(nameof(SuggestionsTemplate), typeof(DataTemplate), typeof(Entry), null, defaultBindingMode: BindingMode.OneWay, propertyChanged: OnSuggestionsTemplateChanged);

        /// <summary>
        /// Bindable Property of <see cref="SuggestionsProvider"/>
        /// </summary>
        public static readonly BindableProperty SuggestionsProviderProperty = BindableProperty.Create(nameof(SuggestionsProvider), typeof(ISuggestionsProvider), typeof(Entry), null, defaultBindingMode: BindingMode.OneWay);

        /// <summary>
        /// Bindable Property of <see cref="Placeholder"/>
        /// </summary>
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create("Placeholder", typeof(string), typeof(Entry), default(string), propertyChanged: OnPlaceholderChanged);

        /// <summary>
        /// Bindable Property of <see cref="Text"/>
        /// </summary>
        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(Entry), null, BindingMode.TwoWay, propertyChanged: OnTextChanged);

        /// <summary>
        /// Bindable Property of <see cref="TextColor"/>
        /// </summary>
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create("TextColor", typeof(Color), typeof(Entry), Color.Default, propertyChanged: OnTextColorChanged);

        /// <summary>
        /// Bindable Property of <see cref="HorizontalTextAlignment"/>
        /// </summary>
        public static readonly BindableProperty HorizontalTextAlignmentProperty = BindableProperty.Create("HorizontalTextAlignment", typeof(TextAlignment), typeof(Entry), TextAlignment.Start, propertyChanged: OnHorizontalTextAlignmentChanged);

        /// <summary>
        /// Bindable Property of <see cref="PlaceholderColor"/>
        /// </summary>
        public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.Create("PlaceholderColor", typeof(Color), typeof(Entry), Color.Default, propertyChanged: OnPlaceholderColorChanged);

        /// <summary>
        /// Bindable Property of <see cref="FontFamily"/>
        /// </summary>
        public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create("FontFamily", typeof(string), typeof(Entry), default(string), propertyChanged: OnFontFamilyChanged);

        /// <summary>
        /// Bindable Property of <see cref="FontSize"/>
        /// </summary>
        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create("FontSize", typeof(double), typeof(IFontElement), -1.0, propertyChanged: OnFontSizeChanged, defaultValueCreator: FontSizeDefaultValueCreator);

        /// <summary>
        /// Bindable Property of <see cref="FontAttributes"/>
        /// </summary>
        public static readonly BindableProperty FontAttributesProperty = BindableProperty.Create("FontAttributes", typeof(FontAttributes), typeof(Entry), FontAttributes.None, propertyChanged: OnFontAttributesChanged);

        /// <summary>
        /// Interface used to pull suggestions
        /// </summary>
        public interface ISuggestionsProvider
        {
            Task<IList> GetSuggestions(string request);
            string ConvertToString(object obj);
        }

        /// <summary>
        /// The autocomplete suggestion provider
        /// </summary>
        public ISuggestionsProvider SuggestionsProvider
        {
            get => (ISuggestionsProvider)GetValue(SuggestionsProviderProperty);
            set => SetValue(SuggestionsProviderProperty, value);
        }

        /// <summary>
        /// Template used for suggestions cell generation
        /// </summary>
        public DataTemplate SuggestionsTemplate
        {
            get => (DataTemplate)GetValue(SuggestionsTemplateProperty);
            set => SetValue(SuggestionsTemplateProperty, value);
        }

        /// <summary>
        /// Template used for suggestions cell generation
        /// </summary>
        public IEnumerable Suggestions
        {
            get => ListView.ItemsSource;
        }

        public TextAlignment HorizontalTextAlignment
        {
            get { return (TextAlignment)GetValue(HorizontalTextAlignmentProperty); }
            set { SetValue(HorizontalTextAlignmentProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public FontAttributes FontAttributes
        {
            get { return (FontAttributes)GetValue(FontAttributesProperty); }
            set { SetValue(FontAttributesProperty, value); }
        }

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        [TypeConverter(typeof(FontSizeConverter))]
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        private ObservableCollection<object> _suggestionsCollection = new ObservableCollection<object>();

        public AutocompleteEntry()
        {
            InitializeComponent();

            HideSuggestions();
            Entry.Unfocused += (e, s) => HideSuggestions();
            ListView.ItemSelected += SuggestionSelected;
            ListView.ItemsSource = _suggestionsCollection;

            HeightRequest = Entry.Height;

            Entry.TextChanged += EntryTextChanged;
        }

        private void SuggestionSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if(SuggestionsProvider != null)
            {
                Entry.Text = SuggestionsProvider.ConvertToString(e.SelectedItem);
            }
            else
            {
                Entry.Text = e.SelectedItem.ToString();
            }

            HideSuggestions();
        }

        private async Task UpdateSuggestions()
        {
            if(SuggestionsProvider == null)
            {
                return;
            }

            ListView.IsVisible = true;
            _suggestionsCollection.Clear();

            if (Entry.Text == "")
            {
                HideSuggestions();
                return;
            }

            var suggestions = await SuggestionsProvider.GetSuggestions(Entry.Text);

            foreach(var suggestion in suggestions)
            {
                _suggestionsCollection.Add(suggestion);
            }
        }

        private void HideSuggestions()
        {
            ListView.IsVisible = false;
        }

        private static void OnSuggestionsTemplateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.ListView.ItemTemplate = (DataTemplate)newValue;
        }

        private static object FontSizeDefaultValueCreator(BindableObject bindable)
        {
            return ((IFontElement)bindable).FontSizeDefaultValueCreator();
        }

        private static void OnPlaceholderChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.Placeholder = (string)newValue;
        }

        private static async void OnTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.Text = (string)newValue;

            await entry.UpdateSuggestions();
        }

        private void EntryTextChanged(object sender, TextChangedEventArgs e)
        {
            SetValue(TextProperty, e.NewTextValue);
        }

        private static void OnTextColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.TextColor = (Color)newValue;
        }

        private static void OnHorizontalTextAlignmentChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.HorizontalTextAlignment = (TextAlignment)newValue;
        }

        private static void OnPlaceholderColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.PlaceholderColor = (Color)newValue;
        }

        private static void OnFontFamilyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.FontFamily = (string)newValue;
        }

        private static void OnFontSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.FontSize = (double)newValue;
        }

        private static void OnFontAttributesChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var entry = bindable as AutocompleteEntry;
            entry.Entry.FontAttributes = (FontAttributes)newValue;
        }
    }
}