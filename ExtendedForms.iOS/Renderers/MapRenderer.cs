﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using CoreGraphics;
using CoreLocation;
using Foundation;
using MapKit;
using ObjCRuntime;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.iOS.ClusterKit;
using System.Collections.ObjectModel;
using ExtendedForms.iOS.Extensions;
using static Xamarin.Forms.Map;

[assembly: ExportRenderer(typeof(Map), typeof(ExtendedForms.iOS.Renderers.MapRenderer))]
namespace ExtendedForms.iOS.Renderers
{
    [Preserve(AllMembers = true)]
    public class MapRenderer : ViewRenderer<Map, MKMapView>
    {
        private MKMapView MapControl
        {
            get { return Control as MKMapView; }
        }

        public Map MapView
        {
            get { return Element as Map; }
        }

        IMapController Controller => Element;

        ClusterMap _clusterMap;
        UIGestureRecognizer _tapGestureRecognizer;
        UIGestureRecognizer _longPressGestureRecognizer;
        CLLocationManager _locationManager;

        /// <summary>
        /// Dummy function to avoid linker.
        /// </summary>
        [Preserve]
        public static void InitMapRenderer()
        {
            var temp = DateTime.Now;
        }

        /// <inheritdoc/>
        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null && MapControl != null)
            {
                MapControl.CalloutAccessoryControlTapped -= MapOnAnnotationClick;
                MapControl.DidUpdateUserLocation -= UserLocationChanged;

                MapControl.RemoveGestureRecognizer(_tapGestureRecognizer);
                MapControl.RemoveGestureRecognizer(_longPressGestureRecognizer);
            }

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    var mapView = new MKMapView();
                    SetNativeControl(mapView);
                }

                MapControl.CalloutAccessoryControlTapped += MapOnAnnotationClick;
                MapControl.DidUpdateUserLocation += UserLocationChanged;

                _tapGestureRecognizer = new UITapGestureRecognizer(MapTapped)
                {
                    //_tapGestureRecognizer.RequireGestureRecognizerToFail(_doubleTapGestureRecognizer);
                    ShouldReceiveTouch = (recognizer, touch) => !(touch.View is MKAnnotationView)
                };
                MapControl.AddGestureRecognizer(_tapGestureRecognizer);
                MapControl.AddGestureRecognizer(_longPressGestureRecognizer = new UILongPressGestureRecognizer(MapHolding));

                MessagingCenter.Subscribe<Map, MoveToRegionArgs>(this, "MapMoveToRegion", (sender, args) => MoveToRegion(args.Region, args.Animate));

                UpdateMapRegion();
                UpdateMapType();
                UpdateIsShowingUser(true);
                UpdateHasClusteringEnabled();
                UpdateHasScrollEnabled();
                UpdateHasZoomEnabled();

                ((ObservableCollection<Pin>)MapView.Pins).CollectionChanged += OnCollectionChanged;

                foreach (var pin in MapView.Pins)
                {
                    AddPin(pin);
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case nameof(Map.MapRegion):
                    //UpdateMapRegion();
                    break;
                case nameof(Map.MapType):
                    UpdateMapType();
                    break;
                case nameof(Map.IsShowingUser):
                    UpdateIsShowingUser(true);
                    break;
                case nameof(Map.HasClusteringEnabled):
                    UpdateHasClusteringEnabled();
                    break;
                case nameof(Map.HasScrollEnabled):
                    UpdateHasScrollEnabled();
                    break;
                case nameof(Map.HasZoomEnabled):
                    UpdateHasZoomEnabled();
                    break;
            }
        }

        /// <summary>
        /// When the map is clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapTapped(UITapGestureRecognizer recognizer)
        {
            if (recognizer.State != UIGestureRecognizerState.Ended) return;

            var pixelLocation = recognizer.LocationInView(MapControl);
            var coordinate = MapControl.ConvertPoint(pixelLocation, MapControl);

            Element.SendMapClicked(coordinate.ToPosition());
        }

        /// <summary>
        /// When the map is long clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void MapHolding(UILongPressGestureRecognizer recognizer)
        {
            if (recognizer.State != UIGestureRecognizerState.Began) return;

            var pixelLocation = recognizer.LocationInView(MapControl);
            var coordinate = MapControl.ConvertPoint(pixelLocation, MapControl);

            Element.SendMapLongPress(coordinate.ToPosition());
        }

        /// <summary>
        /// When the user location changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        private void UserLocationChanged(object sender, MKUserLocationEventArgs e)
        {
            if (e.UserLocation == null || MapView == null) return;

            var newPosition = e.UserLocation.Location.Coordinate.ToPosition();
            Element.SendUserLocationChanged(newPosition);
        }

        /// <summary>
        /// When a marker on the map is clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="args">Event arguments</param>
        void MapOnAnnotationClick(object sender, MKMapViewAccessoryTappedEventArgs e)
        {
            // lookup pin
            var targetPin = GetPinByAnnotation(e.View.Annotation);

            // only consider event handled if a handler is present. 
            // Else allow default behavior of displaying an info window.
            targetPin?.Command?.Execute(targetPin);
        }

        /// <summary>
        /// Move the map view
        /// </summary>
        /// <param name="region">Target region</param>
        /// <param name="animate">Does animate the region movement</param>
        public void MoveToRegion(MapSpan region, bool animate)
        {
            if (MapControl == null) return;

            var mapRegion = new MKCoordinateRegion(region.Center.ToLocationCoordinate(), new MKCoordinateSpan(region.LatitudeDegrees, region.LongitudeDegrees));
            MapControl.SetRegion(mapRegion, animate);
        }

        /// <summary>
        /// When the collection of pins changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
		void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (Pin pin in e.NewItems)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (Pin pin in e.OldItems)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach (Pin pin in e.OldItems)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    foreach (Pin pin in e.NewItems)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Reset:
                    foreach (Pin pin in MapView.Pins)
                    {
                        if (!MapView.Pins.Contains(pin))
                        {
                            RemovePin(pin);
                        }
                    }

                    foreach (Pin pin in MapView.Pins)
                    {
                        AddPin(pin);
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    //do nothing
                    break;
            }
        }

        /// <summary>
        /// Adds a marker to the map
        /// </summary>
        /// <param name="pin">The Forms Pin</param>
        void AddPin(Pin pin)
        {
            var annotation = new CustomMapAnnotation(pin);

            if (MapView.HasClusteringEnabled)
            {
                _clusterMap.ClusterManager.AddAnnotation(annotation);
            }
            else
            {
                MapControl.AddAnnotation(annotation);
            }

            pin.PropertyChanged += OnPinPropertyChanged;
        }

        /// <summary>
        /// Remove a pin from the map and the internal dictionary
        /// </summary>
        /// <param name="pin">The pin to remove</param>
        /// <param name="removeMarker">true to remove the marker from the map</param>
        void RemovePin(Pin pin, bool removeMarker = true)
        {
            if (!MapView.Pins.Contains(pin))
            {
                var annotation = GetCustomAnnotation(pin);

                if (annotation == null)
                {
                    return;
                }

                annotation.CustomPin.PropertyChanged -= OnPinPropertyChanged;

                if (MapView.HasClusteringEnabled)
                {
                    _clusterMap.ClusterManager.RemoveAnnotation(annotation);
                }
                else
                {
                    MapControl.RemoveAnnotation(annotation);
                }
            }
        }

        /// <summary>
        /// Generate iOS annotation
        /// </summary>
        /// <param name="pin">The referenced pin</param>
        CustomMapAnnotation GetCustomAnnotation(Pin pin)
        {
            if (MapView.HasClusteringEnabled)
            {
                return _clusterMap.ClusterManager.Annotations.OfType<CustomMapAnnotation>().SingleOrDefault(i => i.CustomPin.Equals(pin));
            }
            else
            {
                return MapControl.Annotations.OfType<CustomMapAnnotation>().SingleOrDefault(i => i.CustomPin.Equals(pin));
            }
        }

        /// <summary>
        /// Generate iOS annotation
        /// </summary>
        /// <param name="view">The view object</param>
        CustomMapAnnotation GetCustomAnnotation(MKAnnotationView view)
        {
            if (MapView.HasClusteringEnabled)
            {
                var cluster = view.Annotation as CKCluster;

                if (cluster?.Annotations.Count() != 1) return null;

                return cluster.Annotations.First() as CustomMapAnnotation;
            }
            else
            {
                return view.Annotation as CustomMapAnnotation;
            }
        }

        /// <summary>
        /// When a property of the pin changed
        /// </summary>
        /// <param name="sender">Event Sender</param>
        /// <param name="e">Event Arguments</param>
        void OnPinPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Pin.Title) ||
                e.PropertyName == nameof(Pin.Subtitle))
                return;

            var formsPin = (Pin)sender;
            var annotation = GetCustomAnnotation(formsPin);

            if (annotation == null) return;

            MKAnnotationView annotationView = GetViewByAnnotation(annotation);

            if (annotationView == null) return;

            switch (e.PropertyName)
            {
                case nameof(Pin.Image):
                    UpdateImage(annotationView, formsPin);
                    break;
                case nameof(Pin.DefaultPinColor):
                    UpdateImage(annotationView, formsPin);
                    break;
                case nameof(Pin.IsDraggable):
                    annotationView.Draggable = formsPin.IsDraggable;
                    break;
                case nameof(Pin.IsVisible):
                    SetAnnotationViewVisibility(annotationView, formsPin);
                    break;
                case nameof(Pin.Position):
                    annotationView.Annotation.SetCoordinate(formsPin.Position.ToLocationCoordinate());
                    annotation.SetCoordinateInternal(formsPin.Position.ToLocationCoordinate(), true);
                    break;
                case nameof(Pin.ShowCallout):
                    annotationView.CanShowCallout = formsPin.ShowCallout;
                    break;
                case nameof(Pin.Anchor):
                    if (formsPin.Image != null)
                    {
                        annotationView.Layer.AnchorPoint = new CGPoint(formsPin.Anchor.X, formsPin.Anchor.Y);
                    }
                    break;
                case nameof(Pin.Rotation):
                    annotationView.Transform = CGAffineTransform.MakeRotation((float)formsPin.Rotation);
                    break;
            }
        }

        public override SizeRequest GetDesiredSize(double widthConstraint, double heightConstraint)
        {
            return Control.GetSizeRequest(widthConstraint, heightConstraint);
        }

        /// <summary>
        /// Set the visibility of an annotation view
        /// </summary>
        /// <param name="annotationView">The annotation view</param>
        /// <param name="pin">The forms pin</param>
        void SetAnnotationViewVisibility(MKAnnotationView annotationView, Pin pin)
        {
            annotationView.Hidden = !pin.IsVisible;
            annotationView.UserInteractionEnabled = pin.IsVisible;
            annotationView.Enabled = pin.IsVisible;
        }

        /// <summary>
        /// Returns the <see cref="TKCustomMapPin"/> by the native <see cref="IMKAnnotation"/>
        /// </summary>
        /// <param name="annotation">The annotation to search with</param>
        /// <returns>The forms pin</returns>
        protected Pin GetPinByAnnotation(IMKAnnotation annotation)
        {
            return (annotation as CustomMapAnnotation)?.CustomPin;
        }

        /// <summary>
        /// Set the image of the annotation view
        /// </summary>
        /// <param name="annotationView">The annotation view</param>
        /// <param name="pin">The forms pin</param>
        async void UpdateImage(MKAnnotationView annotationView, Pin pin)
        {
            if (pin.Image != null)
            {
                // If this is the case, we need to get a whole new annotation view. 
                if (annotationView.GetType() == typeof(MKPinAnnotationView))
                {

                    if (MapView.HasClusteringEnabled)
                    {
                        _clusterMap.ClusterManager.RemoveAnnotation(GetCustomAnnotation(annotationView));
                        _clusterMap.ClusterManager.AddAnnotation(new CustomMapAnnotation(pin));
                    }
                    else
                    {
                        MapControl.RemoveAnnotation(GetCustomAnnotation(annotationView));
                        MapControl.AddAnnotation(new CustomMapAnnotation(pin));
                    }
                    return;
                }
                UIImage image = await pin.Image.ToImage();
                Device.BeginInvokeOnMainThread(() =>
                {
                    annotationView.Image = image;
                });
            }
            else
            {
                if (annotationView is MKPinAnnotationView pinAnnotationView)
                {
                    var pinTintColorAvailable = pinAnnotationView.RespondsToSelector(new Selector("pinTintColor"));

                    if (!pinTintColorAvailable)
                    {
                        return;
                    }

                    if (pin.DefaultPinColor != Color.Default)
                    {
                        pinAnnotationView.PinTintColor = pin.DefaultPinColor.ToUIColor();
                    }
                    else
                    {
                        pinAnnotationView.PinTintColor = UIColor.Red;
                    }
                }
                else
                {
                    if (MapView.HasClusteringEnabled)
                    {
                        _clusterMap.ClusterManager.RemoveAnnotation(GetCustomAnnotation(annotationView));
                        _clusterMap.ClusterManager.AddAnnotation(new CustomMapAnnotation(pin));
                    }
                    else
                    {
                        MapControl.RemoveAnnotation(GetCustomAnnotation(annotationView));
                        MapControl.AddAnnotation(new CustomMapAnnotation(pin));
                    }
                }
            }
        }

        /// <summary>
        /// Updates the map type
        /// </summary>
        void UpdateMapType()
        {
            if (MapControl == null || MapView == null)
            {
                return;
            }

            switch (MapView.MapType)
            {
                case MapType.Hybrid:
                    MapControl.MapType = MKMapType.Hybrid;
                    break;
                case MapType.Satellite:
                    MapControl.MapType = MKMapType.Satellite;
                    break;
                case MapType.Street:
                    MapControl.MapType = MKMapType.Standard;
                    break;
            }
        }

        /// <summary>
        /// Updates the map region when changed
        /// </summary>
        private void UpdateMapRegion()
        {
            if (MapView == null || MapView.MapRegion == null) return;

            MoveToRegion(MapView.MapRegion, false);
        }

        /// <summary>
        /// Hide or show user when needed
        /// </summary>
        /// <param name="moveToLocation">Does move to location when user found</param>
        void UpdateIsShowingUser(bool moveToLocation = true)
        {
            if (MapView == null || MapControl == null) return;

            if (MapView.IsShowingUser && UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                _locationManager = new CLLocationManager();
                _locationManager.RequestWhenInUseAuthorization();
            }

            MapControl.ShowsUserLocation = MapView.IsShowingUser;
        }

        /// <summary>
        /// Enable or disable clustering when channeededged
        /// </summary>
        void UpdateHasClusteringEnabled()
        {
            if (MapView == null || MapControl == null) return;

            if (MapView.HasClusteringEnabled)
            {
                if (_clusterMap == null)
                {
                    _clusterMap = new ClusterMap(MapControl);
                }

                MapControl.RemoveAnnotations(MapControl.Annotations);
                foreach (var pin in MapView.Pins)
                {
                    AddPin(pin);
                }
                _clusterMap.ClusterManager.UpdateClusters();
            }
            else
            {
                if(_clusterMap != null)
                {
                    _clusterMap.ClusterManager.RemoveAnnotations(_clusterMap.ClusterManager.Annotations);
                    foreach (var pin in MapView.Pins)
                    {
                        AddPin(pin);
                    }
                    _clusterMap.Dispose();
                    _clusterMap = null;
                }
            }
        }

        /// <summary>
        /// Enable or disable scroll when needed
        /// </summary>
        void UpdateHasScrollEnabled()
        {
            if (MapControl == null || MapView == null) return;

            MapControl.ScrollEnabled = MapView.HasScrollEnabled;
        }

        /// <summary>
        /// Enable or disable zoom when channeededged
        /// </summary>
        void UpdateHasZoomEnabled()
        {
            if (MapControl == null || MapView == null) return;

            MapControl.ZoomEnabled = MapView.HasZoomEnabled;
        }

        /// <summary>
        /// When the camera region changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Arguments</param>
        void OnCameraMove(object sender, MKMapViewChangeEventArgs e)
        {
            Element.SendRegionChanged(MapControl.GetCurrentMapRegion());

            if (MapView.HasClusteringEnabled)
            {
                _clusterMap.ClusterManager.UpdateClustersIfNeeded();
            }
        }

        MKAnnotationView GetViewByAnnotation(CustomMapAnnotation annotation)
        {
            if (MapView.HasClusteringEnabled)
            {
                return MapControl.ViewForAnnotation(MapControl.Annotations.GetCluster(annotation));
            }
            else
            {
                return MapControl.ViewForAnnotation(annotation);
            }
        }
    }
}