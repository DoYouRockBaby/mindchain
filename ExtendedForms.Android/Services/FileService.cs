﻿using Android.App;
using System.IO;
using Xamarin.Forms.Services;

namespace ExtendedForms.Android.Services
{
    class FileService : IFileService
    {
        public string CreateTempFile(byte[] bytes, string ext)
        {
            if(ext == "")
            {
                ext = "tmp";
            }

            if(ext[0] == '.')
            {
                ext = ext.Substring(1);
            }

            var outputDir = Application.Context.CacheDir;
            var outputFile = Java.IO.File.CreateTempFile("temp", ext, outputDir);
            
            using (var fileStream = new FileStream(outputFile.Path, FileMode.Create, FileAccess.Write))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }

            return outputFile.Path;
        }
    }
}