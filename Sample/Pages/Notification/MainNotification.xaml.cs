﻿using Sample.Pages.Notification.Notification;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Forms.Xaml;

namespace Sample.Pages.Notification
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainNotification : TabbedPage
    {
        public MainNotification()
        {
            InitializeComponent();
        }

        private async Task BasicClicked(object sender, EventArgs e)
        {
            var notification = new BasicNotification();
            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            var result = await notificationService.PushNotificationAsync(notification);
        }

        private async Task ImageClicked(object sender, EventArgs e)
        {
            var notification = new ImageNotification();
            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            var result = await notificationService.PushNotificationAsync(notification);
        }

        private async Task ActionClicked(object sender, EventArgs e)
        {
            var notification = new ActionNotification();
            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            var result = await notificationService.PushNotificationAsync(notification);
        }
    }
}