﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace Xamarin.Forms
{
    public class AutocompleteEntry : ListView, IEntryController, IFontElement, ITextElement
    {
        /// <summary>
        /// Interface used to pull suggestions
        /// </summary>
        public interface ISuggestionProvider
        {
            Task<IEnumerable> GetSuggestions(string request);
            string ConvertToString(object obj);
        }

        /// <summary>
        /// Bindable Property of <see cref="SuggestionProvider"/>
        /// </summary>
        public static readonly BindableProperty SuggestionProviderProperty = BindableProperty.Create(nameof(SuggestionProvider), typeof(ISuggestionProvider), typeof(AutocompleteEntry), null, defaultBindingMode: BindingMode.OneWay);

        /// <summary>
        /// Bindable Property of <see cref="Placeholder"/>
        /// </summary>
        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(AutocompleteEntry), default(string));

        /// <summary>
        /// Bindable Property of <see cref="Text"/>
        /// </summary>
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(AutocompleteEntry), null, BindingMode.TwoWay);

        /// <summary>
        /// Bindable Property of <see cref="TextColor"/>
        /// </summary>
        public static readonly BindableProperty TextColorProperty = TextElement.TextColorProperty;

        /// <summary>
        /// Bindable Property of <see cref="HorizontalTextAlignment"/>
        /// </summary>
        public static readonly BindableProperty HorizontalTextAlignmentProperty = BindableProperty.Create(nameof(HorizontalTextAlignment), typeof(TextAlignment), typeof(AutocompleteEntry), TextAlignment.Start);

        /// <summary>
        /// Bindable Property of <see cref="PlaceholderColor"/>
        /// </summary>
        public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(AutocompleteEntry), Color.Default);

        /// <summary>
        /// Bindable Property of <see cref="FontFamily"/>
        /// </summary>
        public static readonly BindableProperty FontFamilyProperty = FontElement.FontFamilyProperty;

        /// <summary>
        /// Bindable Property of <see cref="FontSize"/>
        /// </summary>
        public static readonly BindableProperty FontSizeProperty = FontElement.FontSizeProperty;

        /// <summary>
        /// Bindable Property of <see cref="FontAttributes"/>
        /// </summary>
        public static readonly BindableProperty FontAttributesProperty = FontElement.FontAttributesProperty;

        /// <summary>
        /// Bindable Property of <see cref="Keyboard"/>
        /// </summary>
        public static readonly BindableProperty KeyboardProperty = BindableProperty.Create(nameof(Keyboard), typeof(Keyboard), typeof(AutocompleteEntry), Keyboard.Default, coerceValue: (o, v) => (Keyboard)v ?? Keyboard.Default);

        /// <summary>
        /// Bindable Property of <see cref="IsSpellCheckEnabled"/>
        /// </summary>
        public static readonly BindableProperty IsSpellCheckEnabledProperty = BindableProperty.Create(nameof(IsSpellCheckEnabled), typeof(bool), typeof(AutocompleteEntry), true);

        /// <summary>
        /// Bindable Property of <see cref="IsSearch"/>
        /// </summary>
        public static readonly BindableProperty KeyboardActionProperty = BindableProperty.Create(nameof(KeyboardAction), typeof(KeyboardAction), typeof(AutocompleteEntry), KeyboardAction.Send);

        /// <summary>
        /// Bindable Property of <see cref="MaxLength"/>
        /// </summary>
        public static readonly BindableProperty MaxLengthProperty = BindableProperty.Create(nameof(MaxLength), typeof(int), typeof(AutocompleteEntry), int.MaxValue);

        /// <summary>
        /// Bindable Property of <see cref="CharCountBeforeAutocomplete"/>
        /// </summary>
        public static readonly BindableProperty CharCountBeforeAutocompleteProperty = BindableProperty.Create(nameof(CharCountBeforeAutocomplete), typeof(int), typeof(AutocompleteEntry), 2);

        /// <summary>
        /// The autocomplete suggestion provider
        /// </summary>
        public ISuggestionProvider SuggestionProvider
        {
            get => (ISuggestionProvider)GetValue(SuggestionProviderProperty);
            set => SetValue(SuggestionProviderProperty, value);
        }


        public TextAlignment HorizontalTextAlignment
        {
            get { return (TextAlignment)GetValue(HorizontalTextAlignmentProperty); }
            set { SetValue(HorizontalTextAlignmentProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public FontAttributes FontAttributes
        {
            get { return (FontAttributes)GetValue(FontAttributesProperty); }
            set { SetValue(FontAttributesProperty, value); }
        }

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        [TypeConverter(typeof(FontSizeConverter))]
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public int MaxLength
        {
            get { return (int)GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }

        public Keyboard Keyboard
        {
            get { return (Keyboard)GetValue(KeyboardProperty); }
            set { SetValue(KeyboardProperty, value); }
        }

        public bool IsSpellCheckEnabled
        {
            get { return (bool)GetValue(IsSpellCheckEnabledProperty); }
            set { SetValue(IsSpellCheckEnabledProperty, value); }
        }

        public KeyboardAction KeyboardAction
        {
            get { return (KeyboardAction)GetValue(KeyboardActionProperty); }
            set { SetValue(KeyboardActionProperty, value); }
        }

        public int CharCountBeforeAutocomplete
        {
            get { return (int)GetValue(CharCountBeforeAutocompleteProperty); }
            set { SetValue(CharCountBeforeAutocompleteProperty, value); }
        }

        public event EventHandler Completed;

        public AutocompleteEntry()
        {
            VerticalOptions = (LayoutOptions)VerticalOptionsProperty.DefaultValue;
            HorizontalOptions = (LayoutOptions)HorizontalOptionsProperty.DefaultValue;
        }

        public void SendCompleted()
        {
            if (IsEnabled)
            {
                Completed?.Invoke(this, EventArgs.Empty);
            }
        }

        public void OnFontFamilyChanged(string oldValue, string newValue)
        {
            InvalidateMeasureInternal(InvalidationTrigger.MeasureChanged);
        }

        public void OnFontSizeChanged(double oldValue, double newValue)
        {
            InvalidateMeasureInternal(InvalidationTrigger.MeasureChanged);
        }

        public double FontSizeDefaultValueCreator()
        {
            return Device.GetNamedSize(NamedSize.Default, this);
        }

        public void OnFontAttributesChanged(FontAttributes oldValue, FontAttributes newValue)
        {
            InvalidateMeasureInternal(InvalidationTrigger.MeasureChanged);
        }

        public void OnFontChanged(Font oldValue, Font newValue)
        {
            InvalidateMeasureInternal(InvalidationTrigger.MeasureChanged);
        }

        public void OnTextColorPropertyChanged(Color oldValue, Color newValue)
        {
        }

        readonly Dictionary<Size, SizeRequest> _measureCache = new Dictionary<Size, SizeRequest>();

        [Obsolete("OnSizeRequest is obsolete as of version 2.2.0. Please use OnMeasure instead.")]
        protected override SizeRequest OnSizeRequest(double widthConstraint, double heightConstraint)
        {
            if (Platform == null || !IsPlatformEnabled)
            {
                return new SizeRequest(new Size(-1, -1));
            }

            return Platform.GetNativeSize(this, widthConstraint, heightConstraint);
        }
    }
}
