﻿using Xamarin.Forms;

namespace Xamarin.Notifications
{
    public class TextNotificationComponent : NotificationComponent
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(TextNotificationComponent), null);
        public static readonly BindableProperty IsSubtextProperty = BindableProperty.Create(nameof(IsSubtext), typeof(bool), typeof(TextNotificationComponent), false);

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsSubtext
        {
            get { return (bool)GetValue(IsSubtextProperty); }
            set { SetValue(IsSubtextProperty, value); }
        }
    }
}
