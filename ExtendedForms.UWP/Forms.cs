﻿using ExtendedForms.UWP.Services;
using ExtendedForms.UWP.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Forms.Internals;
using ExtendedForms.UWP.Notifications;
using ExtendedForms.UWP.Medias;
using Windows.ApplicationModel.Activation;
using Xamarin.Utility;
using static ExtendedForms.UWP.Notifications.ActionNotificationComponentRenderer;
using System;

namespace ExtendedForms.Forms
{
    public class Forms
    {
        public static void Init()
        {
            //Init renderer
            MapRenderer.InitMapRenderer("L6zk8nmKY7HpbL8vALE0~m4zSYiH6qlED31U5mSd8LQ~Ak0jE-i-bdrOYc2XPBuqmApiYhu99LRXCXUj0lzGs2bUuFZ1rBZCiEoyPitYNxZR");

            //Register handlers
            Registrar.RegisterAll(new[] { typeof(ExportNotificationComponentAttribute), typeof(ExportMediaSourceHandlerAttribute) });

            //Register services
            SimpleContainer.Instance.Register<INotificationService, NotificationService>();
            SimpleContainer.Instance.Register<IGeocoder, Geocoder>();
            SimpleContainer.Instance.Register<IFileService, FileService>();
        }

        public static void OnActivated(IActivatedEventArgs e)
        {
            if (e is ToastNotificationActivatedEventArgs)
            {
                var toastActivationArgs = e as ToastNotificationActivatedEventArgs;

                if (Serializer.Deserialize(toastActivationArgs.Argument, typeof(ActionParameters)) is ActionParameters argument)
                {
                    var notificationService = SimpleContainer.Instance.Create<NotificationService>();
                    var command = notificationService.GetRegistedNotificationAction(argument.ActionIdentifier);

                    if (command != null)
                    {
                        command.Execute(Serializer.Deserialize(argument.SerializedParam, Type.GetType(argument.SerializedParamType)));
                    }
                }
            }
        }

        public static void OnBackgroundActivated(BackgroundActivatedEventArgs e)
        {
        }
    }
}
