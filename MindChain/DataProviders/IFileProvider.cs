﻿using MindChain.Models;
using System;
using System.Threading.Tasks;

namespace MindChain.DataProviders
{
    public interface IFileProvider
    {
        Task<String> PublishFile(FileDatas Filedatas);
    }
}
