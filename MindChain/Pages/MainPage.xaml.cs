﻿using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async Task PageChanged(object sender, Menu.PageChangedArgs e)
        {
            await Detail.Navigation.PushAsync(e.Page);
            IsPresented = false;
        }
    }
}