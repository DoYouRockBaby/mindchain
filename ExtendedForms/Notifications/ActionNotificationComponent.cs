﻿using System;
using Xamarin.Forms;
using Xamarin.Medias;

namespace Xamarin.Notifications
{
    public class ActionNotificationComponent : NotificationComponent
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(ActionNotificationComponent), "Action");
        public static readonly BindableProperty IconProperty = BindableProperty.Create(nameof(Icon), typeof(MediaSource), typeof(ActionNotificationComponent), null);
        public static readonly BindableProperty ActionIdentifierProperty = BindableProperty.Create(nameof(ActionIdentifier), typeof(string), typeof(ActionNotificationComponent), "");
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ActionNotificationComponent), null);

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        [TypeConverter(typeof(MediaSourceConverter))]
        public MediaSource Icon
        {
            get { return (MediaSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public string ActionIdentifier
        {
            get { return (string)GetValue(ActionIdentifierProperty); }
            set { SetValue(ActionIdentifierProperty, value); }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
    }
}
