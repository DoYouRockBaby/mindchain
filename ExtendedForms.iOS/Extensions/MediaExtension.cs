﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using ExtendedForms.iOS.Medias;
using Xamarin.Medias;
using System.Threading.Tasks;
using Foundation;
using UserNotifications;
using Xamarin.Forms.Services;
using System.IO;
using System.Threading;
#if __MOBILE__
using UIKit;
#else
using AppKit;
using UIColor = AppKit.NSColor;
#endif

namespace ExtendedForms.iOS.Extensions
{
    public static class MediaExtension
    {
        public static async Task<UIImage> ToUIImage(this MediaSource self)
        {
            IMediaSourceHandler handler;

            if ((handler = Xamarin.Forms.Internals.Registrar.Registered.GetHandlerForObject<IMediaSourceHandler>(self)) != null)
            {
                if (handler is FileMediaSourceHandler)
                {
                    try
                    {
                        return await handler.LoadImageAsync(self, scale: (float)UIScreen.MainScreen.Scale);
                    }
                    catch (OperationCanceledException)
                    {
                    }
                }
            }

            return null;
        }

        public static async Task<UNNotificationAttachment> ToAttachment(this MediaSource self, string identifier, CancellationToken cancelationToken = default(CancellationToken))
        {
            NSUrl url = null;

            if (self is FileMediaSource)
            {
                var source = self as FileMediaSource;

                var file = source?.File;
                if (!string.IsNullOrEmpty(file))
                {
                    url = NSUrl.FromFilename(file);
                }
            }
            else if(self is UriMediaSource)
            {
                var source = self as UriMediaSource;

                var uri = source?.Uri;
                if (uri != null)
                {
                    url = new NSUrl(uri.AbsoluteUri);
                }
            }
            else
            {
                var source = self as StreamMediaSource;

                byte[] bytes;
                using (Stream stream = await source.GetStreamAsync(cancelationToken).ConfigureAwait(false))
                {
                    bytes = new byte[stream.Length];
                    await stream.ReadAsync(bytes, 0, (int)stream.Length);
                }


                var fileService = SimpleContainer.Instance.Create<IFileService>();
                var filename = fileService.CreateTempFile(bytes, ".tmp");
                url = NSUrl.FromFilename(filename);
            }

            if(url != null)
            {
                var options = new UNNotificationAttachmentOptions();
                return UNNotificationAttachment.FromIdentifier(identifier, url, options, out NSError err);
            }

            return null;
        }
    }
}