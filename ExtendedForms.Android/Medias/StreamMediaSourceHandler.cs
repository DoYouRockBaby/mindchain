﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Xamarin.Forms.Internals;
using Xamarin.Medias;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using ExtendedForms.Android.Medias;

[assembly: ExportMediaSourceHandler(typeof(StreamMediaSource), typeof(StreamMediaSourceHandler))]
namespace ExtendedForms.Android.Medias
{
    public sealed class StreamMediaSourceHandler : IMediaSourceHandler
    {
        public async Task<Bitmap> LoadImageAsync(MediaSource source, Context context, CancellationToken cancelationToken = default(CancellationToken))
        {
            var streamsource = source as StreamMediaSource;
            Bitmap bitmap = null;
            if (streamsource?.Stream != null)
            {
                using (Stream stream = await streamsource.GetStreamAsync(cancelationToken).ConfigureAwait(false))
                    bitmap = await BitmapFactory.DecodeStreamAsync(stream).ConfigureAwait(false);
            }

            if (bitmap == null)
            {
                Log.Warning(nameof(ImageLoaderSourceHandler), "Image data was invalid: {0}", streamsource);
            }

            return bitmap;
        }
    }
}
