﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Medias;

namespace Xamarin.Notifications
{
    public class Notification : Element, IResourcesProvider
    {
        readonly ObservableCollection<NotificationComponent> _components;

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(Notification), "A notification as been sent");
        public static readonly BindableProperty GroupProperty = BindableProperty.Create(nameof(Group), typeof(string), typeof(Notification), default(string));
        public static readonly BindableProperty SoundProperty = BindableProperty.Create(nameof(Sound), typeof(MediaSource), typeof(Notification), null);
        public static readonly BindableProperty ActionIdentifierProperty = BindableProperty.Create(nameof(ActionIdentifier), typeof(string), typeof(Notification), default(string));
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(Notification), null);
        public static readonly BindableProperty IconProperty = BindableProperty.Create(nameof(Icon), typeof(MediaSource), typeof(Notification), null);
        public static readonly BindableProperty DateTimeProperty = BindableProperty.Create(nameof(DateTime), typeof(DateTime), typeof(Notification), DateTime.Now);

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Group
        {
            get { return (string)GetValue(GroupProperty); }
            set { SetValue(GroupProperty, value); }
        }

        [TypeConverter(typeof(MediaSourceConverter))]
        public MediaSource Sound
        {
            get { return (MediaSource)GetValue(SoundProperty); }
            set { SetValue(SoundProperty, value); }
        }

        public string ActionIdentifier
        {
            get { return (string)GetValue(ActionIdentifierProperty); }
            set { SetValue(ActionIdentifierProperty, value); }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        [TypeConverter(typeof(MediaSourceConverter))]
        public MediaSource Icon
        {
            get { return (MediaSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public DateTime DateTime
        {
            get { return (DateTime)GetValue(DateTimeProperty); }
            set { SetValue(DateTimeProperty, value); }
        }

        public ObservableCollection<NotificationComponent> Components { get; } = new ObservableCollection<NotificationComponent>();

        ResourceDictionary _resources;
        public ResourceDictionary Resources
        {
            get
            {
                if (_resources != null)
                    return _resources;
                _resources = new ResourceDictionary();
                ((IResourceDictionary)_resources).ValuesChanged += OnResourcesChanged;
                return _resources;
            }
            set
            {
                if (_resources == value)
                    return;
                OnPropertyChanging();
                if (_resources != null)
                    ((IResourceDictionary)_resources).ValuesChanged -= OnResourcesChanged;
                _resources = value;
                OnResourcesChanged(value);
                if (_resources != null)
                    ((IResourceDictionary)_resources).ValuesChanged += OnResourcesChanged;
                OnPropertyChanged();
            }
        }

        public Notification()
        {
            _components = new ObservableCollection<NotificationComponent>();
            _components.CollectionChanged += OnCollectionChanged;
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if(propertyName == "BindingContext")
            {
                foreach(var item in Components)
                {
                    item.Parent = this;
                    item.BindingContext = BindingContext;
                }
            }
        }

        /// <summary>
        /// When the collection of pins changed
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
		void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (NotificationComponent item in e.NewItems)
                    {
                        item.Parent = this;
                        item.BindingContext = BindingContext;
                    }

                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (NotificationComponent item in e.OldItems)
                    {
                        if (item.Parent == this)
                        {
                            item.Parent = null;
                        }
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                    foreach (NotificationComponent item in e.OldItems)
                    {
                        if(item.Parent == this)
                        {
                            item.Parent = null;
                        }
                    }

                    foreach (NotificationComponent item in e.NewItems)
                    {
                        item.Parent = this;
                        item.BindingContext = BindingContext;
                    }

                    break;
                case NotifyCollectionChangedAction.Move:
                    //do nothing
                    break;
            }
        }
    }
}
