﻿using MindChain.Models;
using MindChain.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PickLocalizationPage : ContentPage
    {
        private PickLocalizationViewModel viewModel;

        public Localization Localization
        {
            get
            {
                return viewModel.Localization;
            }
        }

        public PickLocalizationPage()
        {
            viewModel = SimpleContainer.Instance.Create<PickLocalizationViewModel>();
            viewModel.ParentPage = this;
            BindingContext = viewModel;

            viewModel.PropertyChanged += ViewModelPropertyChanged;

            InitializeComponent();
        }

        private void ViewModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "MapCenter")
            {
                MapView.MoveToRegion(new MapSpan(viewModel.MapCenter, MapView.MapRegion.LatitudeDegrees, MapView.MapRegion.LongitudeDegrees), true);
            }
        }

        private async Task MapClicked(object sender, Map.PositionEventArgs e)
        {
            await viewModel.AddPin(e.Position);
        }

        private async void SuggestionChoosed(object sender, System.EventArgs e)
        {
            await viewModel.Search();
        }

        private async void SearchCompleted(object sender, System.EventArgs e)
        {
            await viewModel.Search();
        }
    }
}