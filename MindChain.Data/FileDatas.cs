﻿using System;

namespace MindChain.Data
{
    public class FileDatas
    {
        public string Name { get; set; }
        public byte[] Datas { get; set; }
    }
}
