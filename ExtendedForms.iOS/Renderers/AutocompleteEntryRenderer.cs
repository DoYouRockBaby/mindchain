using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System;
using Foundation;
using UIKit;
using System.Drawing;
using CoreGraphics;
using ExtendedForms.iOS.Extensions;
using Xamarin.Forms.Internals;
using ExtendedForms.iOS.UIViews;

[assembly: ExportRenderer(typeof(AutocompleteEntry), typeof(ExtendedForms.iOS.Renderers.AutocompleteEntryRenderer))]
namespace ExtendedForms.iOS.Renderers
{
    public class AutocompleteEntryRenderer : ViewRenderer<AutocompleteEntry, UIAutoCompleteTextField>
    {
        UIColor _defaultTextColor;
        bool _disposed;

        static readonly int baseHeight = 30;
        static CGSize initialSize = CGSize.Empty;
        ObservableCollection<object> _suggestions = new ObservableCollection<object>();
        ITemplatedItemsView<Cell> TemplatedItemsView => Element;
        IElementController ElementController => Element as IElementController;

        public AutocompleteEntryRenderer()
        {
            Frame = new RectangleF(0, 20, 320, 40);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<AutocompleteEntry> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;

            if (Control == null)
            {
                var textField = new UIAutoCompleteTextField(RectangleF.Empty)
                {
                    AutoCompleteViewSource = new ViewSource(this),
                    StartAutoCompleteAfterTicks = 0,
                    AutocompleteTableViewHeight = 500
                };

                SetNativeControl(textField);
                textField.Initialize();

                _defaultTextColor = textField.TextColor;
                textField.BorderStyle = UITextBorderStyle.RoundedRect;
                textField.ClipsToBounds = true;
                //textField.ShouldReturn = OnShouldReturn;

                textField.EditingChanged += OnEditingChanged;
                textField.EditingDidBegin += OnEditingBegan;
                textField.EditingDidEnd += OnEditingEnded;
            }

            UpdatePlaceholder();
            UpdateText();
            UpdateColor();
            UpdateFont();
            UpdateKeyboard();
            UpdateAlignment();
            UpdateKeyboardAction();
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            _disposed = true;

            if (disposing)
            {
                _defaultTextColor = null;

                if (Control != null)
                {
                    Control.EditingDidBegin -= OnEditingBegan;
                    Control.EditingChanged -= OnEditingChanged;
                    Control.EditingDidEnd -= OnEditingEnded;
                }
            }

            Control.Dispose();

            base.Dispose(disposing);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AutocompleteEntry.PlaceholderProperty.PropertyName || e.PropertyName == AutocompleteEntry.PlaceholderColorProperty.PropertyName)
            {
                UpdatePlaceholder();
            }
            else if (e.PropertyName == AutocompleteEntry.TextProperty.PropertyName)
            {
                UpdateText();
            }
            else if (e.PropertyName == AutocompleteEntry.TextColorProperty.PropertyName)
            {
                UpdateColor();
            }
            else if (e.PropertyName == AutocompleteEntry.HorizontalTextAlignmentProperty.PropertyName)
            {
                UpdateAlignment();
            }
            else if (e.PropertyName == AutocompleteEntry.FontAttributesProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == AutocompleteEntry.FontFamilyProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == AutocompleteEntry.FontSizeProperty.PropertyName)
            {
                UpdateFont();
            }
            else if (e.PropertyName == VisualElement.IsEnabledProperty.PropertyName)
            {
                UpdateColor();
                UpdatePlaceholder();
            }
            else if (e.PropertyName == AutocompleteEntry.KeyboardProperty.PropertyName)
            {
                UpdateKeyboard();
            }
            else if (e.PropertyName == AutocompleteEntry.IsSpellCheckEnabledProperty.PropertyName)
            {
                UpdateKeyboard();
            }
            else if (e.PropertyName == AutocompleteEntry.KeyboardActionProperty.PropertyName)
            {
                UpdateKeyboardAction();
            }

            base.OnElementPropertyChanged(sender, e);
        }

        public override SizeRequest GetDesiredSize(double widthConstraint, double heightConstraint)
        {
            var baseResult = base.GetDesiredSize(widthConstraint, heightConstraint);

            #if __MOBILE__
            if (Forms.Forms.IsiOS11OrNewer)
                return baseResult;
            #endif

            NSString testString = new NSString("Tj");
            var testSize = testString.GetSizeUsingAttributes(new UIStringAttributes { Font = Control.Font });
            double height = baseHeight + testSize.Height - initialSize.Height;
            height = Math.Round(height);

            return new SizeRequest(new Xamarin.Forms.Size(baseResult.Request.Width, height));
        }

        void OnEditingBegan(object sender, EventArgs e)
        {
            ElementController.SetValueFromRenderer(VisualElement.IsFocusedPropertyKey, true);
        }

        async void OnEditingChanged(object sender, EventArgs eventArgs)
        {
            ElementController.SetValueFromRenderer(AutocompleteEntry.TextProperty, Control.Text);

            Element.ItemsSource = null;

            if (Element.SuggestionProvider != null && Control.Text.Length >= Element.CharCountBeforeAutocomplete)
            {
                var newSuggestions = await Element.SuggestionProvider.GetSuggestions(Control.Text);
                Element.ItemsSource = newSuggestions;
            }

            Control.AutoCompleteTableView.ReloadData();
        }

        void OnEditingEnded(object sender, EventArgs e)
        {
            // Typing aid changes don't always raise EditingChanged event
            if (Control.Text != Element.Text)
            {
                ElementController.SetValueFromRenderer(AutocompleteEntry.TextProperty, Control.Text);
            }

            ElementController.SetValueFromRenderer(VisualElement.IsFocusedPropertyKey, false);
        }

        protected virtual bool OnShouldReturn(UITextField view)
        {
            Control.ResignFirstResponder();
            ((IEntryController)Element).SendCompleted();
            return false;
        }

        void UpdateAlignment()
        {
            Control.TextAlignment = Element.HorizontalTextAlignment.ToNativeTextAlignment();
        }

        void UpdateColor()
        {
            var textColor = Element.TextColor;

            if (textColor.IsDefault || !Element.IsEnabled)
                Control.TextColor = _defaultTextColor;
            else
                Control.TextColor = textColor.ToUIColor();
        }

        void UpdateFont()
        {
            if (initialSize == CGSize.Empty)
            {
                NSString testString = new NSString("Tj");
                initialSize = testString.StringSize(Control.Font);
            }

            Control.Font = Element.ToUIFont();
        }

        void UpdateKeyboard()
        {
            Control.ApplyKeyboard(Element.Keyboard);
            if (!(Element.Keyboard is CustomKeyboard) && Element.IsSet(AutocompleteEntry.IsSpellCheckEnabledProperty))
            {
                if (!Element.IsSpellCheckEnabled)
                {
                    Control.SpellCheckingType = UITextSpellCheckingType.No;
                }
            }
            Control.ReloadInputViews();
        }

        void UpdateKeyboardAction()
        {
            if (Control != null)
            {
                Control.ReturnKeyType = Element.KeyboardAction.ToIOS();
            }
        }

        void UpdatePlaceholder()
        {
            var formatted = (FormattedString)Element.Placeholder;

            if (formatted == null)
                return;

            var targetColor = Element.PlaceholderColor;

            // Placeholder default color is 70% gray
            // https://developer.apple.com/library/prerelease/ios/documentation/UIKit/Reference/UITextField_Class/index.html#//apple_ref/occ/instp/UITextField/placeholder

            var color = Element.IsEnabled && !targetColor.IsDefault ? targetColor : ColorExtension.SeventyPercentGrey.ToColor();

            Control.AttributedPlaceholder = formatted.ToAttributed(Element, color);
        }

        void UpdateText()
        {
            // ReSharper disable once RedundantCheckBeforeAssignment
            if (Control.Text != Element.Text)
                Control.Text = Element.Text;
        }

        class ViewSource : UITableViewSource
        {
            public AutocompleteEntryRenderer Renderer { get; private set; }

            public ViewSource(AutocompleteEntryRenderer renderer)
            {
                Renderer = renderer;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                Cell cell;
                UITableViewCell nativeCell;

                var reference = Guid.NewGuid().ToString();
                Performance.Start(reference);

                cell = GetCellForPath(indexPath);
                nativeCell = CellTableViewCell.GetNativeCell(tableView, cell);

                nativeCell.BackgroundColor = UIColor.Clear;
                Performance.Stop(reference);

                nativeCell.UserInteractionEnabled = true;
                return nativeCell;
            }

            protected Cell GetCellForPath(NSIndexPath indexPath)
            {
                var templatedItems = Renderer.TemplatedItemsView.TemplatedItems;
                var cell = templatedItems[indexPath.Row];
                return cell;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return Renderer.TemplatedItemsView.TemplatedItems.Count;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var item = Renderer.TemplatedItemsView.TemplatedItems[indexPath.Row].BindingContext;

                if (Renderer.Element.SuggestionProvider != null)
                {
                    Renderer.Control.Text = Renderer.Element.SuggestionProvider.ConvertToString(item);
                }
                else
                {
                    Renderer.Control.Text = item.ToString();
                }

                Renderer.Control.HideAutoCompleteView();
                Renderer.Element.SelectedItem = item;
            }
        }
    }
}