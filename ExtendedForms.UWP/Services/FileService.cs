﻿using System.IO;
using Xamarin.Forms.Services;

namespace ExtendedForms.UWP.Services
{
    class FileService : IFileService
    {
        public string CreateTempFile(byte[] bytes, string ext)
        {
            var filename = Path.GetTempFileName().Replace(".tmp", ext);

            using (var fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }

            return filename;
        }
    }
}