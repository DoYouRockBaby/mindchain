﻿using MindChain.Models;
using MindChain.ViewModels;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MindChain.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddContactPage : ContentPage
    {
        private AddContactViewModel viewModel;

        public AddContactPage()
        {
            viewModel = SimpleContainer.Instance.Create<AddContactViewModel>();
            viewModel.ParentPage = this;

            BindingContext = viewModel;

            InitializeComponent();
        }

        private void SearchTextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.UpdateSuggestedFriendsCommand.Execute(null);
        }

        private async Task FriendSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var listView = sender as ListView;
            var account = e.SelectedItem as Account;

            await viewModel.AskFriend(account.UserName);
            listView.SelectedItem = null;
        }
    }
}