﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using Plugin.FilePicker.Abstractions;
using System.Collections.ObjectModel;

namespace MindChain.Converters
{
    public class CountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var list = (ObservableCollection<FileData>)value;
            return list.Count.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
