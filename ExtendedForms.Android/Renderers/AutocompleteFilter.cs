﻿using System.Linq;
using Android.Widget;
using Java.Lang;
using System.Collections.Generic;

namespace ExtendedForms.Android.Renderers
{
    public class AutocompleteFilter : Filter
    {
        private readonly AutocompleteAdapter _adapter;

        public AutocompleteFilter(AutocompleteAdapter adapter)
        {
            _adapter = adapter;
        }

        protected override FilterResults PerformFiltering(ICharSequence constraint)
        {
            FilterResults returnObj = null;

            if (_adapter.AutocompleteEntry.SuggestionProvider == null || constraint == null || constraint.Length() < _adapter.AutocompleteEntry.CharCountBeforeAutocomplete)
            {
                returnObj = new FilterResults
                {
                    Values = FromArray(new List<object>().Cast<object>().Select(t => false).ToArray()),
                    Count = 0
                };

                return returnObj;
            }

            var suggestions = _adapter.AutocompleteEntry.SuggestionProvider.GetSuggestions(constraint.ToString()).Result;

            returnObj = new FilterResults
            {
                // Nasty piece of .NET to Java wrapping, be careful with this!
                Values = FromArray(suggestions.Cast<object>().Select(r => r.ToJavaObject()).ToArray()),
                Count = suggestions.OfType<object>().Count()
            };

            return returnObj;
        }

        protected override void PublishResults(ICharSequence constraint, FilterResults results)
        {
            using (var values = results.Values)
            {
                _adapter.AutocompleteEntry.ItemsSource = values.ToArray<Java.Lang.Object>().Select(r => r.ToNetObject<object>()).ToList();
            }

            _adapter.NotifyDataSetChanged();
        }
    }
}