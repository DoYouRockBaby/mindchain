﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sample.Pages.ListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainListView : TabbedPage
    {
        public class Entry : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            private string testString;
            public string TestString
            {
                get
                {
                    return testString;
                }
                set
                {
                    if (testString != value)
                    {
                        testString = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TestString)));
                    }
                }
            }

            private Color testColor;
            public Color TestColor
            {
                get
                {
                    return testColor;
                }
                set
                {
                    if (testColor != value)
                    {
                        testColor = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TestColor)));
                    }
                }
            }

            private int size;
            public int Size
            {
                get
                {
                    return size;
                }
                set
                {
                    if (size != value)
                    {
                        size = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Size)));
                    }
                }
            }
        }

        public class ViewModels : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public ObservableCollection<Entry> testList;
            public ObservableCollection<Entry> TestList
            {
                get
                {
                    return testList;
                }
                set
                {
                    if (testList != value)
                    {
                        testList = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TestList)));
                    }
                }
            }
        }
        public MainListView()
        {
            var list = new ObservableCollection<Entry>
            {
                new Entry { TestString = "Leo", TestColor = Color.Red, Size = rand.Next(50, 200) },
                new Entry { TestString = "Lea", TestColor = Color.GreenYellow, Size = rand.Next(50, 200) },
                new Entry { TestString = "Lou", TestColor = Color.AliceBlue, Size = rand.Next(50, 200) },
                new Entry { TestString = "Fanny", TestColor = Color.Violet, Size = rand.Next(50, 200) },
                new Entry { TestString = "Farid", TestColor = Color.Orange, Size = rand.Next(50, 200) },
            };

            for(int i = 0; i < 20; i++)
            {
                list.Add(new Entry { TestString = LoremNET.Lorem.Words(5, 10) + Environment.NewLine + LoremNET.Lorem.Words(5, 10) + Environment.NewLine + LoremNET.Lorem.Words(5, 10), TestColor = GetRandomColor(), Size = rand.Next(50, 200) });
            }
            /*foreach (var str in COUNTRIES)
            {
                list.Add(new Entry { TestString = str, TestColor = GetRandomColor(), Size = rand.Next(50, 200) });
                list.Add(new Entry { TestString = LoremNET.Lorem.Words(5, 10) + Environment.NewLine + LoremNET.Lorem.Words(5, 10) + Environment.NewLine + LoremNET.Lorem.Words(5, 10), TestColor = GetRandomColor(), Size = rand.Next(50, 200) });
            };*/

            var vm = new ViewModels
            {
                TestList = list
            };

            BindingContext = vm;

            InitializeComponent();
        }

        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            return Color.FromHsla((hue / 255.0f), 1.0f, 0.5f, 1.0f);
        }

        static string[] COUNTRIES = new string[] {
            "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra",
            "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina",
            "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan",
            "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
            "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
            "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory",
            "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi",
            "Cote d'Ivoire", "Cambodia", "Cameroon", "Canada", "Cape Verde",
            "Cayman Islands", "Central African Republic", "Chad", "Chile", "China",
            "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
            "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic",
            "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic",
            "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea",
            "Estonia", "Ethiopia", "Faeroe Islands", "Falkland Islands", "Fiji", "Finland",
            "Former Yugoslav Republic of Macedonia", "France", "French Guiana", "French Polynesia",
            "French Southern Territories", "Gabon", "Georgia", "Germany", "Ghana", "Gibraltar",
            "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau",
            "Guyana", "Haiti", "Heard Island and McDonald Islands", "Honduras", "Hong Kong", "Hungary",
            "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica",
            "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Laos",
            "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
            "Macau", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands",
            "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova",
            "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia",
            "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand",
            "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Marianas",
            "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
            "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar",
            "Reunion", "Romania", "Russia", "Rwanda", "Sqo Tome and Principe", "Saint Helena",
            "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon",
            "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Saudi Arabia", "Senegal",
            "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands",
            "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "South Korea",
            "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden",
            "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Bahamas",
            "The Gambia", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
            "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Virgin Islands", "Uganda",
            "Ukraine", "United Arab Emirates", "United Kingdom",
            "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan",
            "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Wallis and Futuna", "Western Sahara",
            "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"
        };
    }
}