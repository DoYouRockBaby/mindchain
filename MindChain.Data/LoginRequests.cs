﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MindChain.Data
{
    public class RegisterRequest
    {
        public Account Account { get; set; }
        public string Password { get; set; }
    }

    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
