﻿using ExtendedForms.UWP.Notifications;
using Microsoft.Toolkit.Uwp.Notifications;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Notifications;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Notifications;
using Xamarin.Utility;
using static ExtendedForms.UWP.Notifications.ActionNotificationComponentRenderer;

namespace ExtendedForms.UWP.Services
{
    class NotificationService : INotificationService
    {
        protected static int _currentNotificationIdentifier = 0;
        private static Dictionary<string, Command> _actionIdentifiers = new Dictionary<string, Command>();

        public async Task<int> PushNotificationAsync(Xamarin.Notifications.Notification notification)
        {
            //Initialize notification building objects
            var bindingGeneric = new ToastBindingGeneric()
            {
                Children =
                {
                    new AdaptiveText()
                    {
                        Text = notification.Title,
                        HintMaxLines = 1
                    }
                },
            };

            var content = new ToastContent()
            {
                Launch = "app-defined-string",
                Visual = new ToastVisual()
                {
                    BindingGeneric = bindingGeneric
                },
                DisplayTimestamp = notification.DateTime
            };

            //Travese components
            foreach (var component in notification.Components)
            {
                var renderer = NotificationComponentRenderer.GetRenderer(component);

                if(renderer != null)
                {
                    renderer.SetComponent(component);
                    await renderer.SetUpNotificationAsync(content, bindingGeneric);
                }
            }

            //Define click action
            ActionParameters arguments = null;

            if (notification.CommandParameter == null)
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = notification.ActionIdentifier,
                    SerializedParam = "",
                    SerializedParamType = ""
                };
            }
            else
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = notification.ActionIdentifier,
                    SerializedParam = Serializer.Serialize(notification.CommandParameter),
                    SerializedParamType = notification.CommandParameter.GetType().ToString()
                };
            }

            content.Launch = Serializer.Serialize(arguments);

            //Send notification
            var toast = new ToastNotification(content.GetXml());
            ToastNotificationManager.CreateToastNotifier().Show(toast);

            return 0;
        }

        public void CancelNotification(int id)
        {
            /*var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.Cancel(id);*/
        }

        public void CancelAllNotification()
        {
            /*var notificationManager = Forms.Forms.Context.GetSystemService(Context.NotificationService) as NotificationManager;
            notificationManager.CancelAll();*/
        }

        public Command GetRegistedNotificationAction(string identifier)
        {
            _actionIdentifiers.TryGetValue(identifier, out Command value);
            return value;

        }

        public void RegisterNotificationAction(string identifier, NotificationAction action)
        {
            if(_actionIdentifiers.ContainsKey(identifier))
            {
                return;
            }

            _actionIdentifiers.Add(identifier, action.Command);
            
        }
    }
}