﻿using MindChain.DataProviders;
using MindChain.Models;
using MindChain.Pages;
using Plugin.FilePicker;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class NewPostViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IAccountProvider accountProvider;
        private IPostProvider postProvider;
        private IFileProvider fileProvider;

        public NewPostViewModel(IAccountProvider accountProvider, IPostProvider postProvider, IFileProvider fileProvider)
        {
            this.accountProvider = accountProvider;
            this.postProvider = postProvider;
            this.fileProvider = fileProvider;

            AddFileCommand = new Command(async () => await AddFile());
            AddLocationCommand = new Command(async () => await AddLocation());
            SendCommand = new Command(async () => await Send());
        }

        public ICommand AddFileCommand { get; set; }
        public ICommand AddLocationCommand { get; set; }
        public ICommand SendCommand { get; set; }

        Post newPost = new Post();
        public Post NewPost
        {
            get
            {
                return newPost;
            }
            set
            {
                if (value != newPost)
                {
                    newPost = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NewPost)));
                }
            }
        }

        private ObservableCollection<FileViewModel> files = new ObservableCollection<FileViewModel>();
        public ObservableCollection<FileViewModel> Files
        {
            get
            {
                return files;
            }
            set
            {
                if (value != files)
                {
                    files = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Files)));

                    Files.CollectionChanged += (s, e) =>
                    {
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FilesCount)));
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasFile)));
                    };
                }
            }
        }

        public int FilesCount
        {
            get
            {
                return Files.Count;
            }
        }

        public bool HasFile
        {
            get
            {
                return Files.Count > 0;
            }
        }

        public bool HasLocation
        {
            get
            {
                return NewPost.Location != null;
            }
        }

        public async Task AddFile()
        {
            var file = await CrossFilePicker.Current.PickFile();

            if (file != null)
            {
                Files.Add(new FileViewModel(file, Files) { ParentPage = ParentPage });
            }
        }

        public async Task AddLocation()
        {
            var newPage = new PickLocalizationPage();
            newPage.Disappearing += (e, s) =>
            {
                if (newPage.Localization != null)
                {
                    NewPost.Location = newPage.Localization;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(HasLocation)));
                }
            };

            await ParentPage.Navigation.PushModalAsync(newPage);
        }

        public async Task Send()
        {
            //Send file
            foreach(var file in Files)
            {
                var url = await fileProvider.PublishFile(new FileDatas()
                {
                    Name = file.File.FileName,
                    Datas = file.File.DataArray
                });

                NewPost.Files.Add(new FileUrl()
                {
                    Name = file.File.FileName,
                    Url = url
                });
            }

            //Add informations
            NewPost.Date = DateTime.Now;
            NewPost.AccountId = (await accountProvider.CurrentUser()).Id;

            //Send new post
            await postProvider.Add(NewPost);
        }
    }
}
