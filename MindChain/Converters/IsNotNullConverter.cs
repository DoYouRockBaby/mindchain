﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class IsNotNullConverter : IValueConverter
    {
        public object nullValue = false;
        public object NullValue
        {
            get
            {
                return nullValue;
            }
            set
            {
                nullValue = value;
            }
        }

        public object notNullValue = true;
        public object NotNullValue
        {
            get
            {
                return notNullValue;
            }
            set
            {
                notNullValue = value;
            }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
