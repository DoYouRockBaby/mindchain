﻿using CoreGraphics;
using System;
using System.ComponentModel;
using System.Drawing;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms.Internals;

[assembly: ExportRenderer(typeof(ExtendedListView), typeof(ExtendedForms.iOS.Renderers.ExtendedListViewRenderer))]
namespace ExtendedForms.iOS.Renderers
{
    class ExtendedListViewRenderer : ViewRenderer<ExtendedListView, UICollectionView>
    {
        bool _disposed;
        UICollectionViewDataSource _dataSource;
        UICollectionViewFlowLayout _layout;

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            _disposed = true;

            if (disposing)
            {
                _dataSource.Dispose();

            }

            base.Dispose(disposing);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ExtendedListView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                if (_dataSource != null)
                {
                    _dataSource.Dispose();
                    _layout.Dispose();

                    _dataSource = null;
                    _layout = null;
                }
            }

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    _layout = new UICollectionViewFlowLayout()
                    {
                        EstimatedItemSize = new CGSize(1, 1),
                        MinimumInteritemSpacing = 0.0f,
                        MinimumLineSpacing = 0.0f,
                    };

                    var native = new UICollectionView(RectangleF.Empty, _layout)
                    {
                        DataSource = new ExtendedListViewDataSource(Element),
                    };
                    native.RegisterClassForCell(typeof(ExtendedListViewDataSource.CollectionViewCell), nameof(ExtendedListViewDataSource.CollectionViewCell));

                    SetNativeControl(native);
                }

                UpdateDataset();
                UpdateLayouting();
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case nameof(ExtendedListView.ItemsSource):
                    UpdateDataset();
                    break;
                case nameof(ExtendedListView.ItemTemplate):
                    UpdateDataset();
                    break;
                case nameof(ExtendedListView.Orientation):
                    UpdateLayouting();
                    break;
            }
        }

        protected void UpdateDataset()
        {
        }

        protected void UpdateLayouting()
        {
            if (_layout != null)
            {
                if (Element.Orientation == ExtendedListView.ListViewOrientation.Vertical)
                {
                    _layout.ScrollDirection = UICollectionViewScrollDirection.Vertical;
                }
                else
                {
                    _layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
                }
            }
        }
    }
}