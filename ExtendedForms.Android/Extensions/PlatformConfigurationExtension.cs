﻿using Xamarin.Forms;

namespace ExtendedForms.Android.Extensions
{
    public static class PlatformConfigurationExtension
    {
        public static IPlatformElementConfiguration<Xamarin.Forms.PlatformConfiguration.Android, T> OnThisPlatform<T>(this T element)
    where T : Element, IElementConfiguration<T>
        {
            return (element).On<Xamarin.Forms.PlatformConfiguration.Android>();
        }
    }
}