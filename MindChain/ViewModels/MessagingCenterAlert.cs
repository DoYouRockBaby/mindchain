﻿namespace MindChain.ViewModels
{
    internal class MessagingCenterAlert
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string Cancel { get; set; }
    }
}