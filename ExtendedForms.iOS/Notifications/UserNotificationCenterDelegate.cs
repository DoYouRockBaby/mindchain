﻿using ExtendedForms.iOS.Services;
using Foundation;
using System;
using UserNotifications;
using Xamarin.Forms;
using Xamarin.Utility;
using static ExtendedForms.iOS.Notifications.ActionNotificationComponentRenderer;

namespace ExtendedForms.iOS.Notifications
{
    class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        public UserNotificationCenterDelegate()
        {
        }

        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            completionHandler(UNNotificationPresentationOptions.Alert);
        }

        public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            var parameters = response.Notification.Request.Content.UserInfo;

            if (response.IsDefaultAction)
            {
                var parameter = parameters.TryGetValue(new NSString("ExtendedForms.iOS.Notifications.DefautlAction"), out NSObject argumentString);
                if (argumentString is NSString strArgument && Serializer.Deserialize(strArgument, typeof(ActionParameters)) is ActionParameters argument)
                {
                    ExecuteActionParameters(argument);
                }
            }
            else if(response.IsCustomAction)
            {
                var parameter = parameters.TryGetValue(response.ActionIdentifier, out NSObject argumentString);
                if (argumentString is NSString strArgument && Serializer.Deserialize(strArgument, typeof(ActionParameters)) is ActionParameters argument)
                {
                    ExecuteActionParameters(argument);
                }
            }

            // Inform caller it has been handled
            completionHandler();
        }

        void ExecuteActionParameters(ActionParameters ap)
        {
            if(ap.ActionIdentifier != "")
            {
                var notificationService = SimpleContainer.Instance.Create<NotificationService>();
                var command = notificationService.GetRegistedNotificationAction(ap.ActionIdentifier);

                if (command != null)
                {
                    command.Execute(Serializer.Deserialize(ap.SerializedParam, Type.GetType(ap.SerializedParamType)));
                }
            }
        }
    }
}