﻿using Android.Content;
using Android.Views;
using Android.Widget;
using AView = Android.Views.View;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using System.Collections;
using System;
using Xamarin.Forms.Internals;
using Android.Util;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace ExtendedForms.Android.Renderers
{
    public class AutocompleteAdapter : CellAdapter, IFilterable
    {
        static int s_dividerHorizontalDarkId = int.MinValue;
        readonly Context _context;
        public AutocompleteEntry AutocompleteEntry => _autocompleteEntry;
        protected readonly AutocompleteEntry _autocompleteEntry;
        readonly AutoCompleteTextView _realListView;
        int _listCount = -1; // -1 we need to get count from the list
        public Filter _filter;
        IListViewController Controller => _autocompleteEntry;
        protected ITemplatedItemsView<Cell> TemplatedItemsView => _autocompleteEntry;

        public AutocompleteAdapter(Context context, AutoCompleteTextView realListView, AutocompleteEntry listView) : base(context)
        {
            _context = context;
            _realListView = realListView;
            _autocompleteEntry = listView;
            _filter = new AutocompleteFilter(this);

            var templatedItems = ((ITemplatedItemsView<Cell>)listView).TemplatedItems;
            templatedItems.CollectionChanged += OnCollectionChanged;
            templatedItems.GroupedCollectionChanged += OnGroupedCollectionChanged;

            realListView.OnItemClickListener = this;
            InvalidateCount();
        }

        public override int Count
        {
            get
            {
                if (_listCount == -1)
                {
                    var templatedItems = TemplatedItemsView.TemplatedItems;
                    int count = templatedItems.Count;
                    _listCount = count;
                }
                return _listCount;
            }
        }

        public override bool HasStableIds
        {
            get { return false; }
        }

        public bool IsAttachedToWindow { get; set; }

        public override object this[int position]
        {
            get
            {
                var items = _autocompleteEntry.ItemsSource as IList;

                if (items == null)
                {
                    return null;
                }

                if (_autocompleteEntry.SuggestionProvider != null)
                {
                    return _autocompleteEntry.SuggestionProvider.ConvertToString(items[position]);
                }
                else
                {
                    return items[position];
                }
            }
        }

        public Filter Filter => _filter;

        public override bool AreAllItemsEnabled()
        {
            return false;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override AView GetView(int position, AView convertView, ViewGroup parent)
        {
            var reference = Guid.NewGuid().ToString();
            Performance.Start(reference);

            Cell cell = GetCellForPosition(position);
            if (cell == null)
                return new AView(_context);

            var cellIsBeingReused = false;
            if (convertView is ConditionalFocusLayout layout)
            {
                cellIsBeingReused = true;
                convertView = layout.GetChildAt(0);
            }
            else
                layout = new ConditionalFocusLayout(_context) { Orientation = Orientation.Vertical };

            AView view = CellFactory.GetCell(cell, convertView, parent, _context, _autocompleteEntry);

            Performance.Start(reference, "AddView");

            if (cellIsBeingReused)
            {
                if (convertView != view)
                {
                    layout.RemoveViewAt(0);
                    layout.AddView(view, 0);
                }
            }
            else
                layout.AddView(view, 0);

            Performance.Stop(reference, "AddView");


            UpdateSeparatorVisibility(cell, cellIsBeingReused, layout, out AView bline);

            UpdateSeparatorColor(bline);

            layout.ApplyTouchListenersToSpecialCells(cell);

            Performance.Stop(reference);

            return layout;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _realListView.OnItemClickListener = null;

                var templatedItems = TemplatedItemsView.TemplatedItems;
                templatedItems.CollectionChanged -= OnCollectionChanged;
                templatedItems.GroupedCollectionChanged -= OnGroupedCollectionChanged;
            }

            base.Dispose(disposing);
        }

        protected override Cell GetCellForPosition(int position)
        {
            var templatedItems = TemplatedItemsView.TemplatedItems;
            var templatedItemsCount = templatedItems.Count;
            return templatedItems[position];
        }

        protected override void HandleItemClick(AdapterView parent, AView view, int position, long id)
        {
            if (_autocompleteEntry.ItemsSource is IList items)
            {
                _autocompleteEntry.SelectedItem = items[position];
            }

        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnDataChanged();
        }

        void OnDataChanged()
        {
            InvalidateCount();

            if (IsAttachedToWindow)
                NotifyDataSetChanged();
            else
            {
                _realListView.Adapter = _realListView.Adapter;
            }
        }

        void OnGroupedCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnDataChanged();
        }

        void UpdateSeparatorVisibility(Cell cell, bool cellIsBeingReused, ConditionalFocusLayout layout, out AView bline)
        {
            bline = null;
            if (cellIsBeingReused)
                return;
            bool isSeparatorVisible = _autocompleteEntry.SeparatorVisibility == SeparatorVisibility.Default;
            var makeBline = isSeparatorVisible;
            if (makeBline)
            {
                bline = new AView(_context) { LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, 1) };
                layout.AddView(bline);
            }
            else if (layout.ChildCount > 1)
            {
                layout.RemoveViewAt(1);
            }
        }


        void UpdateSeparatorColor(AView bline)
        {
            if (bline == null)
                return;

            Color separatorColor = _autocompleteEntry.SeparatorColor;

            if (!separatorColor.IsDefault)
                bline.SetBackgroundColor(separatorColor.ToAndroid(Color.Accent));
            else
            {
                if (s_dividerHorizontalDarkId == int.MinValue)
                {
                    using (var value = new TypedValue())
                    {
                        int id = global::Android.Resource.Drawable.DividerHorizontalDark;
                        if (_context.Theme.ResolveAttribute(global::Android.Resource.Attribute.ListDivider, value, true))
                            id = value.ResourceId;
                        else if (_context.Theme.ResolveAttribute(global::Android.Resource.Attribute.Divider, value, true))
                            id = value.ResourceId;

                        s_dividerHorizontalDarkId = id;
                    }
                }

                bline.SetBackgroundResource(s_dividerHorizontalDarkId);
            }
        }

        protected virtual void InvalidateCount()
        {
            _listCount = -1;
        }
    }
}