﻿using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Notifications;

namespace Xamarin.Extensions
{
    public static class PageExtension
    {
        public static async Task<int> PushNotificationAsync(this Page self, Notification notification)
        {
            return await self.PushNotificationAsync(notification, notification.BindingContext);
        }

        public static async Task<int> PushNotificationAsync(this Page self, Notification notification, object bindingContext)
        {
            notification.BindingContext = bindingContext;
            notification.Resources.MergedWith = self.GetType();

            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            return await notificationService.PushNotificationAsync(notification);
        }
    }
}
