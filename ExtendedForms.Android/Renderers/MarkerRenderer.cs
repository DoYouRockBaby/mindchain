﻿using Android.Content;
using Com.Google.Maps.Android.Clustering.View;
using Android.Gms.Maps;
using Com.Google.Maps.Android.Clustering;
using Android.Gms.Maps.Model;
using Com.Google.Maps.Android.UI;

namespace ExtendedForms.Android.Renderers
{
    public class MarkerRenderer : DefaultClusterRenderer
    {
        Context _context;
        GoogleMap _googleMap;
        MapRenderer _mapRenderer;
        IconGenerator _iconGenerator;

        public MarkerRenderer(Context context, GoogleMap googleMap, ClusterManager clusterManager, MapRenderer mapRenderer) :
            base(context, googleMap, clusterManager)
        {
            _context = context;
            _googleMap = googleMap;
            _mapRenderer = mapRenderer;
            _iconGenerator = new IconGenerator(context);
        }

        protected async override void OnBeforeClusterItemRendered(Java.Lang.Object p0, MarkerOptions p1)
        {
            var tkMarker = p0 as CustomMarker;

            if (tkMarker == null) return;

            await tkMarker.InitializeMarkerOptionsAsync(p1);
        }
        protected override void OnClusterItemRendered(Java.Lang.Object p0, Marker p1)
        {
            base.OnClusterItemRendered(p0, p1);

            var tkMarker = p0 as CustomMarker;

            if (tkMarker == null) return;

            tkMarker.Marker = p1;
        }

        protected override void OnBeforeClusterRendered(ICluster p0, MarkerOptions p1)
        {
            base.OnBeforeClusterRendered(p0, p1);
            p1.SetIcon(BitmapDescriptorFactory.FromBitmap(_iconGenerator.MakeIcon(p0.Size.ToString())));
        }

        protected override void OnClusterRendered(ICluster p0, Marker p1)
        {
            base.OnClusterRendered(p0, p1);

            var tkMarker = p0 as CustomMarker;

            if (tkMarker == null) return;

            tkMarker.Marker = p1;
        }
    }
}