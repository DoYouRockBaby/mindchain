﻿using Android.Graphics;
using System.IO;
using Xamarin.Forms;
using ZXing;
using ZXing.Common;
using ZXing.Mobile;

namespace MindChain.Service
{
    public class QrCodeService : IQrCodeService
    {
        ImageSource IQrCodeService.ImageFromCode(string code)
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions { Height = 60, Width = 60 }
            };

            var image = writer.Write(code);

            return ImageSource.FromStream(() =>
            {
                MemoryStream ms = new MemoryStream();
                image.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                ms.Seek(0L, SeekOrigin.Begin);
                return ms;
            });
        }
    }
}
