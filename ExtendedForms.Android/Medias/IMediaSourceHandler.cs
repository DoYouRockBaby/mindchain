﻿using Android.Content;
using Android.Graphics;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Medias;

namespace ExtendedForms.Android.Medias
{
    public interface IMediaSourceHandler : IRegisterable
    {
        Task<Bitmap> LoadImageAsync(MediaSource source, Context context, CancellationToken cancelationToken = default(CancellationToken));
    }
}