﻿using System.Threading.Tasks;
using Xamarin.Notifications;
using UserNotifications;
using System.Collections.Generic;
using ExtendedForms.iOS.Notifications;
using Foundation;
using Xamarin.Utility;

[assembly: ExportNotificationComponent(typeof(ActionNotificationComponent), typeof(ActionNotificationComponentRenderer))]
namespace ExtendedForms.iOS.Notifications
{
    public class ActionNotificationComponentRenderer : NotificationComponentRenderer<ActionNotificationComponent>
    {
        public class ActionParameters
        {
            public string ActionIdentifier;
            public string SerializedParam;
            public string SerializedParamType;
        }

        public override Task SetUpNotificationAsync(UNMutableNotificationContent notification, List<UNNotificationAction> actions)
        {
            //Create callback object
            ActionParameters arguments = null;
            if (Component.CommandParameter == null)
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = Component.ActionIdentifier,
                    SerializedParam = "",
                    SerializedParamType = ""
                };
            }
            else
            {
                arguments = new ActionParameters
                {
                    ActionIdentifier = Component.ActionIdentifier,
                    SerializedParam = Serializer.Serialize(Component.CommandParameter),
                    SerializedParamType = Component.CommandParameter.GetType().ToString()
                };
            }

            //Inject it into the notification
            if (notification.UserInfo == null)
            {
                notification.UserInfo = new NSDictionary(Component.ActionIdentifier, arguments.Serialize());
            }
            else
            {
                var previousUserInfo = new NSMutableDictionary(notification.UserInfo)
                    {
                        { NSObject.FromObject(Component.ActionIdentifier), NSObject.FromObject(arguments.Serialize()) }
                    };

                notification.UserInfo = new NSDictionary(previousUserInfo);
            }

            //Create action, it will be added while category creation into NotificationService
            var action = UNNotificationAction.FromIdentifier(Component.ActionIdentifier, Component.Text, UNNotificationActionOptions.None);
            actions.Add(action);

            return Task.CompletedTask;
        }
    }
}