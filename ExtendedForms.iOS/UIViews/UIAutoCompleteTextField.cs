﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Drawing;

namespace ExtendedForms.iOS.UIViews
{
	[Register("UIAutoCompleteTextField")]
	public class UIAutoCompleteTextField : UITextField, IUITextFieldDelegate
	{
		public UITableViewSource AutoCompleteViewSource { get; set; }
		public UITableView AutoCompleteTableView { get; private set; }
		public int StartAutoCompleteAfterTicks { get; set; } = 2;
		public int AutocompleteTableViewHeight { get; set; } = 150;

		public UIAutoCompleteTextField() : base(){ }
        public UIAutoCompleteTextField(NSCoder coder) : base(coder) { }
        public UIAutoCompleteTextField(NSObjectFlag t) : base(t) { }
        public UIAutoCompleteTextField(RectangleF frame) : base(frame) { }
        public UIAutoCompleteTextField(IntPtr ptr) : base(ptr) { }

		public void Initialize()
		{
			//Make new tableview and do some settings
			AutoCompleteTableView = new UITableView();
			AutoCompleteTableView.Layer.CornerRadius = 5; //rounded corners
			AutoCompleteTableView.ContentInset = UIEdgeInsets.Zero;
			AutoCompleteTableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight; //for resizing (switching from table to portait for example)
			AutoCompleteTableView.Bounces = false;
			AutoCompleteTableView.BackgroundColor = UIColor.White;
		    AutoCompleteTableView.TranslatesAutoresizingMaskIntoConstraints = false;
            AutoCompleteTableView.Source = AutoCompleteViewSource;
			AutoCompleteTableView.TableFooterView = new UIView();
			AutoCompleteTableView.Hidden = true;
            AutoCompleteTableView.AllowsSelection = true;
            AutoCompleteTableView.UserInteractionEnabled = true;
            AutoCompleteTableView.ScrollEnabled = true;

            //Some textfield settings
            TranslatesAutoresizingMaskIntoConstraints = false;
			Delegate = this;
			AutocorrectionType = UITextAutocorrectionType.No;
			ClearButtonMode = UITextFieldViewMode.WhileEditing;

            //listen to edit events
            this.EditingChanged += (sender, eventargs) =>
            {
                if (Text.Length > StartAutoCompleteAfterTicks)
                {
                    ShowAutoCompleteView();
                }
            };

			this.EditingDidEnd += (sender, eventargs) =>
			{
				HideAutoCompleteView();
			};
		}

        public override void MovedToWindow()
        {
            base.MovedToWindow();

            if(Window != null)
            {
                UIView rootView = Window.Subviews[0];
                rootView.InsertSubviewBelow(AutoCompleteTableView, this);

                NSLayoutConstraint.Create(AutoCompleteTableView, NSLayoutAttribute.Width, NSLayoutRelation.Equal, Superview, NSLayoutAttribute.Width, 1.0f, 0.0f).Active = true;
                NSLayoutConstraint.Create(AutoCompleteTableView, NSLayoutAttribute.Left, NSLayoutRelation.Equal, Superview, NSLayoutAttribute.Left, 1.0f, 0.0f).Active = true;
                NSLayoutConstraint.Create(AutoCompleteTableView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, Superview, NSLayoutAttribute.Bottom, 1.0f, 0.0f).Active = true;

                AutoCompleteTableView.HeightAnchor.ConstraintEqualTo(AutocompleteTableViewHeight).Active = true;
            }
        }

        public void ShowAutoCompleteView()
		{
			AutoCompleteTableView.SetContentOffset(CGPoint.Empty, false);
			AutoCompleteTableView.Hidden = false;
        }

		public void HideAutoCompleteView()
		{
			AutoCompleteTableView.Hidden = true;
		}
	}
}