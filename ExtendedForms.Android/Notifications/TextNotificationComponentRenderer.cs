﻿using Android.Support.V4.App;
using ExtendedForms.Android.Notifications;
using System.Threading.Tasks;
using Xamarin.Notifications;

[assembly: ExportNotificationComponent(typeof(TextNotificationComponent), typeof(TextNotificationComponentRenderer))]
namespace ExtendedForms.Android.Notifications
{
    public class TextNotificationComponentRenderer : NotificationComponentRenderer<TextNotificationComponent>
    {
        public override Task SetUpBuilderAsync(NotificationCompat.Builder builder)
        {
            if(Component.IsSubtext)
            {
                builder.SetSubText(Component.Text);
            }
            else
            {
                builder.SetContentText(Component.Text);
            }

            return Task.CompletedTask;
        }
    }
}