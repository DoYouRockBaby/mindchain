﻿using Xamarin.Forms;

namespace ExtendedForms.UWP.Extensions
{
    internal static class AlignmentExtensions
    {
        internal static Windows.UI.Xaml.TextAlignment ToNativeTextAlignment(this TextAlignment alignment)
        {
            switch (alignment)
            {
                case TextAlignment.Center:
                    return Windows.UI.Xaml.TextAlignment.Center;
                case TextAlignment.End:
                    return Windows.UI.Xaml.TextAlignment.Right;
                default:
                    return Windows.UI.Xaml.TextAlignment.Left;
            }
        }

        internal static Windows.UI.Xaml.HorizontalAlignment ToNativeHorizontalAlignment(this TextAlignment alignment)
        {
            switch (alignment)
            {
                case TextAlignment.Center:
                    return Windows.UI.Xaml.HorizontalAlignment.Center;
                case TextAlignment.End:
                    return Windows.UI.Xaml.HorizontalAlignment.Right;
                default:
                    return Windows.UI.Xaml.HorizontalAlignment.Left;
            }
        }

        internal static Windows.UI.Xaml.VerticalAlignment ToNativeVerticalAlignment(this TextAlignment alignment)
        {
            switch (alignment)
            {
                case TextAlignment.Center:
                    return Windows.UI.Xaml.VerticalAlignment.Center;
                case TextAlignment.End:
                    return Windows.UI.Xaml.VerticalAlignment.Bottom;
                default:
                    return Windows.UI.Xaml.VerticalAlignment.Top;
            }
        }
    }
}
