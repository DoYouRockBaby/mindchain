﻿using MindChain.DataProviders;
using MindChain.Helpers;
using MindChain.Models;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class MapPostsViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private IAccountProvider accountProvider;
        private IPostProvider postProvider;

        public MapPostsViewModel(IAccountProvider accountProvider, IPostProvider postProvider)
        {
            this.accountProvider = accountProvider;
            this.postProvider = postProvider;

            Posts = new ObservableRangeCollection<Post>();

            LoadPostsCommand = new Command(async () => await ExecuteLoadPostsCommand());

            new Command(async () => CurrentAccount = await accountProvider.CurrentUser()).Execute(null);
            
        }

        public Command LoadPostsCommand { get; set; }

        public Account currentAccount = null;
        public Account CurrentAccount
        {
            get
            {
                return currentAccount;
            }
            set
            {
                if (value != currentAccount)
                {
                    currentAccount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentAccount)));
                    LoadPostsCommand.Execute(null);
                }
            }
        }
        
        private ObservableRangeCollection<Post> posts = new ObservableRangeCollection<Post>();
        public ObservableRangeCollection<Post> Posts
        {
            get
            {
                return posts;
            }
            set
            {
                if (value != posts)
                {
                    posts = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Posts)));
                }
            }
        }

        bool isBusy = false;
        public bool IsBusy
        {
            get
            {
                return isBusy;
            }
            set
            {
                if (value != isBusy)
                {
                    isBusy = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsBusy)));
                }
            }
        }

        Position mapCenter = new Position(0.0, 0.0);
        public Position MapCenter
        {
            get
            {
                return MapCenter;
            }
            set
            {
                if(value != mapCenter)
                {
                    mapCenter = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MapCenter)));
                }
            }
        }

        public async Task ExecuteLoadPostsCommand()
        {
            if (IsBusy || CurrentAccount == null)
                return;

            IsBusy = true;

            try
            {
                Posts.Clear();
                var posts = await postProvider.GetLocalizable();
                Posts.ReplaceRange(posts);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                MessagingCenter.Send(new MessagingCenterAlert
                {
                    Title = "Error",
                    Message = "Unable to load items.",
                    Cancel = "OK"
                }, "message");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
