﻿using System;

namespace MindChain.Models
{
    public class FileDatas
    {
        public string Name { get; set; }
        public byte[] Datas { get; set; }
    }
}
