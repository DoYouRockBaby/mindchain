﻿using System;
using System.Globalization;
using System.IO;
using Xamarin.Forms;

namespace MindChain.Converters
{
    public class FileTypeFromFilename : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Path.GetExtension((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
