﻿using Plugin.FilePicker.Abstractions;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace MindChain.ViewModels
{
    public class FileViewModel : AbstractViewModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<FileViewModel> others = null;

        public FileViewModel(FileData file, ObservableCollection<FileViewModel> others)
        {
            File = file;
            this.others = others;

            RemoveCommand = new Command(() => Remove());
        }

        public Command RemoveCommand { get; set; }

        FileData file = new FileData();
        public FileData File
        {
            get
            {
                return file;
            }
            set
            {
                if (value != file)
                {
                    file = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(File)));
                }
            }
        }

        public void Remove()
        {
            others.Remove(this);
        }
    }
}
