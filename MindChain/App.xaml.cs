﻿using MindChain.DataProviders;
using MindChain.Models;
using MindChain.Pages;
using MindChain.Service;
using Plugin.Settings;
using System;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using Xamarin.Notifications;

namespace MindChain
{
    public partial class App : Application
    {
        public App()
        {
            SimpleContainer.Instance.Register<HttpClient>(new HttpClient());
            SimpleContainer.Instance.Register<IAccountProvider, AccountProvider>();
            SimpleContainer.Instance.Register<IPostProvider, PostProvider>();
            SimpleContainer.Instance.Register<IFileProvider, FileProvider>();
            SimpleContainer.Instance.Register<WebSocketService, WebSocketService>();

            InitializeComponent();

            var testNotificationAction = new NotificationAction
            {
                Command = new Command((test) =>
                {
                    test = test;
                })
            };

            var notificationService = SimpleContainer.Instance.Create<INotificationService>();
            notificationService.RegisterNotificationAction("TestAction", testNotificationAction);

            MainPage = new LoadingPage();
        }

        protected override void OnStart()
        {
            var previousUsername = CrossSettings.Current.GetValueOrDefault("login-username", "");
            var previousPassword = CrossSettings.Current.GetValueOrDefault("login-password", "");

            if (previousUsername != "" && previousPassword != "")
            {
                var loginCommand = new Command(async () =>
                {
                    var accountProvider = SimpleContainer.Instance.Create<AccountProvider>();

                    await accountProvider.Login(previousUsername, previousPassword);
                    var currentUser = await accountProvider.CurrentUser();

                    if(currentUser != null)
                    {
                        MainPage = new MainPage();

                        var webSocketService = SimpleContainer.Instance.Create<WebSocketService>();
                        await webSocketService.InitializeConnection();
                        webSocketService.Connect<Post>("newPost", p => p = p);
                    }
                    else
                    {
                        MainPage = new RegisterPage();
                    }
                });


                loginCommand.Execute(null);
            }
            else
            {
                MainPage = new RegisterPage();
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
