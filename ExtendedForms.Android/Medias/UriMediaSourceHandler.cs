﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.Graphics;
using Xamarin.Forms.Internals;
using Xamarin.Medias;
using Android.Content;
using Xamarin.Forms.Platform.Android;
using ExtendedForms.Android.Medias;

[assembly: ExportMediaSourceHandler(typeof(UriMediaSource), typeof(UriMediaSourceHandler))]
namespace ExtendedForms.Android.Medias
{
    public sealed class UriMediaSourceHandler : IMediaSourceHandler
    {
        public async Task<Bitmap> LoadImageAsync(MediaSource source, Context context, CancellationToken cancelationToken = default(CancellationToken))
        {
            var imageLoader = source as UriMediaSource;
            Bitmap bitmap = null;
            if (imageLoader?.Uri != null)
            {
                using (Stream imageStream = await imageLoader.GetStreamAsync(cancelationToken).ConfigureAwait(false))
                    bitmap = await BitmapFactory.DecodeStreamAsync(imageStream).ConfigureAwait(false);
            }

            if (bitmap == null)
            {
                Log.Warning(nameof(ImageLoaderSourceHandler), "Could not retrieve image or image data was invalid: {0}", imageLoader);
            }

            return bitmap;
        }
    }
}
