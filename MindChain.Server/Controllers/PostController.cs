﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MindChain.Data;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using MindChain.Server.Services;

namespace MindChain.Server.Controllers
{
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private readonly Context context;
        private readonly WebSocketService webSocketController;

        public PostController(Context context, WebSocketService webSocketController)
        {
            this.context = context;
            this.webSocketController = webSocketController;
        }

        [HttpGet("account/localizable"), Authorize]
        public IEnumerable<Post> GetLocalizableFromUser()
        {
            var username = User.Identity.Name;

            //Get friends
            var friendships = context.Friendships.Include(x => x.Account1).Include(x => x.Account2).Where(t => t.Account1.UserName == username || t.Account2.UserName == username);
            List<string> accounts = new List<string>();
            foreach (Friendship fs in friendships)
            {
                if (fs.Account1.UserName != username)
                {
                    accounts.Add(fs.Account1.UserName);
                }

                if (fs.Account2.UserName != username)
                {
                    accounts.Add(fs.Account2.UserName);
                }
            }

            //Add my own account
            accounts.Add(username);

            //Get events associated
            var query = from post in context.Posts where accounts.Contains(post.Account.UserName) && post.Location != null select post;
            return query.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).ToList();
        }

        [HttpGet("account/{username}")]
        public IEnumerable<Post> GetFromUser(string username)
        {
            return context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).Where(t => t.Account.UserName == username).OrderByDescending(x => x.Date).ToList();
        }

        [HttpGet("{id}")]
        public Post Get(string id)
        {
            return context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).FirstOrDefault(t => t.Id == id);
        }

        [HttpGet("account/{username}/first")]
        public Post GetFirstFromUser(string username)
        {
            return context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).Where(t => t.Account.UserName == username).OrderByDescending(s => s.Date).ThenBy(x => x.Id).FirstOrDefault();
        }

        [HttpGet("{id}/previous")]
        public Post Previous(string id)
        {
            var post = context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).FirstOrDefault(t => t.Id == id);
            return context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).Where(t => t.Account.UserName == post.Account.UserName && t.Id != id).OrderByDescending(s => s.Date).ThenBy(x => x.Id).FirstOrDefault();
        }

        [HttpGet("{id}/next")]
        public Post Next(string id)
        {
            var post = context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).FirstOrDefault(t => t.Id == id);
            return context.Posts.Include(x => x.Account).Include(x => x.Location).Include(x => x.Files).Where(t => t.Account.UserName == post.Account.UserName && t.Id != id).OrderByDescending(s => s.Date).ThenBy(x => x.Id).FirstOrDefault();
        }

        [HttpPost, Authorize]
        public async void Post([FromBody]Post post)
        {
            if (User.Identity.Name != context.Accounts.Where(x => x.Id == post.AccountId).First().UserName)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.Forbidden);
            }

            context.Posts.Add(post);
            context.SaveChanges();

            await webSocketController.Send(User.Identity.Name, "newPost", post);

            //Send new post request to friends
            var friendships = context.Friendships.Include(x => x.Account1).Include(x => x.Account2).Where(t => t.Account1.UserName == User.Identity.Name || t.Account2.UserName == User.Identity.Name);
            foreach (Friendship fs in friendships)
            {
                if (fs.Account1.UserName != User.Identity.Name)
                {
                    await webSocketController.Send(fs.Account1.UserName, "newPost", post);
                }

                if (fs.Account2.UserName != User.Identity.Name)
                {
                    await webSocketController.Send(fs.Account2.UserName, "newPost", post);
                }
            }
        }
        
        [HttpPut("{post}"), Authorize]
        public void Put([FromBody]Post post)
        {
            if (User.Identity.Name != context.Accounts.Where(x => x.Id == post.AccountId).First().UserName)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.Forbidden);
            }

            context.Posts.Update(post);
            context.SaveChanges();
        }
        
        [HttpDelete("{post}"), Authorize]
        public void Delete([FromBody]Post post)
        {
            if (User.Identity.Name != context.Accounts.Where(x => x.Id == post.AccountId).First().UserName)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.Forbidden);
            }

            context.Posts.Remove(context.Posts.FirstOrDefault(t => t.Id == post.Id));
            context.SaveChanges();
        }
    }
}
