﻿using ExtendedForms.iOS.Services;
using ExtendedForms.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Services;
using UserNotifications;
using ExtendedForms.iOS.Notifications;
using ExtendedForms.iOS.Medias;
#if __MOBILE__
using UIKit;
using Xamarin.Forms.Platform.iOS;
using TNativeView = UIKit.UIView;
#else
using AppKit;
using Xamarin.Forms.Platform.MacOS;
using TNativeView = AppKit.NSView;
#endif

namespace ExtendedForms.Forms
{
    public class Forms
    {
        static bool? s_isiOS11OrNewer;

        public static bool IsiOS11OrNewer
        {
            get
            {
                #if __MOBILE__
                if (!s_isiOS11OrNewer.HasValue)
                    s_isiOS11OrNewer = UIDevice.CurrentDevice.CheckSystemVersion(11, 0);
                return s_isiOS11OrNewer.Value;
                #else
                return false;
                #endif
            }
        }

        public static void Init()
        {
            //Register handlers
            Xamarin.Forms.Registrar.RegisterAll(new[] { typeof(ExportNotificationComponentAttribute), typeof(ExportMediaSourceHandlerAttribute) });

            //Enable notifications
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) => {

            });
            UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            //Register services
            SimpleContainer.Instance.Register<IFileService, FileService>();
            SimpleContainer.Instance.Register<IGeocoder, Geocoder>();
            SimpleContainer.Instance.Register<INotificationService, NotificationService>();
        }
    }
}
