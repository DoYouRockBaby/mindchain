﻿using Xamarin.Forms.Xaml;

namespace Sample.Pages.Notification.Notification
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActionNotification : Xamarin.Notifications.Notification
    {
        public ActionNotification()
        {
            InitializeComponent();
        }
    }
}