﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MindChain.Data;
using MindChain.Server.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace MindChain.Server.Controllers
{
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private IHostingEnvironment environment;
        private Random random = new Random((int)DateTime.Now.ToBinary());

        public FileController(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        [HttpGet("{filename}")]
        public IActionResult GetFile(string filename)
        {
            var filepath = Path.Combine(environment.ContentRootPath, "Uploads", filename);

            try
            {
                return new FileStreamResult(new FileStream(filepath, FileMode.Open), FileUtility.GetMimeType(filename));
            }
            catch (Exception)
            {
                throw new ErrorHandlerMiddleware.HttpStatusCodeException(HttpStatusCode.NotFound);
            }
        }

        [HttpPost]
        public async Task<string> SendFile([FromBody]FileDatas file)
        {
            var filename = random.Next() + Path.GetExtension(file.Name);
            var filepath = Path.Combine(environment.ContentRootPath, "Uploads", filename);

            using (var stream = new FileStream(filepath, FileMode.Create))
            {
                await stream.WriteAsync(file.Datas, 0, file.Datas.Length);
            }

            var result = Request.Host + "/api/file/" + filename;
            return result;
        }

        /*[HttpPost]
        public IEnumerable<string> SendFiles([FromBody]IEnumerable<FileDatas> files)
        {
            List<string> filenames = new List<string>();

            foreach(var file in files)
            {
                filenames.Add(SendFile(file));
            }

            return filenames;
        }*/
    }
}
