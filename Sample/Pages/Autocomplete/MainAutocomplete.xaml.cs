﻿using Sample.Pages.Notification.Notification;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Xamarin.Forms.AutocompleteEntry;

namespace Sample.Pages.Autocomplete
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainAutocomplete : TabbedPage
    {
        public class Entry : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            private string testString;
            public string TestString
            {
                get
                {
                    return testString;
                }
                set
                {
                    if (testString != value)
                    {
                        testString = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TestString)));
                    }
                }
            }
        }

        class SuggestionProvider : ISuggestionProvider
        {
            public ObservableCollection<Entry> TestList { get; set; }

            public SuggestionProvider(ObservableCollection<Entry> testList)
            {
                TestList = testList;
            }

            public Task<IEnumerable> GetSuggestions(string request)
            {
                var result = TestList.Where(t => t.TestString.ToLower().Contains(request.ToLower())).ToArray() as IEnumerable;
                return Task.FromResult(result);
            }

            public string ConvertToString(object obj)
            {
                if (obj is Entry entry)
                {
                    return entry.TestString;
                }
                else
                {
                    return obj.ToString();
                }
            }
        }

        public class ViewModels : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;

            public ISuggestionProvider suggestionProvider;
            public ISuggestionProvider SuggestionProvider
            {
                get
                {
                    return suggestionProvider;
                }
                set
                {
                    if (suggestionProvider != value)
                    {
                        suggestionProvider = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SuggestionProvider)));
                    }
                }
            }

            public ObservableCollection<Entry> testList;
            public ObservableCollection<Entry> TestList
            {
                get
                {
                    return testList;
                }
                set
                {
                    if (testList != value)
                    {
                        testList = value;
                        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TestList)));
                    }
                }
            }
        }

        public MainAutocomplete()
        {
            var list = new ObservableCollection<Entry>
            {
                new Entry { TestString = "Leo" },
                new Entry { TestString = "Lea" },
                new Entry { TestString = "Lou" },
                new Entry { TestString = "Fanny" },
                new Entry { TestString = "Farid" },
            };

            foreach (var str in COUNTRIES)
            {
                list.Add(new Entry { TestString = str });
            }

            var vm = new ViewModels
            {
                TestList = list,
                SuggestionProvider = new SuggestionProvider(list)
            };

            BindingContext = vm;

            InitializeComponent();
        }

        static string[] COUNTRIES = new string[] {
            "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra",
            "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina",
            "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan",
            "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
            "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
            "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory",
            "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi",
            "Cote d'Ivoire", "Cambodia", "Cameroon", "Canada", "Cape Verde",
            "Cayman Islands", "Central African Republic", "Chad", "Chile", "China",
            "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
            "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic",
            "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic",
            "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea",
            "Estonia", "Ethiopia", "Faeroe Islands", "Falkland Islands", "Fiji", "Finland",
            "Former Yugoslav Republic of Macedonia", "France", "French Guiana", "French Polynesia",
            "French Southern Territories", "Gabon", "Georgia", "Germany", "Ghana", "Gibraltar",
            "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau",
            "Guyana", "Haiti", "Heard Island and McDonald Islands", "Honduras", "Hong Kong", "Hungary",
            "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica",
            "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Laos",
            "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
            "Macau", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands",
            "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Moldova",
            "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia",
            "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand",
            "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Marianas",
            "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru",
            "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar",
            "Reunion", "Romania", "Russia", "Rwanda", "Sqo Tome and Principe", "Saint Helena",
            "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon",
            "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Saudi Arabia", "Senegal",
            "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands",
            "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "South Korea",
            "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden",
            "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Bahamas",
            "The Gambia", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
            "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Virgin Islands", "Uganda",
            "Ukraine", "United Arab Emirates", "United Kingdom",
            "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan",
            "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Wallis and Futuna", "Western Sahara",
            "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"
        };
    }
}